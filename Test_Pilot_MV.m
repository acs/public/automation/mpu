%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main Script to run the impact analysis test
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Test Pilot in MV site: analysis and design of MV monitoring system
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
clear all
close all


Ntot = 11^2; %evenly spaced among decades
t_delta_in = -8 ; 
t_delta_fin = 2; %so is 10 intervals per decade

time_test = logspace(t_delta_in,t_delta_fin,Ntot);


width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;


run Basecase_MV




%% Time Indipendent Uncertainty Preparation Data
type_test = [0,1,2,3]; %MV
for Ntest = 1 : length(type_test)
    display(strcat('Test TI number: ',num2str(type_test(Ntest))));
    [ST,N_conf] = generate_conf_pilot(BaseConf,type_test(Ntest));
    run Test_Impact_Scenarios_TI.m
    save_dir = strcat(pwd,'\Results\',text_in,'_TI_test_type_',num2str(type_test(Ntest)));
    mkdir(save_dir);
    save_cat = strcat(save_dir,'\Result_Test');
    save(save_cat,'Test_rep_IDQ','Test_rep_VDQ','Test_rep_IAP','Test_rep_VAP','ST','conf_test','P','unc');
end

%% Time Dependent Uncertainty Preparation Data
for Ntest = 1 : length(type_test)
    display(strcat('Test TD number: ',num2str(type_test(Ntest))));
    [ST,N_conf] = generate_conf_pilot(BaseConf,type_test(Ntest));
    run Test_Impact_Scenarios_TDcolor.m
    save_dir =strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(type_test(Ntest)));
    mkdir(save_dir);
    save_cat = strcat(save_dir,'\Metric_Test');
    save(save_cat,'ST','conf_test','P','unc','eigA_m','time_test');
end

%% Time Dependent Uncertainty Generation Indexes
for Ntest = 1 : length(type_test)
    Ntest
    save_dir =strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(type_test(Ntest)),'\Metric_Test');
    load(save_dir)
    run Generate_Test_Metrics_TD.m
    save_dir = strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(type_test(Ntest)),'\Result_Test');
    save(save_dir,'Test_rep_IDQ','Test_rep_VDQ','Test_rep_IAP','Test_rep_VAP','unc','conf_test','ST')
end


%% step 0
Ts = 60; 
type_test = 0; % base_case MV
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)
%% step 1
Ts = 1; 
type_test = 1; % base_case + fast reporting rate
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)

%% step 1
Ts = 1; 
type_test = 1; %Comparison TD: base_case vs improved
plot_combined_TD_comparison_latex_pilot_MV(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)

%% step 2
Ts = 1; 
type_test = 2; %Comparison TI: base_case vs improved
plot_combined_TI_comparison_latex_pilot_MV(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)

%% step 2
Ts = 1; 
type_test = 2; %MV with 3 currents
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)

%% step 3
Ts = 1; 
type_test = 3; %Comparison TI: base_case vs improved
plot_combined_TI_comparison_latex_pilot_MV(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)

%% step 3
Ts = 1; 
type_test = 3; %MV with 3 voltages
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)
