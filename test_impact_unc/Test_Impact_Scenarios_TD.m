%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate iteratively uncertainty models of TD uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k = 0.866*0.9;
   
NLinesTest = []; P = []; unc = []; eigA_m = [];

for m = 1 : N_conf
    m
    [ST] = configure_inverter_parameter(ST,m);
    Rcc = ST(m).Rcc; %Common coupling resistance (between bus 0 and 1)
    Lcc = ST(m).Lcc;%Common coupling inductance (between bus 0 and 1)
    if length(ST(m).rline) == 1
        rline =  ST(m).rline*ones(ST(m).Nnode-1,1); %Line resistance
        Lline =  ST(m).Lline*ones(ST(m).Nnode-1,1);%Line inductance
    else
        rline =  ST(m).rline;
        Lline =  ST(m).Lline;
    end
    Gnom = 1000;%Nominal Irradiance for PV inverters
    if isfield(BaseConf,'Pmax') == 1
        Pmax = BaseConf(m).Pmax;
    else
        Pmax = 10;%Nominal power output by the PV for nominal irradiance
    end
    Pg = abs(ST(m).Sn).*ST(m).PF; %load power in W
    P_obj = - ST(m).Sn.*ST(m).PF;%inverter generated active power in W

    % generation of test data
    %test_case = 'genereting dynamic model'
    [DM,SD] = Dynamic_Model_gen(ST(m).Nnode,ST(m).topology,ST(m).grid_form,...
        ST(m).grid_supp_PV,ST(m).grid_supp_PQ,ST(m).plac_load,....
        P_obj,Pg,ST(m).f,ST(m).Vn,ST(m).kpv,ST(m).kiv,...
        ST(m).kpc,ST(m).kic,...
        ST(m).kp_PLL,ST(m).ki_PLL,...
        ST(m).rf,ST(m).rc,ST(m).Lf,ST(m).Cf,...
        Rcc,Lcc,rline,Lline,Gnom,Pmax,ST(m).PF,ST(m).Sn);% DM: dynamic model, SD: status data, VS: variance status
    [Test_SetUp,Combination_devices,Accuracy,GridData] = set_DSSE_input(DM,ST(m)); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE
    [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
    
   
    
    [right_eig,eigA,left_eig]=eig(Atot);
    
    %%%%
    if isfield(ST(m),'Ts') == 1
        Ts = ST(m).Ts;
    end
    if isfield(ST(m),'colored') == 1
        DM.B_Eo = ST(m).colored;
        DM.B_G = ST(m).colored;
        DM.B_PQ_MV = ST(m).colored;
        DM.B_PQ_LV = ST(m).colored;
    end
    if isfield(BaseConf,'auto_V')==1
        DM.B_Eo = BaseConf.auto_V;
    end
    if isfield(BaseConf,'auto_P')==1
        DM.B_PQ_MV = BaseConf.auto_P;
        DM.B_PQ_LV = BaseConf.auto_P;
    end
    if isfield(BaseConf,'auto_G')==1
        DM.B_G = BaseConf.auto_G;
    end
    
    if isfield(ST(m),'dist') == 1 % multiplier of standard deviation of disturbance
        unc_disturb_Eo = ST(m).dist * unc_disturb_Eo;
        unc_disturb_G  = ST(m).dist * unc_disturb_G;
        unc_distur_PQ  = ST(m).dist * unc_distur_PQ;
    end
    %%%
    [VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy,ST(m).filter);
    eigA_m(m).eigA =diag(eigA);
    for i=1:size(eigA_m(m).eigA,1)
        if real(eigA_m(m).eigA(i))>0
            num_positive_eig = 1
            return
        end
    end
    % -------------
    

    DMTest(m).DM = DM;
    
    
    [u] = create_input(DM,dev_input,1);
    [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
    ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;
    [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
    
    ST(m).P_tot = PowerData.Pflow(1)*GridData.base_power;
    ST(m).Q_tot = PowerData.Qflow(1)*GridData.base_power;


    [Test_SetUp,Combination_devices,Accuracy,GridData] = set_DSSE_input(DM,ST(m)); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE    

    %GridData.inj_status = 0;
    [W,GridData,~] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
    [H_IRI] = Jacobian_m_IRIDSSE(GridData);   
    G1 = H_IRI'*W*H_IRI;    

    var_err_rectangular_mat_IRI = inv(G1);
    var_err_rectangular_mat_IRI = var_err_rectangular_mat_IRI(ref+2:end,ref+2:end);
    for x = 1 : 2*GridData.Lines_num
        var_err_rectangular_mat_IRI(:,x) = var_err_rectangular_mat_IRI(:,x) .* ([GridData.base_current*ones(2*GridData.Lines_num,1)] );
    end
    for x = 1 : 2*GridData.Lines_num
        var_err_rectangular_mat_IRI(x,:) = var_err_rectangular_mat_IRI(x,:) .* ([GridData.base_current*ones(2*GridData.Lines_num,1)] )';
    end
    for x = 1 : GridData.Lines_num
        var_err_rectangular_mat_IRI(:,[2*x-1,2*x]) = var_err_rectangular_mat_IRI(:,[2*x,2*x-1]);
        var_err_rectangular_mat_IRI([2*x-1,2*x],:) = var_err_rectangular_mat_IRI([2*x,2*x-1],:);
    end
    
    P_TI = var_err_rectangular_mat_IRI;

    [~,Combination_devices1,Accuracy1,GridData1] = set_DSSE_Vstate(DM); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE
     GridData1.inj_status = 0;
    [~,GridData1,~] = Weight_m(GridData1,PowerData,Combination_devices1,Accuracy1);
    [H_IRI1] = Jacobian_m_IRIDSSE(GridData1);  
    H_IRI1(:,1) = [];
    H_IRI1(1,:) = [];   

    

    use_pu = 0; %lasciare 0 2019.01.10
    rotI = calc_rotI( GridData,PowerData,use_pu);
    rotI = rotI(2:end,2:end);
    rotV = calc_rotV( GridData,PowerData,use_pu);
    rotV = rotV(2:end,2:end);
   
    if  type_test(Ntest) ~= 16 && type_test(Ntest) ~= 18
        Nt_max = ceil( time_test(end) / Ts);
    else
        Nt_max = ceil( (time_test(end)+ ST(m).delay) / Ts);
    end
        
        Tmatrix_eq = eye(size(TmatrixTs));
        Forz_matrix_eq= zeros(size(Forz_matrixTs));
        P_fin2_z = []; P_fin3_z = []; P_fin4_z = []; V = []; W = []; M = [];
        P_fin2 = zeros(size(VS.var_status_absolute));
        
        
        for z = 1 : Nt_max
            Forz_matrix_eq = Forz_matrixTs + TmatrixTs *Forz_matrix_eq;
            Tmatrix_eq = Tmatrix_eq * TmatrixTs;
            M_tot = zeros(11*DM.NGF+10*DM.NGS+2*DM.Nload+2*DM.PCC+2*DM.Nline,11*DM.NGF+10*DM.NGS+2*DM.Nload+2*DM.PCC+2*DM.Nline);
            if ST(m).filter == 1
                if z > 1
                    for s = 1 : z-1
                        V(s).V = VS.F_matrix^(s) * W(z-s).W; %correlation
                        M(s).M = Dist_matrixTs * V(s).V * transpose(Dist_matrixTs) ;
                        M_tot  = M_tot + (TmatrixTs^s) * M(s).M + M(s).M * transpose(TmatrixTs^s) ;
                    end
                    W(z).W = VS.F_matrix * W(z-1).W * transpose(VS.F_matrix) + VS.var_disturbance_absolute;
                    P_fin2_z(z).P_fin2 = TmatrixTs * P_fin2_z(z-1).P_fin2 * transpose(TmatrixTs) + Dist_matrixTs * W(z).W * transpose(Dist_matrixTs) + M_tot;
                else
                    W(z).W = VS.var_disturbance_absolute;
                    P_fin2_z(z).P_fin2 = Dist_matrixTs * W(z).W * transpose(Dist_matrixTs);
                end
            end
            P_fin3_z(z).P_fin3 =  Forz_matrix_eq * diag((u*dev_input).^2) * transpose(Forz_matrix_eq) ;
            P_fin4_z(z).P_fin4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
        end
    
    
    for t = 1 : Ntot
        
            Tmatrix_eq = eye(size(TmatrixTs));
            Forz_matrix_eq= zeros(size(Forz_matrixTs));
            
            if  type_test(Ntest) ~= 16 && type_test(Ntest) ~= 18
                time_delta = time_test(t);
            else
                time_delta = time_test(t) + ST(m).delay;
            end
            
            
            [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
            Nt = ceil( time_delta / Ts);
            if ST(m).filter == 1
                if time_delta > Ts
                    P_fin2 = P_fin2_z(Nt).P_fin2;
                    P_fin3 = P_fin3_z(Nt).P_fin3;
                    P_fin4 = P_fin4_z(Nt).P_fin4;
                else
                    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
                    P_fin3 = Forz_matrix * diag((u*dev_input).^2) * transpose(Forz_matrix) ;
                    P_fin4 = (Tmatrix-eye(size(Tmatrix))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;
                end
            else
                P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
                P_fin3 = Forz_matrix * diag((u*dev_input).^2) * transpose(Forz_matrix) ;
                P_fin4 = (Tmatrix-eye(size(Tmatrix))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;
            end
            P_fin = VS.var_status_absolute + P_fin2 + P_fin3 + P_fin4 ;
            
            for x = 1 : GridData.Lines_num
                P_fin(:,[ref+2*x-1,ref+2*x]) = P_fin(:,[ref+2*x,ref+2*x-1]);
                P_fin([ref+2*x-1,ref+2*x],:) = P_fin([ref+2*x,ref+2*x-1],:);
                P_fin2(:,[ref+2*x-1,ref+2*x]) = P_fin2(:,[ref+2*x,ref+2*x-1]);
                P_fin2([ref+2*x-1,ref+2*x],:) = P_fin2([ref+2*x,ref+2*x-1],:);
                P_fin3(:,[ref+2*x-1,ref+2*x]) = P_fin3(:,[ref+2*x,ref+2*x-1]);
                P_fin3([ref+2*x-1,ref+2*x],:) = P_fin3([ref+2*x,ref+2*x-1],:);
                P_fin4(:,[ref+2*x-1,ref+2*x]) = P_fin4(:,[ref+2*x,ref+2*x-1]);
                P_fin4([ref+2*x-1,ref+2*x],:) = P_fin4([ref+2*x,ref+2*x-1],:);
            end
            
            P_TOT = P_fin;
            P_TI = VS.var_status_absolute(ref+1:end,ref+1:end);
            P_TD = P_fin2 + P_fin3 + P_fin4;
       
        P_TD = P_TD(ref+1:end,ref+1:end);
        P(m).P_TD = diag(P_TD);
        if DM.PCC == 1
            P(m).P_TD_I_D(1,t) = P(m).P_TD(1,1);
            P(m).P_TD_I_Q(1,t) = P(m).P_TD(2,1);
        end
        for x = DM.PCC + 1 : DM.PCC + DM.Nline
            P(m).P_TD_I_D(x,t) = P(m).P_TD(2*(x-1)+1,1);
            P(m).P_TD_I_Q(x,t) = P(m).P_TD(2*(x-1)+2,1);
        end
        
        
        P_fin_temp = P_TD;

        Rx_polar_IRI = rotI * P_fin_temp * rotI';
        var_err_polar_mat_IRI = diag(Rx_polar_IRI);
        

        if DM.PCC == 1
            P(m).P_TD_I_A(1,t)  = var_err_polar_mat_IRI(1,1);
            P(m).P_TD_I_P(1,t)  = var_err_polar_mat_IRI(2,1);
        end
        for x = DM.PCC + 1 : DM.PCC + DM.Nline
            P(m).P_TD_I_A(x,t) = var_err_polar_mat_IRI(2*(x-1)+1);
            P(m).P_TD_I_P(x,t) = var_err_polar_mat_IRI(2*(x-1)+2);
        end
        
        % here the covariance for the node voltages are calculated
        for x = 1 : 2*GridData.Lines_num
            P_fin_temp(:,x) = P_fin_temp(:,x) ./ ([ GridData.base_current*ones(2*GridData.Lines_num,1)] );
        end
        for x = 1 :  2*GridData.Lines_num
            P_fin_temp(x,:) = P_fin_temp(x,:) ./ ([ GridData.base_current*ones(2*GridData.Lines_num,1)] )';
        end

        P_V = H_IRI1 * P_fin_temp * H_IRI1';

        for x = 1 :  2*GridData.Lines_num
            P_V(:,x) = P_V(:,x) .* ([GridData.base_voltage*ones(2*(GridData.Nodes_num-1),1)] );
        end
        for x = 1 : 2*GridData.Lines_num
            P_V(x,:) = P_V(x,:) .* ([GridData.base_voltage*ones(2*(GridData.Nodes_num-1),1)] )';
        end
        
        var_status_out_absolute = diag(P_V);
        for x = 2 : GridData.Nodes_num 
            P(m).P_TD_V_D(x,t) = var_status_out_absolute(2*(x-2) + 1);
            P(m).P_TD_V_Q(x,t) = var_status_out_absolute(2*(x-2) + 2);
        end
        
        Rx_polar_VRI = rotV * P_V * rotV';
        var_err_polar_mat_VRI = diag(Rx_polar_VRI);

        for x = 2 : GridData.Nodes_num 
            P(m).P_TD_V_A(x,t) = var_err_polar_mat_VRI(2*(x-2)+1,1);
            P(m).P_TD_V_P(x,t) = var_err_polar_mat_VRI(2*(x-2)+2,1);
        end
        
    unc(m).unc_V_A(1:GridData.Nodes_num,t) = (P(m).P_TD_V_A(1:GridData.Nodes_num,t).^0.5./ (PowerData.Vmagn*GridData.base_voltage));
    unc(m).unc_V_D(1:GridData.Nodes_num,t) = (P(m).P_TD_V_D(1:GridData.Nodes_num,t).^0.5./ (PowerData.Vmagn*GridData.base_voltage));
    unc(m).unc_V_Q(1:GridData.Nodes_num,t) = (P(m).P_TD_V_Q(1:GridData.Nodes_num,t).^0.5./ (PowerData.Vmagn*GridData.base_voltage));
    unc(m).unc_V_P(1:GridData.Nodes_num,t) = (P(m).P_TD_V_P(1:GridData.Nodes_num,t).^0.5 );
    
    unc(m).unc_I_A(1:GridData.Lines_num,t) = (P(m).P_TD_I_A(1:GridData.Lines_num,t).^0.5 ./ (PowerData.Imagn*GridData.base_current));
    unc(m).unc_I_D(1:GridData.Lines_num,t) = (P(m).P_TD_I_D(1:GridData.Lines_num,t).^0.5 ./ (PowerData.Imagn*GridData.base_current));
    unc(m).unc_I_Q(1:GridData.Lines_num,t) = (P(m).P_TD_I_Q(1:GridData.Lines_num,t).^0.5 ./ (PowerData.Imagn*GridData.base_current));
    unc(m).unc_I_P(1:GridData.Lines_num,t) = (P(m).P_TD_I_P(1:GridData.Lines_num,t).^0.5 );

    end
  
end

conf_test = [1:N_conf]

