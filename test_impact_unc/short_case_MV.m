%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script with base case parameter of a MV grid
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unc_disturb_Eo = 0.10;
unc_disturb_G  = 0.01;
unc_distur_PQ  = 0.50;
dev_input      = 0.0;
dev_status     = 0.0;

BaseConf.Nnode = 4;
BaseConf.Nline = 1;
BaseConf.PQRL = 1;
%BaseConf.Sn = 4*5e5/BaseConf.Nnode;
BaseConf.Sn = 50000;

BaseConf.f = 50;
BaseConf.fsw = 10000;
BaseConf.PF = 0.95; %Power Factor, applied to RL loads and PV inverters
BaseConf.Vn = 16300;

%%% to calculate the Rcc and Lcc
Vn_HV = 16300;
Sn_HV = 5000*10^6;
Rcc1 = sqrt(1/11)*(Vn_HV^2)/(Sn_HV);
Lcc1 = 10*Rcc1/(2*pi*50);
BaseConf.Rcc = Rcc1 + 0.016;
BaseConf.Lcc = Lcc1 + 1.92/(2*pi*50);
%%%
%%% to calculate line impedance
rline_km = (1/3)*(2*0.510+0.658); %Line resistance per km
Lline_km = (1/3)*(2*0.366+1.611)/(2*pi*50);%Line inductance per km
length_km = 1;
%%%
BaseConf.rline = rline_km * length_km;
BaseConf.Lline = Lline_km * length_km;


BaseConf.PQflow = [];
BaseConf.Pseudo = [2:BaseConf.Nnode+1;zeros(1,BaseConf.Nnode)];
BaseConf.PQ = [];
BaseConf.V = [1;0];
BaseConf.I = [1;0];
BaseConf.unc_dev =  0.01;
BaseConf.unc_pseudo = 0.5;

BaseConf.delay = 0;
BaseConf.colored = 0;
BaseConf.Ts = 1e6;
BaseConf.filter = 0;
BaseConf.dist = 1;