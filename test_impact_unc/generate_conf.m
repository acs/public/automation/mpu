%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate scenarios to be tested for impact analysis
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ST,N_conf] = generate_conf(BaseConf,type_test)

%0 base case
%1 number nodes
%2 number lines
%3 power consumption
%4 line impedances
%5 ratio of inverters installed
%6 switchingr frequency
%7 colored noise Ts
%8 colored noise autovariance

%10 number voltage devices
%11 number current flow devices
%12 number P inj devices
%13 number P flow devices
%14 accuracy meas devices
%15 accuracy pseudo meas
%16 delay
%17 unsynchr
%18 delay linearly spaced

N_conf = 0;

if type_test == 0 %base_case - colored
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('base case');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
        2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
    ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = BaseConf.V;
    ST(N_conf).I = BaseConf.I;
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = BaseConf.delay;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).Ts = BaseConf.Ts;
    ST(N_conf).filter = BaseConf.filter;
   ST(N_conf).dist = 1;

   
elseif type_test == 0.1 %base_case - non colored
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('base case');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
        2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
    ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = BaseConf.V;
    ST(N_conf).I = BaseConf.I;
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = BaseConf.delay;
    ST(N_conf).colored = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).filter = 0;
   ST(N_conf).dist = 1;
   
elseif type_test == 1 %number of nodes
    for x = 4 : 2 : 32
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('number nodes');
        ST(N_conf).Nnode = x ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                               2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = [2:ST(N_conf).Nnode+1;zeros(1,ST(N_conf).Nnode)];
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
elseif type_test == 2 %number of lines
    
    topology_x(1).topology = [1,2,3,4,5,6,7,8, 9,10,11,12,13,14,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(2).topology = [1,2,3,4,5,6,7,8, 2,10,11,12,13,14,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(3).topology = [1,2,3,4,5,6,7,4, 9,10,11, 4,13,14,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(4).topology = [1,2,3,4,5,6,4,8, 9, 4,11,12, 4,14,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(5).topology = [1,2,3,4,5,6,7,6, 9, 6,11, 6,13, 6,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(6).topology = [1,2,3,4,5,4,7,4, 9, 4,11, 4,13, 4,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(7).topology = [1,2,3,2,5,2,7,2, 9,2,11, 2,13, 2,15;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(8).topology = [1,2,3,4,5,6,7,8, 8, 8, 8, 8, 8, 8, 8;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(9).topology =  [1,2,3,4,5,6,7,7, 7, 7, 7, 7, 7, 7, 7;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(10).topology =  [1,2,3,4,5,6,6,6, 6, 6, 6, 6, 6, 6, 6;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(11).topology = [1,2,3,4,5,5,5,5, 5, 5, 5, 5, 5, 5, 5;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(12).topology =  [1,2,3,4,4,4,4,4, 4, 4, 4, 4, 4, 4, 4;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(13).topology = [1,2,3,3,3,3,3,3, 3, 3, 3, 3, 3, 3, 3;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    topology_x(14).topology = [1,2,2,2,2,2,2,2, 2, 2, 2, 2, 2, 2, 2;...
        2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    
    for x = 1 : 14
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('number lines');
        ST(N_conf).Nnode = BaseConf.Nnode;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = x;
        ST(N_conf).topology = topology_x(x).topology;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
elseif type_test == 3 %power consumption
    
    Sn_x(1:6)  = BaseConf.Sn*linspace(0.33,1,6);
    Sn_x(7:12) = BaseConf.Sn*linspace(1,3,6);
    
    for x = 1 : 12
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('power consumption');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = Sn_x(x);
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                                2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end

elseif type_test == 4 %line impedances
        
        rline_x(1:6) = BaseConf.rline*linspace(0.25,1,6);
        Lline_x(1:6) = BaseConf.Lline*linspace(0.25,1,6);
        rline_x(7:12) = BaseConf.rline*linspace(1,4,6);
        Lline_x(7:12) = BaseConf.Lline*linspace(1,4,6);
        
    for x = 1 : 12
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('line impedances');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                                2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = rline_x(x);
        ST(N_conf).Lline = Lline_x(x);
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
    
elseif type_test == 5 %ratio of inverters installed
    
    grid_supp_PQ_x(1).grid_supp_PQ = ones(1,BaseConf.Nnode);
    plac_load_x(1).plac_load = zeros(1,BaseConf.Nnode);
    PQRL_x(1) = 1;
    
    grid_supp_PQ_x(2).grid_supp_PQ = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0];
    plac_load_x(2).plac_load =       [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
    PQRL_x(2) = 15/16;
                                     
    grid_supp_PQ_x(3).grid_supp_PQ = [1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0];
    plac_load_x(3).plac_load =       [0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1];
    PQRL_x(3) = 14/16;
                                     
    grid_supp_PQ_x(4).grid_supp_PQ = [1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,0];
    plac_load_x(4).plac_load =       [0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1];
    PQRL_x(4) = 13/16;
                                     
    grid_supp_PQ_x(5).grid_supp_PQ = [1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,0];
    plac_load_x(5).plac_load =       [0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1];
    PQRL_x(5) = 12/16;
                                     
    grid_supp_PQ_x(6).grid_supp_PQ = [1,1,1,0,1,1,1,0,1,1,1,0,1,0,1,0];
    plac_load_x(6).plac_load =       [0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1];
    PQRL_x(6) = 11/16;
                                     
    grid_supp_PQ_x(7).grid_supp_PQ = [1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,0];
    plac_load_x(7).plac_load =       [0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1];
    PQRL_x(7) = 10/16;
                                     
    grid_supp_PQ_x(8).grid_supp_PQ = [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0];
    plac_load_x(8).plac_load =       [0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1];
    PQRL_x(8) = 9/16;
                                      
    grid_supp_PQ_x(9).grid_supp_PQ = [1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0]; 
    plac_load_x(9).plac_load =       [0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1];
    PQRL_x(9) = 8/16;
                                      
    grid_supp_PQ_x(10).grid_supp_PQ = [1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0]; 
    plac_load_x(10).plac_load =       [0,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1];
    PQRL_x(10) = 7/16;
                                      
    grid_supp_PQ_x(11).grid_supp_PQ = [1,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0];
    plac_load_x(11).plac_load =       [0,1,0,1,0,1,0,1,1,1,0,1,0,1,1,1];
    PQRL_x(11) = 6/16;

    grid_supp_PQ_x(12).grid_supp_PQ = [1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0];
    plac_load_x(12).plac_load =       [0,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1];
    PQRL_x(12) = 5/16;
    
    grid_supp_PQ_x(13).grid_supp_PQ = [1,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0];
    plac_load_x(13).plac_load =       [0,1,0,1,0,1,1,1,1,1,0,1,1,1,1,1];
    PQRL_x(13) = 4/16;
    
    grid_supp_PQ_x(14).grid_supp_PQ = [1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0];
    plac_load_x(14).plac_load =       [0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1];
    PQRL_x(14) = 3/16;
    
    grid_supp_PQ_x(15).grid_supp_PQ = [1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0];
    plac_load_x(15).plac_load =       [0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1];
    PQRL_x(15) = 2/16;
    
    grid_supp_PQ_x(16).grid_supp_PQ = [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    plac_load_x(16).plac_load =       [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    PQRL_x(16) = 1/16;
    
    for x = 1 : 16
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('ratio of inverters installed');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = PQRL_x(x);
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                                2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = grid_supp_PQ_x(x).grid_supp_PQ;
        ST(N_conf).plac_load =    plac_load_x(x).plac_load;
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
elseif type_test == 6 %switching frequency
   
    fsw_x(1:6) = BaseConf.fsw*linspace(0.25,1,6);
    fsw_x(7:12) = BaseConf.fsw*linspace(1,4,6);

    
    for x = 1 : 12
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('switching frequency');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = fsw_x(x);
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end

elseif type_test == 7 %colored noise Ts
 
    Ts_x =     [1e6 1e-1  0.3162 1e0 3.162 1e1]; 
    filter_x = [0      1       1   1     1    1];
    for x = 1 : 6
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('colored noise Ts');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = 1;
        ST(N_conf).Ts = Ts_x(x);
        ST(N_conf).filter = filter_x(x);
       ST(N_conf).dist = 1;
    end
 
elseif type_test == 8 %colored noise autovariance
%     Ts_x =      [1e6 1e0   1e0 1e0 1e0  1e0   1e0 1e0]; 
%     colored_x = [1     1     0 0.5 0.9 0.99 0.999   1]; 
%     filter_x =  [0     0     1   1   1    1     1   1];

colored_x = [0.5 0.9 0.99 0.999]; 
    %1st case t_res off + large Ts + colored
    %2nd case t_res_off + small Ts + colored 
    %3rd case t_res_on  + small Ts + colored 
    %4th case t_res_on  + small Ts + white 
    for x = 1 : length(colored_x)
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('colored noise autovariance');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = colored_x(x);
        ST(N_conf).Ts = 1;
        ST(N_conf).filter = 1;
       ST(N_conf).dist = 1;
    end

elseif type_test == 9 %power factor
   
    PF_x(1:10) = 0.9 + 0.1*linspace(0,0.9,10);

    
    for x = 1 : 10
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('power factor');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = PF_x(x);
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end

    
elseif type_test == 10 %number voltage devices
        
    for x = 1 : 17
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('number voltage devices');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = [1:x;BaseConf.delay*(ones(1,x))];
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;  
       ST(N_conf).dist = 1;
    end
    
    
elseif type_test == 11 %number current flow devices
    
    
    for x = 0 : 16
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('number current flow devices');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = [1:x;BaseConf.delay*(ones(1,x))];
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
 
elseif type_test == 12 %number P inj devices
     for x = 1 : 17
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('number P inj devices');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = [2:x;BaseConf.delay*(ones(1,x-1))];
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
     end
    
elseif type_test == 13 %number P flow devices
    
    for x = 1 : 16
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('number P flow devices');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                               2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = [2:x;BaseConf.delay*(ones(1,x-1))];
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end

elseif type_test == 14 %accuracy meas devices
   unc_dev_x(1:5) = 0.002*[1:5];
   unc_dev_x(6:10) = 0.01*[1:5];
    for x = 1 : 10
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('accuracy meas devices');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev =  unc_dev_x(x);
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
elseif type_test == 15 %accuracy pseudo       
    unc_pseudo_x = 0.1*[1:10];
    for x = 1 : 10
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('accuracy pseudo');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = unc_pseudo_x(x);
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
elseif type_test == 16 %delay        
    
    delay_x = [0 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2];
     delay_x = [0 1e-2 1e-1 1e0 1e1];
    for x = 1 : length(delay_x)
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('delay');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        if size(BaseConf.PQflow,1)>1
            ST(N_conf).PQflow = [BaseConf.PQflow(1,:);delay_x(x)*ones(size(BaseConf.PQflow(1,:)))];
        else
            ST(N_conf).PQflow = [];
        end
        ST(N_conf).Pseudo = [BaseConf.Pseudo(1,:);delay_x(x)*ones(size(BaseConf.Pseudo(1,:)))];
        if size(BaseConf.PQ,1)>1
            ST(N_conf).PQ = [BaseConf.PQ(1,:);delay_x(x)*ones(size(BaseConf.PQ(1,:)))];
        else
            ST(N_conf).PQ = [];
        end
        ST(N_conf).V = [BaseConf.V(1,:);delay_x(x)*ones(size(BaseConf.V(1,:)))];
        ST(N_conf).I = [BaseConf.I(1,:);delay_x(x)*ones(size(BaseConf.I(1,:)))];
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = delay_x(x);
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
        ST(N_conf).dist = 1;
    end

    
    
elseif type_test == 17 %standard deviation of white noise       

    dist_x(1:6)  = linspace(0.25,1,6);
    a = linspace(1,4,6);
    dist_x(7:11) = a(2:end);
    
    for x = 1 : length(dist_x)
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('std_noise');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                                2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = ones(1,ST(N_conf).Nnode);%repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =    zeros(1,ST(N_conf).Nnode);%repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
        ST(N_conf).dist = dist_x(x);
    end
    
    
    elseif type_test == 18 %delay linear spaced
    
    delay_x = linspace(0,1,11);
    for x = 1 : length(delay_x)
         N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('delay linearly spaced');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = BaseConf.PQRL;
        ST(N_conf).Sn = BaseConf.Sn;
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
            2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        if size(BaseConf.PQflow,1)>1
            ST(N_conf).PQflow = [BaseConf.PQflow(1,:);delay_x(x)*ones(size(BaseConf.PQflow(1,:)))];
        else
            ST(N_conf).PQflow = [];
        end
        ST(N_conf).Pseudo = [BaseConf.Pseudo(1,:);delay_x(x)*ones(size(BaseConf.Pseudo(1,:)))];
        if size(BaseConf.PQ,1)>1
            ST(N_conf).PQ = [BaseConf.PQ(1,:);delay_x(x)*ones(size(BaseConf.PQ(1,:)))];
        else
            ST(N_conf).PQ = [];
        end
        ST(N_conf).V = [BaseConf.V(1,:);delay_x(x)*ones(size(BaseConf.V(1,:)))];
        ST(N_conf).I = [BaseConf.I(1,:);delay_x(x)*ones(size(BaseConf.I(1,:)))];
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = delay_x(x);
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
        ST(N_conf).dist = 1;
    end
  
    

    
    
    elseif type_test == 23 %negative power consumption
    
    Sn_x  = - BaseConf.Sn*linspace(0.1,3,6);
    Sn_x = [Sn_x,BaseConf.Sn*linspace(0.1,3,6)];

    for x = 1 : length(Sn_x)
        N_conf = N_conf + 1;
        ST(N_conf).Script = strcat('negative power consumption');
        ST(N_conf).Nnode = BaseConf.Nnode ;
        ST(N_conf).PQRL = 1;
        ST(N_conf).Sn = Sn_x(x);
        ST(N_conf).Vn = BaseConf.Vn;
        ST(N_conf).PF = BaseConf.PF;
        ST(N_conf).Nline = BaseConf.Nline;
        ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
                                2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).fsw = BaseConf.fsw;
        ST(N_conf).Rcc = BaseConf.Rcc;
        ST(N_conf).Lcc = BaseConf.Lcc;
        ST(N_conf).rline = BaseConf.rline;
        ST(N_conf).Lline = BaseConf.Lline;
        ST(N_conf).f = BaseConf.f;
        ST(N_conf).grid_supp_PQ = ones(1,ST(N_conf).Nnode);%repmat([1,0],1,ST(N_conf).Nnode/2);
        ST(N_conf).plac_load =    zeros(1,ST(N_conf).Nnode);%repmat([0,1],1,ST(N_conf).Nnode/2);
        ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
        ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
        
        ST(N_conf).PQflow = BaseConf.PQflow;
        ST(N_conf).Pseudo = BaseConf.Pseudo;
        ST(N_conf).PQ = BaseConf.PQ;
        ST(N_conf).V = BaseConf.V;
        ST(N_conf).I = BaseConf.I;
        ST(N_conf).unc_dev = BaseConf.unc_dev;
        ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
        ST(N_conf).delay = BaseConf.delay;
        ST(N_conf).colored = BaseConf.colored;
        ST(N_conf).Ts = BaseConf.Ts;
        ST(N_conf).filter = BaseConf.filter;
       ST(N_conf).dist = 1;
    end
    
    
end


end