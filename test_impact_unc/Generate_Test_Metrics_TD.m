%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate the indexes to evaluate the uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Test_rep_IDQ = []; Test_rep_IAP = [];
Test_rep_VDQ = []; Test_rep_VAP = [];
N_conf = length(conf_test);
treshold1 = 0.22;
treshold2 = 0.07;

for m = 1 : N_conf

    diff_I_D = []; diff_I_Q = []; diff_I_A = []; diff_I_P = [];
    diff_V_D = []; diff_V_Q = []; diff_V_A = []; diff_V_P = [];
    %%
    % Test indexes for the current in D and Q axis
    % It is evaluated how the variance behaves as a function of the reporting rate

unc(m).unc_I_D = P(m).P_TD_I_D.^0.5;
unc(m).unc_I_Q = P(m).P_TD_I_Q.^0.5;
unc(m).unc_I_A = P(m).P_TD_I_A.^0.5;
unc(m).unc_I_P = P(m).P_TD_I_P.^0.5;

unc(m).unc_V_D = P(m).P_TD_V_D.^0.5;
unc(m).unc_V_Q = P(m).P_TD_V_Q.^0.5;
unc(m).unc_V_A = P(m).P_TD_V_A.^0.5;
unc(m).unc_V_P = P(m).P_TD_V_P.^0.5;


    index_I_D = 0;
    index_I_Q = 0;
    index_I_A = 0;
    index_I_P = 0;
    index_V_D = 0;
    index_V_Q = 0;
    index_V_A = 0;
    index_V_P = 0;

    t_init_dI_D = 1;
    t_fin_dI_D = Ntot;
 
    t_init_dI_Q = 1;
    t_fin_dI_Q = Ntot;

    t_init_dI_A = 1;
    t_fin_dI_A = Ntot;
  
    t_init_dI_P = 1;
    t_fin_dI_P = Ntot;

    t_init_dV_D = 1;
    t_fin_dV_D = Ntot;
   
    t_init_dV_Q = 1;
    t_fin_dV_Q = Ntot;

    t_init_dV_A = 1;
    t_fin_dV_A = Ntot;
  
    t_init_dV_P = 1;
    t_fin_dV_P = Ntot;

    max_beta_I_D = 0;
    max_beta_I_Q = 0;
    max_gamma_I_D = 0;
    max_gamma_I_Q = 0;
    max_beta_I_A = 0;
    max_beta_I_P = 0;
    max_gamma_I_A = 0;
    max_gamma_I_P = 0;
    max_beta_V_D = 0;
    max_beta_V_Q = 0;
    max_gamma_V_D = 0;
    max_gamma_V_Q = 0;
    max_beta_V_A = 0;
    max_beta_V_P = 0;
    max_gamma_V_A = 0;
    max_gamma_V_P = 0;
        
    max_P_I_D = max(max(unc(m).unc_I_D));
    max_P_I_Q = max(max(unc(m).unc_I_Q));
    max_P_I_A = max(max(unc(m).unc_I_A));
    max_P_I_P = max(max(unc(m).unc_I_P));
    max_P_V_D = max(max(unc(m).unc_V_D));
    max_P_V_Q = max(max(unc(m).unc_V_Q));
    max_P_V_A = max(max(unc(m).unc_V_A));
    max_P_V_P = max(max(unc(m).unc_V_P));


    for t = 2 : Ntot

        diff_I_D(:,t) = abs((unc(m).unc_I_D(:,t) - unc(m).unc_I_D(:,t-1)))./unc(m).unc_I_D(:,t-1);
        diff_I_Q(:,t) = abs((unc(m).unc_I_Q(:,t) - unc(m).unc_I_Q(:,t-1)))./unc(m).unc_I_Q(:,t-1);
      
        diff_I_A(:,t) = abs((unc(m).unc_I_A(:,t) - unc(m).unc_I_A(:,t-1)))./unc(m).unc_I_A(:,t-1);
        diff_I_P(:,t) = abs((unc(m).unc_I_P(:,t) - unc(m).unc_I_P(:,t-1)))./unc(m).unc_I_P(:,t-1);

        diff_V_D(:,t) = abs((unc(m).unc_V_D(:,t) - unc(m).unc_V_D(:,t-1)))./unc(m).unc_V_D(:,t-1);
        diff_V_Q(:,t) = abs((unc(m).unc_V_Q(:,t) - unc(m).unc_V_Q(:,t-1)))./unc(m).unc_V_Q(:,t-1);

        diff_V_A(:,t) = abs((unc(m).unc_V_A(:,t) - unc(m).unc_V_A(:,t-1)))./unc(m).unc_V_A(:,t-1);
        diff_V_P(:,t) = abs((unc(m).unc_V_P(:,t) - unc(m).unc_V_P(:,t-1)))./unc(m).unc_V_P(:,t-1);


        if max(diff_I_D(:,t)) > treshold1  && index_I_D == 0  %start time beta
                index_I_D = 1;
                t_init_dI_D = t-1; %initial time of the dynamics        
        elseif max(diff_I_D(:,t)) < treshold2 && index_I_D == 1 %start time gamma
                index_I_D = 2;            
                t_fin_dI_D = t ; %final time of the dynamics
        elseif max(diff_I_D(:,t)) > treshold2 && index_I_D == 2 %we see that gamma was not actually started
                index_I_D = 1; 
                t_fin_dI_D = Ntot;
        end


        
        if max(diff_I_Q(:,t)) > treshold1  && index_I_Q == 0  %start time beta
                index_I_Q = 1;
                t_init_dI_Q = t-1; %initial time of the dynamics
        elseif max(diff_I_Q(:,t)) < treshold2 && index_I_Q == 1 %start time gamma
                index_I_Q = 2;            
                t_fin_dI_Q = t ; %final time of the dynamics
        elseif max(diff_I_Q(:,t)) > treshold2 && index_I_Q == 2 %we see that gamma was not actually started
                index_I_Q = 1; 
                t_fin_dI_Q = Ntot;
        end


        if max(diff_I_A(:,t)) > treshold1  && index_I_A == 0  %start time beta
                index_I_A = 1;
                t_init_dI_A = t-1; %initial time of the dynamics 
        elseif max(diff_I_A(:,t)) < treshold2 && index_I_A == 1 %start time gamma
                index_I_A = 2;            
                t_fin_dI_A = t ; %final time of the dynamics
        elseif max(diff_I_A(:,t)) > treshold2 && index_I_A == 2 %we see that gamma was not actually started
                index_I_A = 1; 
                t_fin_dI_A = Ntot;      
        end


        if max(diff_I_P(:,t)) > treshold1  && index_I_P == 0  %start time beta
                index_I_P = 1;
                t_init_dI_P = t-1; %initial time of the dynamics
        elseif max(diff_I_P(:,t)) < treshold2 && index_I_P == 1 %start time gamma
                index_I_P = 2;            
                t_fin_dI_P = t ; %final time of the dynamics
        elseif max(diff_I_P(:,t)) > treshold2 && index_I_P == 2 %we see that gamma was not actually started
                index_I_P = 1; 
                t_fin_dI_P = Ntot;   
        end


        if max(diff_V_D(:,t)) > treshold1  && index_V_D == 0  %start time beta
                index_V_D = 1;
                t_init_dV_D = t-1; %initial time of the dynamics
        elseif max(diff_V_D(:,t)) < treshold2 && index_V_D == 1 %start time gamma
                index_V_D = 2;            
                t_fin_dV_D = t ; %final time of the dynamics
        elseif max(diff_V_D(:,t)) > treshold2 && index_V_D == 2 %we see that gamma was not actually started
                index_V_D = 1; 
                t_fin_dV_D = Ntot;    
        end

        
        if max(diff_V_Q(:,t)) > treshold1  && index_V_Q == 0  %start time beta
                index_V_Q = 1;
                t_init_dV_Q = t-1; %initial time of the dynamics
        elseif max(diff_V_Q(:,t)) < treshold2 && index_V_Q == 1 %start time gamma
                index_V_Q = 2;            
                t_fin_dV_Q = t ; %final time of the dynamics
        elseif max(diff_V_Q(:,t)) > treshold2 && index_V_Q == 2 %we see that gamma was not actually started
                index_V_Q = 1; 
                t_fin_dV_Q = Ntot;    
        end


        if max(diff_V_A(:,t)) > treshold1 && index_V_A == 0  %start time beta
                index_V_A = 1;
                t_init_dV_A = t-1; %initial time of the dynamics
        elseif max(diff_V_A(:,t)) < treshold2 && index_V_A == 1 %start time gamma
                index_V_A = 2;            
                t_fin_dV_A = t ; %final time of the dynamics
        elseif max(diff_V_A(:,t)) > treshold2 && index_V_A == 2 %we see that gamma was not actually started
                index_V_A = 1; 
                t_fin_dV_A = Ntot;    
        end

        if max(diff_V_P(:,t)) > treshold1  && index_V_P == 0  %start time beta
                index_V_P = 1;
                t_init_dV_P = t-1; %initial time of the dynamics
        elseif max(diff_V_P(:,t)) < treshold2 && index_V_P == 1 %start time gamma
                index_V_P = 2;            
                t_fin_dV_P = t ; %final time of the dynamics
        elseif max(diff_V_P(:,t)) > treshold2 && index_V_P == 2 %we see that gamma was not actually started
                index_V_P = 1; 
                t_fin_dV_P = Ntot;  
        end
    end
         
        t_init_DQ = min([t_init_dI_D,t_init_dI_Q,t_init_dV_D,t_init_dV_Q]);
        t_fin_DQ = max([t_fin_dI_D,t_fin_dI_Q,t_fin_dV_D,t_fin_dV_Q]);
        t_init_AP = min([t_init_dI_A,t_init_dI_P,t_init_dV_A,t_init_dV_P]);
        t_fin_AP = max([t_fin_dI_A,t_fin_dI_P,t_fin_dV_A,t_fin_dV_P]);
        
        max_beta_I_D =  max(max(unc(m).unc_I_D(:,t_init_AP:t_fin_AP)));
        max_gamma_I_D =   max(max(unc(m).unc_I_D(:,t_fin_AP:Ntot)));
        max_beta_I_Q =   max(max(unc(m).unc_I_Q(:,t_init_AP:t_fin_AP)));
        max_gamma_I_Q =   max(max(unc(m).unc_I_Q(:,t_fin_AP:Ntot)));
        max_beta_I_A =   max(max(unc(m).unc_I_A(:,t_init_AP:t_fin_AP)));
        max_gamma_I_A =   max(max(unc(m).unc_I_A(:,t_fin_AP:Ntot)));
        max_beta_I_P =  max(max(unc(m).unc_I_P(:,t_init_AP:t_fin_AP)));
        max_gamma_I_P =   max(max(unc(m).unc_I_P(:,t_fin_AP:Ntot)));
        max_beta_V_D =   max(max(unc(m).unc_V_D(:,t_init_AP:t_fin_AP)));
        max_gamma_V_D =   max(max(unc(m).unc_V_D(:,t_fin_AP:Ntot)));
        max_beta_V_Q =  max(max(unc(m).unc_V_Q(:,t_init_AP:t_fin_AP)));
        max_gamma_V_Q =   max(max(unc(m).unc_V_Q(:,t_fin_AP:Ntot)));
        max_beta_V_A =  max(max(unc(m).unc_V_A(:,t_init_AP:t_fin_AP)));
        max_gamma_V_A =   max(max(unc(m).unc_V_A(:,t_fin_AP:Ntot)));
        max_beta_V_P =   max(max(unc(m).unc_V_P(:,t_init_AP:t_fin_AP)));
        max_gamma_V_P =   max(max(unc(m).unc_V_P(:,t_fin_AP:Ntot)));
    
   
    
Inv_abs_eig = sqrt((real(eigA_m(1).eigA)).^2 + (imag(eigA_m(1).eigA)).^2).^-1;
Inv_damp = (abs(real(eigA_m(1).eigA))).^-1;
Inv_freq = (abs(imag(eigA_m(1).eigA))).^-1;

Inv_damp = Inv_abs_eig;
Inv_freq = Inv_abs_eig;
Inv_dampV = Inv_damp;
Inv_freqV = Inv_freq;

% if exist('Atot','var') == 1
%     [~,Combination_devices1,Accuracy1,GridData1] = set_DSSE_Vstate(DM); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE
%     [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData1,SD.x_status);
%     [~,GridData1,~] = Weight_m(GridData1,PowerData,Combination_devices1,Accuracy1);
%     [H_IRI1] = Jacobian_m_IRIDSSE(GridData1);
%     ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;
%     H_IRI1(:,ref+1) = [];
%     H_IRI1(ref+1,:) = [];
%     Avolt = H_IRI1*Atot;
%     
%     [right_eig,eigA,left_eig]=eig(Avolt);
%     eigA = diag(eigA);
%     Inv_abs_eigV = sqrt((real(eigA)).^2 + (imag(eigA_m(1).eigA)).^2).^-1;
%     Inv_dampV = (abs(real(eigA))).^-1;
%     Inv_freqV = (abs(imag(eigA))).^-1;
% 
%     Inv_dampV = Inv_abs_eigV;
%     Inv_freqV = Inv_abs_eigV;
% end

Test_rep_IDQ = [Test_rep_IDQ;[max_beta_I_D,max_beta_I_Q,max_gamma_I_D,max_gamma_I_Q,time_test(t_init_DQ),time_test(t_fin_DQ),min(Inv_freq),max(Inv_damp)]];  
Test_rep_IAP = [Test_rep_IAP;[max_beta_I_A,max_beta_I_P,max_gamma_I_A,max_gamma_I_P,time_test(t_init_AP),time_test(t_fin_AP),min(Inv_freq),max(Inv_damp)]];
Test_rep_VDQ = [Test_rep_VDQ;[max_beta_V_D,max_beta_V_Q,max_gamma_V_D,max_gamma_V_Q,time_test(t_fin_DQ),time_test(t_fin_DQ),min(Inv_freqV),max(Inv_dampV)]];
Test_rep_VAP = [Test_rep_VAP;[max_beta_V_A,max_beta_V_P,max_gamma_V_A,max_gamma_V_P,time_test(t_init_AP),time_test(t_fin_AP),min(Inv_freqV),max(Inv_dampV)]];

end