%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate iteratively uncertainty models of TI uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Test_rep_IDQ = []; Test_rep_IAP = [];
Test_rep_VDQ = []; Test_rep_VAP = [];
NLinesTest = []; P = []; unc = [];


for m = 1 : N_conf

    [ST] = configure_inverter_parameter(ST,m);
    Rcc = ST(m).Rcc; %Common coupling resistance (between bus 0 and 1)
    Lcc = ST(m).Lcc;%Common coupling inductance (between bus 0 and 1)
    if length(ST(m).rline) == 1
        rline =  ST(m).rline*ones(ST(m).Nnode-1,1); %Line resistance
        Lline =  ST(m).Lline*ones(ST(m).Nnode-1,1);%Line inductance
    else
        rline =  ST(m).rline;
        Lline =  ST(m).Lline;
    end
    Gnom = 1000;%Nominal Irradiance for PV inverters
    if isfield(BaseConf,'Pmax') == 1
        Pmax = BaseConf(m).Pmax;
    else
        Pmax = 10;%Nominal power output by the PV for nominal irradiance
    end

    Pg = abs(ST(m).Sn).*ST(m).PF; %load power in W
    P_obj = - ST(m).Sn.*ST(m).PF;%inverter generated active power in W
    % generation of test data
    %test_case = 'genereting dynamic model'
    [DM,SD] = Dynamic_Model_gen(ST(m).Nnode,ST(m).topology,ST(m).grid_form,...
        ST(m).grid_supp_PV,ST(m).grid_supp_PQ,ST(m).plac_load,....
        P_obj,Pg,ST(m).f,ST(m).Vn,ST(m).kpv,ST(m).kiv,...
        ST(m).kpc,ST(m).kic,...
        ST(m).kp_PLL,ST(m).ki_PLL,...
        ST(m).rf,ST(m).rc,ST(m).Lf,ST(m).Cf,...
        Rcc,Lcc,rline,Lline,Gnom,Pmax,ST(m).PF,ST(m).Sn);
    [Test_SetUp,Combination_devices,Accuracy,GridData] = set_DSSE_input(DM,ST(m)); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE
    [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
    [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);

    ST(m).P_tot = PowerData.Pflow(1)*GridData.base_power;
    ST(m).Q_tot = PowerData.Qflow(1)*GridData.base_power;
    
    GridData.inj_status = 0;
    GridData.rm_column = 0;
    [W,GridData,R] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
    [H_IRI] = Jacobian_m_IRIDSSE(GridData);
    [H_VRI] = Jacobian_m_VRIDSSE(GridData);
    
    G1 = H_IRI'*W*H_IRI;
    G2 = H_VRI'*W*H_VRI;
    
    var_err_rectangular_mat_IRI = inv(G1);
    var_err_rectangular_mat_VRI = inv(G2);

    rotI = calc_rotI( GridData,PowerData,1);
    rotV = calc_rotV( GridData,PowerData,1);
    Rx_polar_VRI = rotV * var_err_rectangular_mat_VRI * rotV';
    diag_VRI = diag(var_err_rectangular_mat_VRI);
    diag_VAP = diag(Rx_polar_VRI);
    
    Rx_polar_IRI = rotI * var_err_rectangular_mat_IRI * rotI';
    diag_IRI = diag(var_err_rectangular_mat_IRI);
    diag_IAP = diag(Rx_polar_IRI);
    
    P_TI = var_err_rectangular_mat_IRI;

    for x = 1 : 2 + 2*GridData.Lines_num
        P_TI(:,x) = P_TI(:,x) .* ([GridData.base_voltage*ones(2,1);GridData.base_current*ones(2*GridData.Lines_num,1)] );
    end
    for x = 1 : 2 + 2*GridData.Lines_num
        P_TI(x,:) = P_TI(x,:) .* ([GridData.base_voltage*ones(2,1);GridData.base_current*ones(2*GridData.Lines_num,1)] )';
    end
        

    [~,Combination_devices1,Accuracy1,GridData1] = set_DSSE_Vstate(DM); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE
    
    GridData1.rm_column = 0;
    GridData1.inj_status = 0;
    ref = 0;
     
    [~,GridData1,~] = Weight_m(GridData1,PowerData,Combination_devices1,Accuracy1);
    [H_IRI1] = Jacobian_m_IRIDSSE(GridData1);
    [H_VRI1] = Jacobian_m_VRIDSSE(GridData1);
    
    
    use_pu = 0;
    rotI = calc_rotI( GridData,PowerData,use_pu);
    rotV = calc_rotV( GridData,PowerData,use_pu);

    P_TI_diag = diag(P_TI);
    
    if DM.PCC == 1
        P(m).P_TI_I_D(1,1) = P_TI_diag(2 + 1,1);
        P(m).P_TI_I_Q(1,1) = P_TI_diag(2 + 2,1);
    end
    for x = DM.PCC + 1 : DM.PCC + DM.Nline
        P(m).P_TI_I_D(x,1) = P_TI_diag(2 + 2*(x-1)+1,1);
        P(m).P_TI_I_Q(x,1) = P_TI_diag(2 + 2*(x-1)+2,1);
    end
	unc(m).unc_I_D = (P(m).P_TI_I_D(1:DM.PCC + DM.Nline,1).^0.5);% ./ (PowerData.Imagn*GridData.base_current);
    unc(m).unc_I_Q = (P(m).P_TI_I_Q(1:DM.PCC + DM.Nline,1).^0.5);% ./ (PowerData.Imagn*GridData.base_current);
    
    Rx_polar_IRI = rotI * P_TI * rotI';
    var_err_polar_mat_IRI = diag(Rx_polar_IRI);

    for x = 1 : GridData.Lines_num
        P(m).P_TI_I_A(x,1) = var_err_polar_mat_IRI(2+2*(x-1)+1);
        P(m).P_TI_I_P(x,1) = var_err_polar_mat_IRI(2+2*(x-1)+2);
    end
    unc(m).unc_I_A = (P(m).P_TI_I_A(1:DM.PCC + DM.Nline,1).^0.5);% ./ (PowerData.Imagn*GridData.base_current);
    unc(m).unc_I_P =  P(m).P_TI_I_P(1:DM.PCC + DM.Nline,1).^0.5;

    P_V = H_IRI1 * var_err_rectangular_mat_IRI * H_IRI1';

    for x = 1 : 2*GridData.Nodes_num
        P_V(:,x) = P_V(:,x) .* ([GridData.base_voltage*ones(2*GridData.Nodes_num,1)] );
    end
    for x = 1 : 2*GridData.Nodes_num
        P_V(x,:) = P_V(x,:) .* ([GridData.base_voltage*ones(2*GridData.Nodes_num,1)] )';
    end

    P_V_diag = diag(P_V);
    for x = 1 : GridData.Nodes_num 
        P(m).P_TI_V_D(x,1) = P_V_diag( 2*(x-1) + 1);
        P(m).P_TI_V_Q(x,1) = P_V_diag( 2*(x-1) + 2);
    end
    unc(m).unc_V_D = (P(m).P_TI_V_D(1:DM.PCC + DM.Nnode,1).^0.5);% ./ (PowerData.Vmagn*GridData.base_voltage);
    unc(m).unc_V_Q = (P(m).P_TI_V_Q(1:DM.PCC + DM.Nnode,1).^0.5);% ./ (PowerData.Vmagn*GridData.base_voltage);
    
    Rx_polar_VRI = rotV * P_V * rotV';
    var_err_polar_mat_VRI = diag(Rx_polar_VRI);
    for x = 1 : GridData.Nodes_num
        P(m).P_TI_V_A(x,1) = var_err_polar_mat_VRI(2*(x-1)+1);
        P(m).P_TI_V_P(x,1) = var_err_polar_mat_VRI(2*(x-1)+2);
    end
    unc(m).unc_V_A = (P(m).P_TI_V_A(1:GridData.Nodes_num,1).^0.5);% ./ (PowerData.Vmagn*GridData.base_voltage);
    unc(m).unc_V_P =  P(m).P_TI_V_P(1:GridData.Nodes_num,1).^0.5;
    
    Test_rep_IDQ = [Test_rep_IDQ;[max(unc(m).unc_I_D),max(unc(m).unc_I_Q)]];
    Test_rep_IAP = [Test_rep_IAP;[max(unc(m).unc_I_A),max(unc(m).unc_I_P)]];
    Test_rep_VDQ = [Test_rep_VDQ;[max(unc(m).unc_V_D),max(unc(m).unc_V_Q)]];
    Test_rep_VAP = [Test_rep_VAP;[max(unc(m).unc_V_A),max(unc(m).unc_V_P)]];
 
    
end



conf_test = [1:N_conf]


