%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function populates a structure with the main data of the dynamic
% model. The input is the number of nodes, nominal frequency and voltage
% amplitude
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Statespacemodeldistribution
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DM,SD,CLINE_tot] = Dynamic_Model_gen(Nnodes,topology,grid_form,grid_supp_PV,grid_supp_PQ,plac_load,P_obj,Pg,f,Vn,kpv,kiv,kpc,kic,kp_PLL,ki_PLL,rf,rc,Lf,Cf,Rcc,Lcc,rline,Lline,Gnom,Pmax,PF,Sn)

% fixed parameters
wPLL = 2*pi*f;  %PLL initial measured frequency
wc_PLL = 7853.98; %cut-frequency PLL - not needed
wc = 50.26; %cut frequency of average power calculation block
m = (1e-3) ; %(w_max - w_min)/Qmax
n = (1e-3) ; %(V_max - V_min)/Pmax
G = 1000/Gnom;%Initial irradiance, in pu


DM = struct;
DM.topology = topology;
DM.nodes     =     [1:Nnodes];
DM.grid_form =     grid_form;%insert inverters id in incremental order, 0 if not present
DM.grid_supp_PV =  grid_supp_PV; %position of grid forming inverters, the other are grid supporting inverters
DM.grid_supp_PQ =  grid_supp_PQ;
DM.plac_load    = plac_load;%insert loads id (1 if present 0 if not present)
DM.PCC = 1; %if zero the microgrid is deconnected, if 1 it is connected to PCC

DM.grid_supp = DM.grid_supp_PV + DM.grid_supp_PQ; %grid supporting nodes
DM.plac_inv_tot = DM.grid_supp + DM.grid_form;  %nodes with inverter placement
DM.ref = find(DM.plac_inv_tot~=0,1); %ref is the reference node
if isempty(DM.ref) == 1
    DM.ref = 1;
end
DM.Ninv = nnz(DM.grid_form)+nnz(DM.grid_supp);
DM.inv_count = find(DM.grid_supp + DM.grid_form,1,'last');
DM.NGF = nnz(DM.grid_form); %
DM.NGS = nnz(DM.grid_supp);
DM.Nload = nnz(DM.plac_load);
DM.Nnode = size(DM.nodes,2);
DM.Nline = size(DM.topology,2);

%general parameters
DM.f = f;
DM.Vn = Vn;
DM.Sn = Sn; %0.01*Sn
DM.wn = 2*pi*f; %general control parameters
DM.Eod = 0;
DM.Eoq = Vn;
% parameters of GF and GS inverters
if length(kpv) == 1
    DM.kp_PLL = kp_PLL*ones(DM.Nnode,1);
    DM.ki_PLL = ki_PLL*ones(DM.Nnode,1);
    DM.kpv_d = kpv*ones(DM.Nnode,1);
    DM.kpv_q = kpv*ones(DM.Nnode,1);
    DM.kiv_d = kiv*ones(DM.Nnode,1);
    DM.kiv_q = kiv*ones(DM.Nnode,1);
    DM.kpc_d = kpc*ones(DM.Nnode,1);
    DM.kpc_q = kpc*ones(DM.Nnode,1);
    DM.kic_d = kic*ones(DM.Nnode,1);
    DM.kic_q = kic*ones(DM.Nnode,1);
    DM.Lf = Lf*ones(DM.Nnode,1);
    DM.Cf = Cf*ones(DM.Nnode,1);
    DM.rf  = rf*ones(DM.Nnode,1);
    DM.rc  = rc*ones(DM.Nnode,1);
else
    DM.kp_PLL = kp_PLL;
    DM.ki_PLL = ki_PLL;
    DM.kpv_d = kpv;
    DM.kpv_q = kpv;
    DM.kiv_d = kiv;
    DM.kiv_q = kiv;
    DM.kpc_d = kpc;
    DM.kpc_q = kpc;
    DM.kic_d = kic;
    DM.kic_q = kic;
    DM.Lf = Lf;
    DM.Cf = Cf;
    DM.rf  = rf;
    DM.rc  = rc;
end

DM.Lc = 0*ones(DM.Nnode,1);
DM.Rd  = 0  *ones(DM.Nnode,1);

DM.wc = wc*ones(DM.Nnode,1);
DM.wc_PLL = wc_PLL *ones(DM.Nnode,1);
DM.wPLL = wPLL*ones(DM.Nnode,1); %377
%GF inverters
DM.m = m *ones(DM.Nnode,1);
DM.n = n *ones(DM.Nnode,1);
DM.Voqn = Vn *ones(DM.Nnode,1);
%GS inverters
if length(P_obj) == 1
    DM.P_obj = P_obj*ones(DM.Nnode,1);%objective power for case with PQ - GS
    DM.Q_obj = P_obj.*tan(acos(PF))*ones(DM.Nnode,1);%objective power for case with PQ - GS
else
    DM.P_obj = P_obj;
    DM.Q_obj = P_obj.*tan(acos(PF));
end
DM.Gnom = Gnom; %case with PV - GS
DM.G = G;
DM.Pmax = Pmax* ones(DM.Nnode,1); % nominal power of GS units
if length(PF) == 1
    DM.PF = PF .* ones(DM.Nnode,1); %power factor for the inverters
else
    DM.PF = PF;
end
%loads
if length(Pg) == 1
    DM.Pg = Pg*ones(DM.Nnode,1);% objective power for loads
    DM.Qg = DM.Pg.*tan(acos(DM.PF));% objective power for loads
else
    DM.Pg = Pg;
    DM.Qg = DM.Pg.*tan(acos(DM.PF));
end
DM.Rcc = Rcc;
DM.Lcc = Lcc;
DM.Lline = Lline;
DM.rline = rline;

[SD] = calc_SD( DM ); %5; calculate A and B matrixes of the systemm
[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
[~,~,~,~,~,~] = calc_eig(Atot);
[ Tmatrix,Forz_matrix,~] = calc_TDF(100,Atot,B_input,B_disturbance);

u = create_input(DM,0,1);
for w = 1 : 100
if w == 1
    x_status(:,1) =  SD.x_status ;
else
    x_status(:,1) = x_status(:,5);
end
x_status(:,2) = Tmatrix * x_status(:,1);
x_status(:,3) = Forz_matrix * u;
x_status(:,5) = x_status(:,2) + x_status(:,3) ;
end
SD.x_status = x_status(:,5);

end

