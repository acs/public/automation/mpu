%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate the inverter parameters based on switching
% frequency and nominal power
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ST] = configure_inverter_parameter(ST,m)

k = 0.866*0.9;

if length(ST(m).Sn) == 1
    
    ST(m).kp_PLL = 0.25; ST(m).ki_PLL = 2; %proportional and integral gain PLL
    ST(m).rf  = 0.1; %filter resistance
    ST(m).rc  = 0.1; %coupling resistance
    Vn = ST(m).Vn;
    % voltage and power, switching frequency and system frequency
    ST(m).Lf = 100*(Vn / k) / (16 * ST(m).fsw * 0.1 * (abs(ST(m).Sn)/Vn));%filter inductance %100*
    % Lf = Vdc / (16 * fsw * 0.1 * In)
    Zn = ((Vn / k)^2) / abs(ST(m).Sn);
    % Zn = ((Vn / k)^2)/ Sn);
    Cn = 1 / (2 * pi * 50 * Zn);
    ST(m).Cf = 100*0.05 * Cn;%Filter capacitance %100*
    
    check_sw = 0;
    % check if cut frequency is respected
    if check_sw == 1
        A = [-ST(m).rf/ST(m).Lf     2*pi*50           -1/ST(m).Lf            0;...
            -2*pi*50          -ST(m).rf/ST(m).Lf      0               -1/ST(m).Lf;...
            1/ST(m).Cf        0           -1/(Zn*ST(m).Cf)      2*pi*50;...
            0           1/ST(m).Cf        -2*pi*50              -1/(Zn*ST(m).Cf) ];
        
        B = blkdiag(1/ST(m).Lf, 1/ST(m).Lf, 1/ST(m).Cf,1/ST(m).Lf);
        C = zeros(2,4); C(1,3) = 1; C(2,4) = 1;
        D = zeros(2,4); D(1,3) = -ST(m).rc; D(2,4) = -ST(m).rc;
        
        syms s
        FTS = (D + C * ((s * eye(4) - A)^(-1) )* B);
        FTS11 = FTS(1,1);
        FTS12 = FTS(1,2);
        FTS11_str = char(FTS11);
        FTS12_str = char(FTS12);
        
        FTS21 = FTS(2,1);
        FTS22 = FTS(2,2);
        FTS21_str = char(FTS21);
        FTS22_str = char(FTS22);
        
        FTS13 = FTS(1,1);
        FTS14 = FTS(1,4);
        FTS13_str = char(FTS13);
        FTS14_str = char(FTS14);
        
        s = tf('s');
        
        eval(['FTS11_sys = ',FTS11_str])
        figure
        bode(FTS11_sys)
        fb11 = bandwidth(FTS11_sys)/(2*pi)
        
        eval(['FTS22_sys = ',FTS22_str])
        figure
        bode(FTS22_sys)
        fb22 = bandwidth(FTS22_sys)/(2*pi)
        
        if fb11 > 0.3*ST(m).fsw ||  fb22 > 0.3*ST(m).fsw
           warning('wrong cut frequency at LC filter') 
        end
    end
    
    
    
    wbc1 = 2*pi* ST(m).fsw/100;
    % wbc1 = 2 * pi * fsw / 100;
    wbc2 = wbc1/10;
    G_IlV1 = tf([ST(m).Cf*ST(m).rc 1],[ST(m).Lf*ST(m).Cf*ST(m).rf (ST(m).Lf + ST(m).Cf*ST(m).rc*ST(m).rf) (ST(m).rc + ST(m).rf)]);
    opt = pidtuneOptions('PhaseMargin',60);
    [C_c] = pidtune(G_IlV1,'PI',wbc1,opt);
    kpc = C_c.Kp;
    kic = C_c.Ki;
    G_I0Il = tf([1],[ST(m).Cf*ST(m).rc 1]);
    GIs = tf([kpc kic],[1 0]); % current control TF
    H2 = GIs*G_IlV1/(1+GIs*G_IlV1); % current control + plant and feedback TF
    H2plant =  Vn * H2 * G_I0Il; % Current control + Capacitance
    opt = pidtuneOptions('PhaseMargin',30);
    [C_p,~] = pidtune(H2plant,'PI',wbc2,opt);
    kpv = C_p.Kp;
    kiv = C_p.Ki;
    
    ST(m).kpc = kpc;
    ST(m).kic = kic;
    ST(m).kpv = kpv;
    ST(m).kiv = kiv;
    
else
    Vn = ST(m).Vn;
    for h = 1 : length(ST(m).Sn)
    ST(m).kp_PLL(h) = 0.25; ST(m).ki_PLL(h) = 2; %proportional and integral gain PLL
    ST(m).rf(h)  = 0.1; %filter resistance
    ST(m).rc(h)  = 0.1; %coupling resistance
    
    % voltage and power, switching frequency and system frequency
    ST(m).Lf(h) = (Vn / k) / (16 * ST(m).fsw * 0.1 * (abs(ST(m).Sn(h))/Vn));%filter inductance
    % Lf = Vdc / (16 * fsw * 0.1 * In)
    Zn = ((Vn / k)^2) / abs(ST(m).Sn(h));
    % Zn = ((Vn / k)^2)/ Sn);
    Cn = 1 / (2 * pi * 50 * Zn);
    ST(m).Cf(h) =  0.05 * Cn;%Filter capacitance
    
    wbc1 = 2*pi* ST(m).fsw/100;
    % wbc1 = 2 * pi * fsw / 100;
    wbc2 = wbc1/10;
    G_IlV1 = tf([ST(m).Cf(h)*ST(m).rc(h) 1],[ST(m).Lf(h)*ST(m).Cf(h)*ST(m).rf(h) (ST(m).Lf(h) + ST(m).Cf(h)*ST(m).rc(h)*ST(m).rf(h)) (ST(m).rc(h) + ST(m).rf(h))]);
    opt = pidtuneOptions('PhaseMargin',60);
    [C_c] = pidtune(G_IlV1,'PI',wbc1,opt);
    kpc = C_c.Kp;
    kic = C_c.Ki;
    G_I0Il = tf([1],[ST(m).Cf(h)*ST(m).rc(h) 1]);
    GIs = tf([kpc kic],[1 0]); % current control TF
    H2 = GIs*G_IlV1/(1+GIs*G_IlV1); % current control + plant and feedback TF
    H2plant =  Vn * H2 * G_I0Il; % Current control + Capacitance
    opt = pidtuneOptions('PhaseMargin',30);
    [C_p,~] = pidtune(H2plant,'PI',wbc2,opt);
    kpv = C_p.Kp;
    kiv = C_p.Ki;
    
    ST(m).kpc(h) = kpc;
    ST(m).kic(h) = kic;
    ST(m).kpv(h) = kpv;
    ST(m).kiv(h) = kiv;
    end
end
end

