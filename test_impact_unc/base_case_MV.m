%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script with base case parameter of a MV grid
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unc_disturb_Eo = 0.10;
unc_disturb_G  = 0.01;
unc_distur_PQ  = 0.50;
dev_input      = 0.0;
dev_status     = 0.0;

BaseConf.Nnode = 16;
BaseConf.Nline = 2;
BaseConf.PQRL = 0.5;
%BaseConf.Sn = 4*5e5/BaseConf.Nnode;
BaseConf.Sn = 4*50000; %10*

BaseConf.f = 50;
BaseConf.fsw = 10000;
BaseConf.PF = 0.95; %Power Factor, applied to RL loads and PV inverters
BaseConf.Vn = 20000;

%%% to calculate the Rcc and Lcc
Vn_HV = 110*10^3;
Sn_HV = 5000*10^6;
Rcc1 = sqrt(1/11)*(Vn_HV^2)/(Sn_HV);

Rcc1 = Rcc1/((Vn_HV/BaseConf.Vn)^2);
Lcc1 = 10*Rcc1/(2*pi*50);
BaseConf.Rcc = Rcc1 + 0.016;
BaseConf.Lcc = Lcc1 + 1.92/(2*pi*50);

check_eig = 0;
if check_eig == 1
    Rl = BaseConf.Vn^2 / (BaseConf.Sn*BaseConf.Nnode);
    A = [-(BaseConf.Rcc+Rl)/BaseConf.Lcc     2*pi*50          ;...
        -2*pi*50          -(BaseConf.Rcc+Rl)/BaseConf.Lcc      ];
    B = blkdiag(1/BaseConf.Lcc, 1/BaseConf.Lcc);
    C = zeros(2,2); C(1,1) = Rl; C(2,2) = Rl;
    D = zeros(2,2);
    syms s
    eig(A)
    abs(eig(A)).^-1
    
    FTS = (D + C * ((s * eye(2) - A)^(-1) )* B);
    FTS11 = FTS(1,1);
    
    FTS11_str = char(FTS11);
    FTS22 = FTS(2,2);
    
    FTS22_str = char(FTS22);
    
    
    s = tf('s');
    
    eval(['FTS11_sys = ',FTS11_str])
    figure(1)
    bode(FTS11_sys)
    fb11 = bandwidth(FTS11_sys)/(2*pi)
    
    eval(['FTS22_sys = ',FTS22_str])
    figure(4)
    bode(FTS22_sys)
    fb22 = bandwidth(FTS22_sys)/(2*pi)
    

end

%%%
%%% to calculate line impedance
rline_km = (1/3)*(2*0.510+0.658); %Line resistance per km
Lline_km = (1/3)*(2*0.366+1.611)/(2*pi*50);%Line inductance per km

if check_eig == 1
    Rl = BaseConf.Vn^2 / (BaseConf.Sn*BaseConf.Nnode);
    A = [-(rline_km+Rl)/Lline_km     2*pi*50          ;...
        -2*pi*50          -(rline_km+Rl)/Lline_km      ];
    B = blkdiag(1/Lline_km, 1/Lline_km);
    C = zeros(2,2); C(1,1) = Rl; C(2,2) = Rl;
    D = zeros(2,2);
    syms s
    eig(A)
    abs(eig(A)).^-1
    
    FTS = (D + C * ((s * eye(2) - A)^(-1) )* B);
    FTS11 = FTS(1,1);
    
    FTS11_str = char(FTS11);
    FTS22 = FTS(2,2);
    
    FTS22_str = char(FTS22);
    
    
    s = tf('s');
    
    eval(['FTS11_sys = ',FTS11_str])
    figure(1)
    bode(FTS11_sys)
    fb11 = bandwidth(FTS11_sys)/(2*pi)
    
    eval(['FTS22_sys = ',FTS22_str])
    figure(4)
    bode(FTS22_sys)
    fb22 = bandwidth(FTS22_sys)/(2*pi)
    

end

length_km = 1;
%%%
BaseConf.rline = rline_km * length_km;
BaseConf.Lline = Lline_km * length_km;


BaseConf.PQflow = [];
BaseConf.Pseudo = [2:BaseConf.Nnode+1;zeros(1,BaseConf.Nnode)];
BaseConf.PQ = [];
BaseConf.V = [1;0];
BaseConf.I = [1;0];
BaseConf.unc_dev =  0.01/3;
BaseConf.unc_pseudo = 0.5/3;

BaseConf.delay = 0.1;

BaseConf.colored = 0.99; %0;
BaseConf.Ts = 1; %1e6;
BaseConf.filter = 1; %0;
BaseConf.dist = 1;