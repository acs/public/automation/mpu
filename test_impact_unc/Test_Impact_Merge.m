%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to extract from data stored the scenarios for the impact
% analysis
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Test_rep_IDQ,Test_rep_VDQ,Test_rep_IAP,Test_rep_VAP,x_axis] = Test_Impact_Merge(BaseConf,type_test,TI,TD)

if type_test < 18
x_vector = [BaseConf.Nnode;BaseConf.Nline;BaseConf.Sn;(BaseConf.rline^2+BaseConf.Lline^2)^0.5; BaseConf.PQRL; BaseConf.fsw; BaseConf.Ts; BaseConf.colored; BaseConf.PF; size(BaseConf.V,2);...
    size(BaseConf.I,2); size(BaseConf.PQ,2); size(BaseConf.PQflow,2); BaseConf.unc_dev; BaseConf.unc_pseudo; BaseConf.delay; BaseConf.dist];
x_vector(type_test) = 99999; %dummy
n_test = type_test;

elseif type_test == 18 %delay
x_vector = [BaseConf.Nnode;BaseConf.Nline;BaseConf.Sn;(BaseConf.rline^2+BaseConf.Lline^2)^0.5; BaseConf.PQRL; BaseConf.fsw; BaseConf.Ts; BaseConf.colored; BaseConf.PF; size(BaseConf.V,2);...
    size(BaseConf.I,2); size(BaseConf.PQ,2); size(BaseConf.PQflow,2); BaseConf.unc_dev; BaseConf.unc_pseudo; BaseConf.delay; BaseConf.dist];
n_test = 16;
x_vector(n_test) = 99999; %dummy
    
elseif type_test == 23 %negative power factor
    x_vector = [BaseConf.Nnode;BaseConf.Nline;BaseConf.Sn;(BaseConf.rline^2+BaseConf.Lline^2)^0.5; 1; BaseConf.fsw; BaseConf.Ts; BaseConf.colored; BaseConf.PF; size(BaseConf.V,2);...
        size(BaseConf.I,2); size(BaseConf.PQ,2); size(BaseConf.PQflow,2); BaseConf.unc_dev; BaseConf.unc_pseudo; BaseConf.delay; BaseConf.dist];
    n_test = 3;
    x_vector(n_test) = 99999; %dummy
end

if TI == 1
    Scenario = load(strcat(pwd,'\Results\Comparison_TI_test_type_',num2str(type_test),'\Result_Test'));
else
    Scenario = load(strcat(pwd,'\Results\Comparison_TD_test_type_',num2str(type_test),'\Result_Test'));
end



Test_rep_VDQ = []; Test_rep_IDQ = []; Test_rep_VAP = []; Test_rep_IAP = []; 
Ntest = 0;

for g = 1 : length(Scenario.conf_test)
    vect_parameters_temp = [...
        Scenario.ST(g).Nnode;...
        Scenario.ST(g).Nline;...
        Scenario.ST(g).Sn;...
        (Scenario.ST(g).rline^2+Scenario.ST(g).Lline^2)^0.5;...
        Scenario.ST(g).PQRL;...
        Scenario.ST(g).fsw; ...
        Scenario.ST(g).Ts; ...
        Scenario.ST(g).colored;...
        Scenario.ST(g).PF;...
        size(Scenario.ST(g).V,2);...
        size(Scenario.ST(g).I,2);...
        size(Scenario.ST(g).PQ,2); ...
        size(Scenario.ST(g).PQflow,2);...
        Scenario.ST(g).unc_dev; ...
        Scenario.ST(g).unc_pseudo;...
        Scenario.ST(g).delay;
        Scenario.ST(g).dist
    ];
    
    if abs(vect_parameters_temp(5,1)- x_vector(5,1))< 1e-15 %resistances and inductances may be just slightly different
        vect_parameters_temp(5,1) = x_vector(5,1);
    end
    
    index_eq = (vect_parameters_temp == x_vector);
    if index_eq(floor(n_test)) == 0  %if the variable of type_x varies
        if sum(index_eq) == 16 %if the other 5 are like in x vector
            Ntest = Ntest + 1;
            x_axis(Ntest,1) = vect_parameters_temp(n_test);
            Test_rep_VDQ   =  [Test_rep_VDQ;Scenario.Test_rep_VDQ(g,1:end)];
            Test_rep_IDQ   =  [Test_rep_IDQ;Scenario.Test_rep_IDQ(g,1:end)];
            Test_rep_VAP =  [Test_rep_VAP;Scenario.Test_rep_VAP(g,1:end)];
            Test_rep_IAP =  [Test_rep_IAP;Scenario.Test_rep_IAP(g,1:end)];
        end
    end
end


[x_axis,index_x] = sort(x_axis);
Test_rep_VAP = Test_rep_VAP(index_x,:);
Test_rep_IAP = Test_rep_IAP(index_x,:);
Test_rep_VDQ = Test_rep_VDQ(index_x,:);
Test_rep_IDQ = Test_rep_IDQ(index_x,:);

if TI == 1
    Test_rep_IAP(:,2) = 100*Test_rep_IAP(:,2);
    Test_rep_VAP(:,2) = 100*Test_rep_VAP(:,2);
else
    Test_rep_IAP(:,2) = 100*Test_rep_IAP(:,2);
    Test_rep_VAP(:,2) = 100*Test_rep_VAP(:,2);
    Test_rep_IAP(:,4) = 100*Test_rep_IAP(:,4);
    Test_rep_VAP(:,4) = 100*Test_rep_VAP(:,4);
end

end