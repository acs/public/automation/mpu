%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to run the test to model and test the uncertainty of a base case
% MV and LV distribution grid
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
format longg

print_figure = 1;
unc_disturb_Eo = 0.10;
unc_disturb_G  = 0.01;
unc_distur_PQ  = 0.50;
dev_input      = 0.0;
dev_status     = 0.0;

Ts = 1e6; %time resolution of inputs and disturbances 1 second
filter_on = 0; %with colored noise
Ntot = 4; %evenly spaced among decades
t_delta_in = -4 ; t_delta_fin = 2; %so is 10 intervals per decade


Nw = 10;
time_test = logspace(t_delta_in,t_delta_fin,Ntot);
N_MC = 10000;

%run short_case_MV
run base_case_MV
%run base_case_LV

%0,2,2.1,3,3.1,6,7,8,8.1,9,9.1
%0,
type_unc = [2,2.1,3,3.1,6,7,8,8.1,9,9.1];
type_unc = 0
[ST,N_conf] = generate_conf_mpu_test(BaseConf,type_unc);


for m = 1 : N_conf
    ST(m).Script
    [ST] = configure_inverter_parameter(ST,m);
    Rcc = ST(m).Rcc; %Common coupling resistance (between bus 0 and 1)
    Lcc = ST(m).Lcc;%Common coupling inductance (between bus 0 and 1)
    rline =  ST(m).rline*ones(ST(m).Nnode-1,1); %Line resistance
    Lline =  ST(m).Lline*ones(ST(m).Nnode-1,1);%Line inductance
    
    Gnom = 1000;%Nominal Irradiance for PV inverters
    Pmax = 10;%Nominal power output by the PV for nominal irradiance
    Pg = abs(ST(m).Sn)*ST(m).PF; %load power in W
    P_obj = - ST(m).Sn*ST(m).PF;%inverter generated active power in W

    [DM,SD,CLINE_tot] = Dynamic_Model_gen(ST(m).Nnode,ST(m).topology,ST(m).grid_form,...
        ST(m).grid_supp_PV,ST(m).grid_supp_PQ,ST(m).plac_load,....
        P_obj,Pg,ST(m).f,ST(m).Vn,ST(m).kpv,ST(m).kiv,...
        ST(m).kpc,ST(m).kic,...
        ST(m).kp_PLL,ST(m).ki_PLL,...
        ST(m).rf,ST(m).rc,ST(m).Lf,ST(m).Cf,...
        Rcc,Lcc,rline,Lline,Gnom,Pmax,ST(m).PF,ST(m).Sn);
    

[Test_SetUp,Combination_devices,Accuracy,GridData] = set_DSSE_input(DM,ST(m)); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE

% ST(m).Lf
% ST(m).Cf
% ST(m).kpv
% ST(m).kiv
% ST(m).kpc
% ST(m).kic

%     if isfield(ST(m),'colored') == 1
%         if isempty(ST(m).colored) == 0
%         DM.B_Eo = ST(m).colored;
%         DM.B_G = ST(m).colored;
%         DM.B_PQ_MV = ST(m).colored;
%         DM.B_PQ_LV = ST(m).colored;
%         end
%     end 
[VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy,ST(m).filter);
[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
%load('SimulationData_10.2.mat')

Ntot1 = Ntot;
if type_unc < 1
 Ntot1 = 1;
end

if ST(m).filter == 1
    Vref = ST(m).colored;
    Tref = ST(m).Ts;
    [diag_ratio,V] = update_Vdisturbance(Vref,Tref,time_test)
end

    
UncDM = struct; 
UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Ntot1);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Ntot1);unc_estimated_i_lineA0=zeros(1,Ntot1);unc_estimated_i_lineP0=zeros(1,Ntot1);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Ntot1);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Ntot1);unc_system_i_lineA0 =zeros(1,Ntot1);unc_system_i_lineP0 =zeros(1,Ntot1);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Ntot1);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Ntot1);unc_estimated_i_lineD0=zeros(1,Ntot1);unc_estimated_i_lineQ0=zeros(1,Ntot1);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Ntot1);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Ntot1);unc_system_i_lineD0 =zeros(1,Ntot1);unc_system_i_lineQ0 =zeros(1,Ntot1);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Ntot1);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Ntot1);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Ntot1);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Ntot1);
    
for t = 1 : Ntot1
    time_delta = time_test(t)
    if ST(m).filter == 1
        [VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy,ST(m).filter);
        VS.var_disturbance_absolute = diag_ratio(t)*VS.var_disturbance_absolute;
        VS.P_delta_x = diag_ratio(t)*VS.P_delta_x;
    end
    [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
        uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,ST(m).type_unc,ST(m).Ts,Nw,ST(m).filter,Accuracy);
    [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
        unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
        uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
        ST(m).type_unc,time_delta,ST(m).Ts,ST(m).filter,Nw);
end
UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
save_dir =strcat(pwd,'\Results\ComparisonDSSE_MV_',ST(m).Script); %,datestr( datetime('now'),'yyyymmddhhMMss')
mkdir(save_dir);save_cat = strcat(save_dir,'\UncDM_',ST(m).Script);
save(save_cat,'UncDM');
if Ntot1 > 1
    plot_3fig_uncertainty(UncDM.unc_estimated_i_lineD,UncDM.unc_estimated_i_lineQ,UncDM.unc_system_i_lineD,UncDM.unc_system_i_lineQ,UncDM.unc_DSSE_emp_i_lineD,UncDM.unc_DSSE_emp_i_lineQ,UncDM.time_test,DM,save_dir)
end
run plot_mpu

end

