%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main Script to run the impact analysis test
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Test Pilot LV site: It performs analysis and design of a monitoring
% system in a LV network
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
clear all
close all


Ntot = 11^2; %evenly spaced among decades
t_delta_in = -8 ; 
t_delta_fin = 2; %so is 10 intervals per decade

time_test = logspace(t_delta_in,t_delta_fin,Ntot);


width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;

run Basecase_LV


%% Time Indipendent Uncertainty Preparation Data
type_test = [0,10,11];
for Ntest = 1 : length(type_test)
    display(strcat('Test TI number: ',num2str(type_test(Ntest))));
    [ST,N_conf] = generate_conf_pilot(BaseConf,type_test(Ntest));
    run Test_Impact_Scenarios_TI.m
    save_dir = strcat(pwd,'\Results\',text_in,'_TI_test_type_',num2str(type_test(Ntest)));
    mkdir(save_dir);
    save_cat = strcat(save_dir,'\Result_Test');
    save(save_cat,'Test_rep_IDQ','Test_rep_VDQ','Test_rep_IAP','Test_rep_VAP','ST','conf_test','P','unc');
end

%% Time Dependent Uncertainty Preparation Data
for Ntest = 1 : length(type_test)
    display(strcat('Test TD number: ',num2str(type_test(Ntest))));
    [ST,N_conf] = generate_conf_pilot(BaseConf,type_test(Ntest));
    run Test_Impact_Scenarios_TDcolor.m
    save_dir =strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(type_test(Ntest)));
    mkdir(save_dir);
    save_cat = strcat(save_dir,'\Metric_Test');
    save(save_cat,'ST','conf_test','P','unc','eigA_m','time_test');
end

%%
% Time Dependent Uncertainty Generation Indexes
for Ntest = 1 : length(type_test)
    Ntest
    save_dir =strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(type_test(Ntest)),'\Metric_Test');
    load(save_dir)
    run Generate_Test_Metrics_TD.m
    save_dir = strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(type_test(Ntest)),'\Result_Test');
    save(save_dir,'Test_rep_IDQ','Test_rep_VDQ','Test_rep_IAP','Test_rep_VAP','unc','conf_test','ST')
end



%%
Ts = 1;  % base_case LV
type_test = 0; %LV with less delay
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)

%%
Ts = 1; 
type_test = 10; %Comparison TI: base case vs improved
plot_combined_TI_comparison_latex_pilot_LV(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)
%%
Ts = 1; 
type_test = 10; %Comparison TD: base case vs improved
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)
%%
Ts = 0.1; 
type_test = 11; %Comparison TI: base case vs improved
plot_combined_TD_comparison_latex_pilot_LV(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)
%%
Ts = 0.1; 
type_test = 11; %Comparison TD: base case vs improved
plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,type_test)
