%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to test the DSSE in MC simulations
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [unc_i_lineA,unc_i_lineP,unc_i_lineD,unc_i_lineQ,unc_DSSE_emp_i_lineA,unc_DSSE_emp_i_lineP,unc_DSSE_emp_i_lineD,unc_DSSE_emp_i_lineQ] = ...
    uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,type_unc,time_delta,Ts,filter_on,Nw)
%     type_unc = 0; 'Steady State estimation and system'
%     type_unc = 1; 'Steady State estimation and dynamic system with StdDev'
%     type_unc = 2; 'Steady State estimation and dynamic system with RMSE'
%     type_unc = 3; 'Dynamic State estimation and dynamic system with RMSE'
%     type_unc = 4; 'Steady State estimation and dynamic system with delay batch measurements with RMSE'
%     type_unc = 5; 'Steady State estimation and dynamic system with recursive estimator and StdDev'
%     type_unc = 6; 'Steady State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 7; 'Dynamic State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 8; 'Steady State estimation and dynamic system with delay splitted measurements with RMSE'
%     type_unc = 9; 'Dynamic State estimation and dynamic system with delay splitted measurements with RMSE'

results_DSSE=struct; results_DSSE.err_Vmagnitude=[];results_DSSE.err_Vphase=[];results_DSSE.Vmagn_status=[];results_DSSE.Vph_status=[];results_DSSE.Imagn_status=[];results_DSSE.Iph_status=[];results_DSSE.Vreal_status=[];results_DSSE.Vimag_status=[];results_DSSE.Ireal_status=[];results_DSSE.Iimag_status=[];results_DSSE.Vmagn_true=[];results_DSSE.Vph_true=[];results_DSSE.Imagn_true=[];results_DSSE.Iph_true=[];results_DSSE.Vreal_true=[];results_DSSE.Vimag_true=[];results_DSSE.Ireal_true=[];results_DSSE.Iimag_true=[];results_DSSE.err_Imagnitude=[];results_DSSE.err_Iphase=[];results_DSSE.err_Ireal=[];results_DSSE.err_Iimag=[];results_DSSE.err_Vreal=[];results_DSSE.err_Vimag=[];results_DSSE.err_Vmagnitude_in   =[];results_DSSE.err_Vphase_in=[];results_DSSE.err_Imagnitude_in  =[];results_DSSE.err_Iphase_in   =[];results_DSSE.err_Ireal_in   =[];results_DSSE.err_Iimag_in =[];results_DSSE.err_inj_status =[];
err_state = zeros(size(SD.x_status,1),N_MC);
var_status_absolute = diag(VS.var_status_absolute); std_dev_status_absolute = sqrt(var_status_absolute);
ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;
[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);

[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
[W,GridData,R] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
[~,~,R1] = Weight_m_PQ(GridData,PowerData,Combination_devices,Accuracy);
[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);


P_fin2 = zeros(size(VS.var_status_absolute));
u = create_input(DM,dev_input,1);
if type_unc == 4
    time_delta = (1 +  Combination_devices.common_delay_base ) * time_delta ;
    
elseif type_unc == 6 %RMSE in dynamic conditions and static state estimation with recursive
    
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    P_fin4 = (Tmatrix-eye(size(Tmatrix))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  =  P_post + P_fin2 + P_fin4  ;
        else
            P_pre = diag(diag(P_pre));
            for x = 1 : GridData.Lines_num
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end
            
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF((w-1)*time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * diag(diag(VS.P_delta_x)) * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((u*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_fin4w(w).P_fin4 = (Tmatrix-eye(size(Tmatrix))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;
            %P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_post = diag((diag(P_pre).^-1 + diag(VS.Gain_matrix)).^-1);
            
            for x = 1 : GridData.Lines_num
                P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
            end
            if w == Nw
                P_pre  =  P_post ;
            else
                P_pre  =  P_post + P_fin2 + P_fin4w(w).P_fin4;
            end
        end
    end
    P_fin = P_pre;
    
elseif  type_unc == 7 %RMSE in dynamic conditions and dynamic state estimation  with recursive (KALMAN FILTER)
    
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute;
            P_pre  = Tmatrix * P_post * transpose(Tmatrix) + P_fin2  ;
        else
            P_pre = diag(diag(P_pre));
            for x = 1 : GridData.Lines_num
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end
            %P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_post = diag((diag(P_pre).^-1 + diag(VS.Gain_matrix)).^-1);
            
            for x = 1 : GridData.Lines_num
                P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
            end
            P_post = diag(diag(P_post));
            if w == Nw
                P_pre  =  P_post ;
            else
                P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
            end
        end
    end
    for x = 1 : GridData.Lines_num
        P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
        P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
    end
    P_fin = P_pre;
    
    
    
elseif type_unc == 8 %we have delay split in measurements with time_delta_meas vector
    time_delta_meas = [(sort(unique(Combination_devices.time_delta_meas),'descend')),0];
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(GridData.DelayMeas == time_delta_meas(m));
        [ Tmatrix_m(m).Tmatrix,Forz_matrix_m(m).Forz_matrix,Dist_matrix_m(m).Dist_matrix] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
    end
    
    
elseif type_unc == 9 %Dynamic case
    Rtot = zeros(size(VS.R));
    [C] = Jacobian_m_IRIDSSE(GridData);
    C(:,ref+1)=[];
    time_delta_meas = [(sort(unique(Combination_devices.time_delta_meas),'descend')),0];
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(GridData.DelayMeas == time_delta_meas(m));
        [ Tmatrix_m(m).Tmatrix,Forz_matrix_m(m).Forz_matrix,Dist_matrix_m(m).Dist_matrix] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
    end
end


for z = 1 : N_MC
    u = create_input(DM,dev_input,type_unc);
    if type_unc == 0 % steady state case
        x_status(:,1) = SD.x_status ;
    
    elseif type_unc == 1 || type_unc == 2 || type_unc == 3 || type_unc == 4
        
        [d] = create_disturbance(DM,VS);
        if type_unc == 1
            x_status(:,1) = SD.x_status;
        elseif type_unc == 2 || type_unc == 3 || type_unc == 4
            x_status(:,1) = SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
        end
        x_status(:,2) = Tmatrix * x_status(:,1);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
    
    elseif type_unc == 6 || type_unc == 7
        for w = 1 : Nw
            
            [d] = create_disturbance(DM,VS);
            if w == 1
                x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
            else
                x_status(:,1) = x_status(:,5);
            end
            x_status(:,2) = Tmatrix * x_status(:,1);
            x_status(:,3) = Forz_matrix * u;
            x_status(:,4) = Dist_matrix * d;
            x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
            
            [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
            [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
            Meas_vector_external(w).Meas_vector_external = mvnrnd(Meas_true_vector, R1)';
        end
        
        
    elseif type_unc == 8 || type_unc == 9
        [d] = create_disturbance(DM,VS);
        for m = 1 : length(t_delay)
            if m == 1
                x_status_m(m).x_status(:,1) = SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
            else
                x_status_m(m).x_status(:,1) =  x_status_m(m-1).x_status(:,5);
            end
            x_status_m(m).x_status(:,2) = Tmatrix_m(m).Tmatrix * x_status_m(m).x_status(:,1);
            x_status_m(m).x_status(:,3) = Forz_matrix_m(m).Forz_matrix * u;
            x_status_m(m).x_status(:,4) = Dist_matrix_m(m).Dist_matrix * d;
            x_status_m(m).x_status(:,5) = x_status_m(m).x_status(:,2) + x_status_m(m).x_status(:,3) + x_status_m(m).x_status(:,4);
        end
        %[d] = create_disturbance(DM,VS);
        x_status(:,1) = x_status_m(length(t_delay)).x_status(:,5);%
        x_status(:,2) = Tmatrix * x_status(:,1);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
    end
    
    if type_unc == 0
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        %Meas_vector_external = mvnrnd(Meas_true_vector, R)';
        Meas_vector_external = mvnrnd(Meas_true_vector, R1)';
        %[Vmagn_status,Vph_status,~,~,inj_status] = VRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [Vmagn_status,Vph_status,~,~,inj_status] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [results_DSSE] = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status);
    elseif type_unc == 1 || type_unc == 2 || type_unc == 4
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = mvnrnd(Meas_true_vector, R1)';
        %[Vmagn_status,Vph_status] = VRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [Vmagn_status,Vph_status,~,~,inj_status] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE] = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status);
    elseif type_unc == 3
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = mvnrnd(Meas_true_vector, R1)';
        %[Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status] = VRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [ Vmagn_status,Vph_status,~,~,inj_status ] = calc_xestimate( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE] = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status);
        
        
    elseif type_unc == 6
        
        for w = 1 : Nw
            if w == 1
                [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post]  = IRIDSSE(Meas_vector_external(w).Meas_vector_external,W,GridData,Test_SetUp);
                Vmagn_status_in = Vmagn_status;
                Vph_status_in = Vph_status;
                Imagn_status_in = Imagn_status;
                Iph_status_in = Iph_status;
                inj_status_in = inj_status;
                for x = 1 : GridData.Lines_num
                    P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                    P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
                end %firstly we put it in the proper shape
                P_pre = P_post + P_fin2 + P_fin4;
                for x = 1 : GridData.Lines_num
                    P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                    P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
                end %then we reinvert it to use it in the estimator
                
            else
                %P_pre = diag(diag(P_pre));
                [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post] = IRIDSSE_rec(Meas_vector_external(w).Meas_vector_external,...
                    W,GridData,Test_SetUp,Imagn_status_in,Iph_status_in,inj_status_in,P_pre);
                Vmagn_status_in = Vmagn_status;
                Vph_status_in = Vph_status;
                Imagn_status_in = Imagn_status;
                Iph_status_in = Iph_status;
                inj_status_in = inj_status;
                
                for x = 1 : GridData.Lines_num
                    P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                    P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
                end %firstly we put it in the proper shape
                P_pre = P_post + P_fin2 + P_fin4w(w).P_fin4;
                for x = 1 : GridData.Lines_num
                    P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                    P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
                end %then we reinvert it to use it in the estimator
            end
        end
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE] = Data_Output(Vmagn_status_in,Vph_status_in,results_DSSE,GridData,PowerData,inj_status);
        
        
    elseif type_unc == 7
        for w = 1 : Nw
            if w == 1
                [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post]  = IRIDSSE(Meas_vector_external(w).Meas_vector_external,W,GridData,Test_SetUp);
                [ Vmagn_status_in,Vph_status_in,Imagn_status_in,Iph_status_in,inj_status_in ] = calc_xestimate( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
                
                for x = 1 : GridData.Lines_num
                    P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                    P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
                end %firstly we put it in the proper shape
                P_pre = Tmatrix *P_post* transpose(Tmatrix) + P_fin2;
                for x = 1 : GridData.Lines_num
                    P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                    P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
                end %then we reinvert it to use it in the estimator
            else
                P_pre = diag(diag(P_pre));
                [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post] = IRIDSSE_rec(Meas_vector_external(w).Meas_vector_external,...
                    W,GridData,Test_SetUp,Imagn_status_in,Iph_status_in,inj_status_in,P_pre);
                [ Vmagn_status_in,Vph_status_in,Imagn_status_in,Iph_status_in,inj_status_in ] = calc_xestimate( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
                for x = 1 : GridData.Lines_num
                    P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                    P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
                end %firstly we put it in the proper shape
                P_post = diag(diag(P_post));
                P_pre = Tmatrix *P_post* transpose(Tmatrix) + P_fin2;
                for x = 1 : GridData.Lines_num
                    P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                    P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
                end %then we reinvert it to use it in the estimator
            end
        end
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE] = Data_Output(Vmagn_status_in,Vph_status_in,results_DSSE,GridData,PowerData,inj_status);
        
        
    elseif type_unc == 8
        Meas_vector_external = zeros(size(R,1),1);
        for m = 1 : length(t_delay)
            [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status_m(m).x_status(:,1));
            [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
            Meas_vector_external_temp = mvnrnd(Meas_true_vector, R1)';
            Meas_vector_external(m_meas(m).meas,1) = Meas_vector_external_temp(m_meas(m).meas,1);
        end
        [Vmagn_status,Vph_status,~,~,inj_status] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE] = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status);
        
    elseif type_unc == 9
        
        Meas_vector_external = zeros(size(R,1),1);
        for m = 1 : length(t_delay)
            [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status_m(m).x_status(:,1));
            [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
            Meas_vector_external_temp = mvnrnd(Meas_true_vector, R1)';
            Meas_vector_external(m_meas(m).meas,1) = Meas_vector_external_temp(m_meas(m).meas,1);
        end
        [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [Vmagn_status,Vph_status,~,~,inj_status ] = calc_xestimate( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE] = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status);
    end
    
    
    if type_unc == 0
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(ref + 2*DM.PCC + 2*DM.Nline,1);
        err_state(:,z) = (x_status_corrupt(:,1) - x_status(:,1));
    elseif type_unc == 1 || type_unc == 2 || type_unc == 4
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(ref + 2*DM.PCC + 2*DM.Nline,1);
        err_state(:,z) = (x_status_corrupt(:,1) - x_status(:,5));
    elseif type_unc == 3
        u = create_input(DM,dev_input,1);
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(ref + 2*DM.PCC + 2*DM.Nline,1);
        x_status_corrupt(:,2) = Tmatrix * x_status_corrupt(:,1);
        x_status_corrupt(:,3) = Forz_matrix * u;
        x_status_corrupt(:,5) = x_status_corrupt(:,2) + x_status_corrupt(:,3) ;
        err_state(:,z) = (x_status_corrupt(:,5) - x_status(:,5));
    elseif type_unc == 6
        x_status_corrupt(:,1) = x_status(:,1) + (diag(P_fin).^0.5).*randn(size(SD.x_status,1),1); %it is the last initial status of the window Nw
        err_state(:,z) = (x_status_corrupt(:,1) - x_status(:,5));
    elseif type_unc == 7
        u = create_input(DM,dev_input,1);
        
        %x_status_corrupt(:,1) =  mvnrnd(x_status(:,1), P_fin)';
        x_status_corrupt(:,1) = x_status(:,1) + (diag(P_fin).^0.5).*randn(ref + 2*DM.PCC + 2*DM.Nline,1); %it is the last initial status of the window Nw
        
        x_status_corrupt(:,2) = Tmatrix * x_status_corrupt(:,1);
        x_status_corrupt(:,3) = Forz_matrix * u;
        x_status_corrupt(:,5) = x_status_corrupt(:,2) + x_status_corrupt(:,3) ;
        err_state(:,z) = (x_status_corrupt(:,5) - x_status(:,5));
    elseif type_unc == 8  %not accurate modeling
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(ref + 2*DM.PCC + 2*DM.Nline,1);
        err_state(:,z) = (x_status_corrupt(:,1) - x_status(:,5));
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(ref + 2*DM.PCC + 2*DM.Nline,1);
        x_status_corrupt(:,2) = Tmatrix * x_status_corrupt(:,1);
        x_status_corrupt(:,3) = Forz_matrix * u;
        x_status_corrupt(:,5) = x_status_corrupt(:,2) + x_status_corrupt(:,3) ;
        err_state(:,z) = 0;
    elseif type_unc == 9 %not accurate modeling
        u = create_input(DM,dev_input,1);
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(ref + 2*DM.PCC + 2*DM.Nline,1);
        x_status_corrupt(:,2) = Tmatrix * x_status_corrupt(:,1);
        x_status_corrupt(:,3) = Forz_matrix * u;
        x_status_corrupt(:,5) = x_status_corrupt(:,2) + x_status_corrupt(:,3) ;
        err_state(:,z) = (x_status_corrupt(:,5) - x_status(:,5));
    end
end %end z = 1 : N_MC

[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
[results_DSSE_Uncertainty] = calc_Unc_DSSE( results_DSSE,PowerData,GridData,Combination_devices,Accuracy,Test_SetUp);

if type_unc == 1 || type_unc == 5
    unc_DSSE_emp_i_lineA = results_DSSE_Uncertainty.std_err_Imagnitude_empmat;
    unc_DSSE_emp_i_lineP = results_DSSE_Uncertainty.std_err_Iphase_empmat;
    unc_DSSE_emp_i_lineD = results_DSSE_Uncertainty.std_err_Ireal_emp;
    unc_DSSE_emp_i_lineQ = results_DSSE_Uncertainty.std_err_Iimag_emp;
    var_status_out_absolute = var(transpose(err_state));
else
    unc_DSSE_emp_i_lineA = results_DSSE_Uncertainty.rms_err_Imagnitude_empmat;
    unc_DSSE_emp_i_lineP = results_DSSE_Uncertainty.rms_err_Iphase_empmat;
    unc_DSSE_emp_i_lineD = results_DSSE_Uncertainty.rms_err_Ireal_emp;
    unc_DSSE_emp_i_lineQ = results_DSSE_Uncertainty.rms_err_Iimag_emp;
    unc_status_out_absolute = rms(transpose(err_state));
    var_status_out_absolute = unc_status_out_absolute.^2;
end

Pfin0 = diag(var_status_out_absolute);

for x = 1 : GridData.Lines_num
    Pfin0(:,[ref+2*x-1,ref+2*x]) = Pfin0(:,[ref+2*x,ref+2*x-1]);
    Pfin0([ref+2*x-1,ref+2*x],:) = Pfin0([ref+2*x,ref+2*x-1],:);
end

[unc_i_lineA,unc_i_lineP,unc_i_lineD,unc_i_lineQ] = convert_cov_unc(Pfin0 ,DM,SD,GridData);


end

