%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to create the uncertainty model of a system described by A,B, C
% matrices
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [unc_out] = uncertainty_model_GP(Atot,B_input,B_disturbance,C,SD,VS,dev_input,time_delta,type_unc,Ts,Nw,filter_on)
% this function calculate the expected uncertainty of the state. It
% considers that the state is mainatined fixed untill the next update
%     type_unc = 0; 'Steady State estimation and system'
%     type_unc = 1; 'Steady State estimation and dynamic system with StdDev'
%     type_unc = 2; 'Steady State estimation and dynamic system with RMSE'
%     type_unc = 3; 'Dynamic State estimation and dynamic system with RMSE'
%     type_unc = 4; 'Steady State estimation and dynamic system with delay batch measurements with RMSE'
%     type_unc = 5; 'Steady State estimation and dynamic system with recursive estimator and StdDev'
%     type_unc = 6; 'Steady State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 7; 'Dynamic State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 8; 'Steady State estimation and dynamic system with delay splitted measurements with RMSE'
%     type_unc = 9; 'Dynamic State estimation and dynamic system with delay splitted measurements with RMSE'


u = SD.input;
if type_unc == 4
    time_delta = (1 + VS.meas_delay ) * time_delta ;
end

[ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);

P_fin2 = zeros(size(VS.var_status_absolute));
Tmatrix_eq = eye(size(TmatrixTs)); %Tmatrix_eq = zeros(size(TmatrixTs));
Forz_matrix_eq= zeros(size(Forz_matrixTs));
Dist_matrix_eq= zeros(size(Dist_matrixTs));

if type_unc == 0 %steady state case
    P_fin  = VS.var_status_absolute ;
    
elseif type_unc == 1 %standard deviation in static conditions
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ;
    P_fin  = VS.var_status_absolute + P_fin2 ;
    
    
elseif  type_unc == 2 || type_unc == 4 %RMSE in dynamic conditions and steady state estimation with BATCH
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    P_fin3 = Forz_matrix * diag((u*dev_input).^2) * transpose(Forz_matrix) ;
    P_fin4 = (Tmatrix-eye(size(Tmatrix))) * VS.P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    P_fin = VS.var_status_absolute + P_fin2 + P_fin3 + P_fin4 ;
    
elseif  type_unc == 3 %RMSE in dynamic conditions and dynamic state estimation with BATCH
    P_fin1 = Tmatrix * VS.var_status_absolute * transpose(Tmatrix); %covariance due to measurement error
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    P_fin = P_fin1 + P_fin2 ;
    
    
elseif  type_unc == 6 %RMSE in dynamic conditions and steady state estimation with recursive in block
        P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
        P_fin4 = (Tmatrix-eye(size(Tmatrix))) * VS.P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            
        else
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF((w-1)*time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * VS.P_delta_x * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((SD.input*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_fin4 = (Tmatrix-eye(size(Tmatrix))) * P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
        end
        %P_pre  = diag(diag(P_post)) + P_fin2 + P_fin4  ;
        P_pre  = P_post + P_fin2 + P_fin4  ;
    end
    P_fin = P_pre;
    
elseif  type_unc == 7 %RMSE in dynamic conditions and dynamic state estimation with recursive in block (KALMAN FILTER)
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  = Tmatrix * P_post * transpose(Tmatrix) + P_fin2  ;
        else
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
        end
    end
    P_fin = P_pre;
    
elseif  type_unc == 8  %delay - SED
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    Rtot = zeros(size(VS.R));
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(VS.time_delta_individual_meas == time_delta_meas(m));
    end
    t_delay(m+1) = 1;
    
    for m = 1 : length(time_delta_meas) %for every time update of the measurements 
        C_m = C (m_meas(m).meas,:); %we extract the corresponding element of the Jacobian
        
        if m > 1
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF(sum(t_delay(1:m-1)) *time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * VS.P_delta_x * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((SD.input*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
        else
            P_delta_x = VS.P_delta_x;
        end
        [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
        Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
        Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
        Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
        
        
        for n = m + 1 : length(time_delta_meas) + 1
            [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF( t_delay(n)*time_delta,Atot,B_input,B_disturbance);
            Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
            Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
            Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
        end
        
        P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
        P_delta3 =  Forz_matrix_eq * diag((u*dev_input).^2) * transpose(Forz_matrix_eq) ;
        P_delta4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * P_delta_x * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
        P_delta = P_delta2 + P_delta3 + P_delta4;
        Rtot(m_meas(m).meas,m_meas(m).meas) = Rtot(m_meas(m).meas,m_meas(m).meas) + C_m * P_delta * transpose(C_m);
        
        Tmatrix_eq = eye(size(TmatrixTs));
        Forz_matrix_eq= zeros(size(Forz_matrixTs));
        Dist_matrix_eq= zeros(size(Dist_matrixTs));
    end
    Rtot = Rtot + VS.R;
    P_fin = inv(C' * inv(Rtot) * C);
    
    
elseif  type_unc == 9  %delay - DED
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    Rtot = zeros(size(VS.R));
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(VS.time_delta_individual_meas == time_delta_meas(m));
    end
    t_delay(m+1) = 1;
    
    for m = 1 : length(time_delta_meas) %for every time update of the measurements
        C_m = C (m_meas(m).meas,:); %we extract the corresponding element of the Jacobian
        
        if m > 1
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF(sum(t_delay(1:m-1)) *time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * VS.P_delta_x * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((SD.input*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
        else
            P_delta_x = VS.P_delta_x;
        end
        [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
        Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
        Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
        Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
        
        
        for n = m + 1 : length(time_delta_meas)
            [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF( t_delay(n)*time_delta,Atot,B_input,B_disturbance);
            Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
            Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
            Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
        end
        
        P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
        P_delta3 =  Forz_matrix_eq * diag((u*dev_input).^2) * transpose(Forz_matrix_eq) ;
        P_delta4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * P_delta_x * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
        P_delta = P_delta2 + P_delta3 + P_delta4;
        Rtot(m_meas(m).meas,m_meas(m).meas) = Rtot(m_meas(m).meas,m_meas(m).meas) + C_m * P_delta * transpose(C_m);
        
        Tmatrix_eq = eye(size(TmatrixTs));
        Forz_matrix_eq= zeros(size(Forz_matrixTs));
        Dist_matrix_eq= zeros(size(Dist_matrixTs));
    end
    Rtot = Rtot + VS.R;
    
    P_in = inv(C' * inv(Rtot) * C); %initial uncertainty
    
    P_fin1 = Tmatrix * P_in * transpose(Tmatrix); %covariance due to measurement error
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    
    P_fin = P_fin1 + P_fin2 ;

elseif  type_unc == 10  %delay + static uncertainty
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    
    [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF(time_delta_meas,Atot,B_input,B_disturbance);
    Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
    Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
    Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
    
    
    P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
    P_delta3 =  Forz_matrix_eq * diag((u*dev_input).^2) * transpose(Forz_matrix_eq) ;
    P_delta4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * VS.P_delta_x * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
    P_fin = VS.var_status_absolute + P_delta2 + P_delta3 + P_delta4;
    
end

var_status_out_absolute = diag(P_fin);
unc_out = sqrt(var_status_out_absolute);

end

