%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate the filter coefficient for the disturbances
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [VS] = calc_filter(VS,DM,B_Eo,B_G,B_PQ)

%Fs = Ts^-1;

if DM.PCC == 1
    F_matrix = (B_Eo)*[1,0;0,1];
end
if sum(DM.grid_supp_PV)>0
    F_matrix = blkdiag(F_matrix,B_G);
end
for x = 1 : DM.Nnode
    if DM.grid_supp_PQ(x)==1
        F_matrix = blkdiag(F_matrix,B_PQ*[1,0;0,1]);
    end
end
VS.F_matrix = F_matrix; %state transition matrix of colored noise
VS.B_Eo = B_Eo;
VS.B_G  = B_G;
VS.B_PQ = B_PQ;

end

