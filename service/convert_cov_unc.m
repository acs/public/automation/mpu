%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to obtain uncertainty values from covariance matrix
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [unc_i_lineA,unc_i_lineP,unc_i_lineD,unc_i_lineQ,unc_v_nodeA,unc_v_nodeP,unc_v_nodeD,unc_v_nodeQ] = convert_cov_unc( P_fin,DM,SD,GridData)

ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;
P_fin = P_fin(ref+1:end,ref+1:end); P_fin_temp = P_fin;
var_status_out_absolute = diag(P_fin);
unc_out = sqrt(var_status_out_absolute);

if DM.PCC == 1
    unc_i_lineD(1,1) = unc_out(1);
    unc_i_lineQ(1,1) = unc_out(2);
end
for x = DM.PCC + 1 : DM.PCC + DM.Nline 
    unc_i_lineD(x,1) = unc_out(2*(x-1)+1);
    unc_i_lineQ(x,1) = unc_out(2*(x-1)+2);
end

use_pu = 0;
[~,~,~,CLINE_tot] = calc_A_B_matrices(DM,SD);
[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
[~,Combination_devices,Accuracy,GridData] = set_DSSE_Vstate(DM); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE

 GridData.inj_status = 0;

[~,GridData,~] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
[H_IRI] = Jacobian_m_IRIDSSE(GridData);

    H_IRI(:,1) = [];
    H_IRI(1,:) = []; 

    use_pu = 0; %lasciare 0 2019.01.10
    rotI = calc_rotI( GridData,PowerData,use_pu);
    rotI = rotI(2:end,2:end);
    rotV = calc_rotV( GridData,PowerData,use_pu);
    rotV = rotV(2:end,2:end);
    
Rx_polar_IRI = rotI*P_fin*rotI';
var_err_polar_mat_IRI = diag(Rx_polar_IRI);
std_err_polar_mat_IRI = sqrt(var_err_polar_mat_IRI);

if DM.PCC == 1
    unc_i_lineA(1,1) = std_err_polar_mat_IRI(1);
    unc_i_lineP(1,1) = std_err_polar_mat_IRI(2);
end
for x = DM.PCC + 1 : DM.PCC + DM.Nline 
    unc_i_lineA(x,1) = std_err_polar_mat_IRI( 2*(x-1)+1);
    unc_i_lineP(x,1) = std_err_polar_mat_IRI( 2*(x-1)+2);
end

% here the uncertainty for the node voltages are calculated
for x = 1 : 2*GridData.Lines_num
    P_fin_temp(:,x) = P_fin_temp(:,x) ./ ([ GridData.base_current*ones(2*GridData.Lines_num,1)] );
end
for x = 1 :  2*GridData.Lines_num
    P_fin_temp(x,:) = P_fin_temp(x,:) ./ ([ GridData.base_current*ones(2*GridData.Lines_num,1)] )';
end

P_V = H_IRI * P_fin_temp * H_IRI';

for x = 1 :  2*GridData.Lines_num
    P_V(:,x) = P_V(:,x) .* ([GridData.base_voltage*ones(2*(GridData.Nodes_num-1),1)] );
end
for x = 1 : 2*GridData.Lines_num
    P_V(x,:) = P_V(x,:) .* ([GridData.base_voltage*ones(2*(GridData.Nodes_num-1),1)] )';
end

var_status_out_absolute = diag(P_V);
unc_out = sqrt(var_status_out_absolute);

unc_v_nodeD(1,1) = unc_out(1,1);
for x = 2 : GridData.Nodes_num 
    unc_v_nodeD(x,1) = unc_out(2*(x-2) + 1);
    unc_v_nodeQ(x,1) = unc_out(2*(x-2) + 2);
end


Rx_polar_VRI = rotV * P_V * rotV';
var_err_polar_mat_VRI = diag(Rx_polar_VRI);
std_err_polar_mat_VRI = sqrt(var_err_polar_mat_VRI);

%unc_v_nodeA(1,1) = std_err_polar_mat_VRI( 1);
for x = 1 : GridData.Nodes_num - 1
    unc_v_nodeA(x+1,1) = std_err_polar_mat_VRI( 2*(x-1) + 1);
    unc_v_nodeP(x+1,1) = std_err_polar_mat_VRI( 2*(x-1) + 2);
end

end

