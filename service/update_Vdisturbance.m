%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to update the variance of the disturbance base on the time
% window and the autocovariance
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [diag_ratio,V] = update_Vdisturbance(Vref,Tref,time_delta)

% V =     ST(N_conf).colored = 1;
% Ts =   ST(N_conf).Ts = 1e0;

for x = 1 : length(time_delta)
    A = -log(Vref)/Tref;
    V(x) = exp(-A*time_delta(x));
    diag_ratio(x) = (1-V(x)^2);
end
end

