function [GridData] = generate_DM_DSSE_GridData(DM)

if DM.PCC == 0
    R1 = transpose(DM.rline);
    X1 = 2*pi*DM.f*transpose(DM.Lline);
    Nline = DM.Nline;
    Nnode = DM.Nnode;
else
    Nline = DM.Nline +1;
    Nnode = DM.Nnode +1;
    DM.topology = DM.topology +1;
    DM.topology = [[1;2],DM.topology];
    R1 = [DM.Rcc,transpose(DM.rline)];
    X1 = [2*pi*DM.f*DM.Lcc,  2*pi*DM.f*transpose(DM.Lline)];
end
topology=[(1:Nline);DM.topology];
base_power = sum(abs(DM.Sn)) ;
base_voltage = DM.Vn ;
base_current = base_power / base_voltage;
base_Z = (base_voltage^2)/base_power;

base_status = [];
for x = 1 : DM.Nnode
    if DM.grid_form(x)~=0
        base_status = [base_status;[1;base_power;base_power;1;1;1;1;base_current;base_current;base_voltage;base_voltage]];
        
    elseif DM.grid_supp(x)~=0
        base_status = [base_status;[base_power;base_power;base_power;base_power;1;1;base_current;base_current;base_voltage;base_voltage]];
    end
end
for x = 1 : DM.Nnode
    if DM.plac_load(x)~=0
        base_status = [base_status;[base_current;base_current]];
    end
end

R1 = R1 / base_Z;
X1 = X1 / base_Z;

Z1 = R1+1i*X1; 
Y1 = Z1.^-1;
R2 = real(Y1);
X2 = imag(Y1);
B1 = zeros(1,Nline);
G1 = zeros(1,Nline);

A=zeros(Nline,Nnode);%logic matrix N_nodes X N_nodes with 1 in i-m if the nodes i and m are connected
for m = 1 : Nline
    for n = 1 : Nnode
        if topology(3,m)== n
            A(m,n) = 1;
        elseif topology(2,m) == n
            for t = 1 : Nline
                if A(t,topology(2,m)) == 1
                    A(t,topology(3,m)) = 1;
                end
            end
        end
    end
end

GridData=struct;
GridData.Nphase = 1;
GridData.Nodes_num=Nnode;
GridData.Lines_num=Nline;
GridData.topology=topology;
GridData.base_power=base_power;
GridData.base_voltage=base_voltage;
GridData.base_current = base_current;
GridData.base_status = base_status;
GridData.base_impedance = base_Z;

GridData.R1=R1;
GridData.X1=X1;
GridData.B1=B1;
GridData.G1=G1;
GridData.R2=R2;
GridData.X2=X2;
GridData.A=A;
GridData.type_of_model = 'single_phase';
GridData.inj_status = 1;
end
