%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to test the uncertainty of a system described by A,B,C matrices
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [unc_system,unc_SE] = uncertainty_MC_SE_GP(Atot,B_input,B_disturbance,C,SD,VS,dev_input,time_delta,type_unc,Ts,Nw,N_MC,filter_on)
%     type_unc = 0; 'Steady State estimation and system'
%     type_unc = 1; 'Steady State estimation and dynamic system with StdDev'
%     type_unc = 2; 'Steady State estimation and dynamic system with RMSE'
%     type_unc = 3; 'Dynamic State estimation and dynamic system with RMSE'
%     type_unc = 4; 'Steady State estimation and dynamic system with delay batch measurements with RMSE'
%     type_unc = 5; 'Steady State estimation and dynamic system with recursive estimator and StdDev'
%     type_unc = 6; 'Steady State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 7; 'Dynamic State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 8; 'Steady State estimation and dynamic system with delay splitted measurements with RMSE'
%     type_unc = 9; 'Dynamic State estimation and dynamic system with delay splitted measurements with RMSE'


err_state = zeros(size(SD.x_status,1),N_MC);
var_status_absolute = diag(VS.var_status_absolute);
std_dev_status_absolute = sqrt(var_status_absolute);

P_fin2 = zeros(size(VS.var_status_absolute));

[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
Tmatrix_eq = eye(size(Tmatrix)); 
Forz_matrix_eq= zeros(size(Forz_matrix));
Dist_matrix_eq= zeros(size(Dist_matrix));
Rtot = zeros(size(VS.R));
if type_unc == 4
    time_delta = (1 +  VS.meas_delay ) * time_delta ;

elseif  type_unc == 6 %RMSE in dynamic conditions and dynamic state estimation  with recursive (KALMAN FILTER)

        P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
        P_fin4 = (Tmatrix-eye(size(Tmatrix))) * VS.P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  = P_post + P_fin2 + P_fin4 ;
        else
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF((w-1)*time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * VS.P_delta_x * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((SD.input*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_fin4w(w).P_fin4 = (Tmatrix-eye(size(Tmatrix))) * P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  = P_post + P_fin2 + P_fin4w(w).P_fin4 ;
        end
        %P_pre  = diag(diag(P_post)) + P_fin2 + P_fin4w(w).P_fin4 ;
        
    end
    P_fin = P_post;
    
elseif  type_unc == 7 %RMSE in dynamic conditions and dynamic state estimation  with recursive (KALMAN FILTER)

        P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute;
            P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
        else
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
        end
    end
    P_fin = P_post;
    
elseif type_unc == 8  %we have delay split in measurements with time_delta_meas vector
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(VS.time_delta_individual_meas == time_delta_meas(m));
        [ Tmatrix_m(m).Tmatrix,Forz_matrix_m(m).Forz_matrix,Dist_matrix_m(m).Dist_matrix] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
    end

 elseif type_unc == 10 %we have delay split in measurements with time_delta_meas vector
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta_meas,Atot,B_input,B_disturbance);
    


elseif type_unc == 9 %Dynamic case
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(VS.time_delta_individual_meas == time_delta_meas(m));
        [ Tmatrix_m(m).Tmatrix,Forz_matrix_m(m).Forz_matrix,Dist_matrix_m(m).Dist_matrix] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
    end
    
end



for z = 1 : N_MC
    if type_unc == 0 % steady state case
       x_status(:,1) = SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status)) ; 
       
    elseif type_unc == 1 || type_unc == 2 || type_unc == 3 || type_unc == 4 || type_unc == 10
        if type_unc == 1
            u = SD.input;
        else
            u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
        end
        %d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
        d =  mvnrnd(zeros(size(VS.var_disturbance_absolute,1),1),VS.var_disturbance_absolute)';
        
        if type_unc == 1
            x_status(:,1) = SD.x_status;
        elseif type_unc == 2 || type_unc == 3 || type_unc == 4 || type_unc == 10
            x_status(:,1) = SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
        end
        x_status(:,2) = Tmatrix * x_status(:,1);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
        
       
    elseif type_unc == 6 || type_unc == 7
        u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
        for w = 1 : Nw
%                 d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
d =  mvnrnd(zeros(size(VS.var_disturbance_absolute,1),1),VS.var_disturbance_absolute)';
        
                if w == 1
                    x_status_w(w).x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
                else
                    x_status_w(w).x_status(:,1) = x_status_w(w-1).x_status(:,5);
                end
                x_status_w(w).x_status(:,2) = Tmatrix * x_status_w(w).x_status(:,1);
                x_status_w(w).x_status(:,3) = Forz_matrix * u;
                x_status_w(w).x_status(:,4) = Dist_matrix * d;
                x_status_w(w).x_status(:,5) = x_status_w(w).x_status(:,2) + x_status_w(w).x_status(:,3) + x_status_w(w).x_status(:,4);
        end
        
    elseif type_unc == 8 || type_unc == 9 || type_unc == 10
        u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
        %d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
        d =  mvnrnd(zeros(size(VS.var_disturbance_absolute,1),1),VS.var_disturbance_absolute)';
        x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
        for m = 1 : length(t_delay)
            %d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
            d =  mvnrnd(zeros(size(B_disturbance,2),1),VS.var_disturbance_absolute)';
        
            if m == 1
                x_status_m(m).x_status(:,1) = x_status(:,1);
            else
                x_status_m(m).x_status(:,1) =  x_status_m(m-1).x_status(:,5);
            end
            x_status_m(m).x_status(:,2) = Tmatrix_m(m).Tmatrix * x_status_m(m).x_status(:,1);
            x_status_m(m).x_status(:,3) = Forz_matrix_m(m).Forz_matrix * u;
            x_status_m(m).x_status(:,4) = Dist_matrix_m(m).Dist_matrix * d;
            x_status_m(m).x_status(:,5) = x_status_m(m).x_status(:,2) + x_status_m(m).x_status(:,3) + x_status_m(m).x_status(:,4);
        end
        x_status(:,2) = Tmatrix * x_status_m(length(t_delay)).x_status(:,5);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);

    end
    
    if type_unc == 0
        x_status_true = x_status(:,1);
        z_measure_true = C * x_status_true;
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est = VS.invG_HW * z_measure_error;
        err_est_state(:,z) = (x_status_est - x_status(:,1)); 
        
    elseif type_unc == 1 || type_unc == 2 || type_unc == 4 || type_unc == 10
        x_status_true = x_status(:,1);
        z_measure_true = C * x_status_true;
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est = VS.invG_HW * z_measure_error;
        err_est_state(:,z) = (x_status_est - x_status(:,5));
    
    elseif type_unc == 3
        x_status_true = x_status(:,1);
        z_measure_true = C * x_status_true;
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est(:,1) = VS.invG_HW * z_measure_error;
        x_status_est(:,2) = Tmatrix * x_status_est(:,1);
        x_status_est(:,3) = Forz_matrix * u;
        x_status_est(:,5) = x_status_est(:,2) + x_status_est(:,3);
        err_est_state(:,z) = (x_status_est(:,5) - x_status(:,5));
 
   elseif type_unc == 6
       
        for w = 1 : Nw
             x_status_true = x_status_w(w).x_status(:,1);
            if w == 1
                z_measure_true = C * x_status_true;
                z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
                x_status_est_post = VS.invG_HW * z_measure_error;
                P_post = VS.var_status_absolute;
            else
                %P_pre = diag(diag(P_post)) + P_fin2 + P_fin4w(w).P_fin4;
                P_pre = P_post + P_fin2 + P_fin4w(w).P_fin4;
                x_status_est_pre = x_status_est_post ;
                z_measure_true = C * x_status_true;
                z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
                K = P_pre * transpose(C) * inv( C * P_pre * transpose(C) + VS.R);
                x_status_est_post = x_status_est_pre + K*(z_measure_error - C * x_status_est_pre);
                P_post = inv( inv(P_pre) + VS.Gain_matrix);
            end
        end
        x_status_est(:,1) = x_status_est_post;
        err_est_state(:,z) = (x_status_est(:,1) - x_status_w(Nw).x_status(:,5));
        
    elseif type_unc == 7
       
        for w = 1 : Nw
             x_status_true = x_status_w(w).x_status(:,1);
            if w == 1
                z_measure_true = C * x_status_true;
                z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
                x_status_est_post = VS.invG_HW * z_measure_error;
                P_post = VS.var_status_absolute;
            else
                P_pre = Tmatrix *P_post* transpose(Tmatrix) + P_fin2;
                x_status_est_pre = Tmatrix * x_status_est_post + Forz_matrix * u;
                z_measure_true = C * x_status_true;
                z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
                K = P_pre * transpose(C) * inv( C * P_pre * transpose(C) + VS.R);
                x_status_est_post = x_status_est_pre + K*(z_measure_error - C * x_status_est_pre);
                P_post = inv( inv(P_pre) + VS.Gain_matrix);
            end
        end
        x_status_est(:,1) = x_status_est_post;
        x_status_est(:,2) = Tmatrix * x_status_est(:,1);
        x_status_est(:,3) = Forz_matrix * u;
        x_status_est(:,5) = x_status_est(:,2) + x_status_est(:,3);
        err_est_state(:,z) = (x_status_est(:,5) - x_status_w(Nw).x_status(:,5));
        
    elseif type_unc == 8 
        for m = 1 : length(t_delay)
           x_status_true = x_status_m(m).x_status(:,1);
           C_m = C (m_meas(m).meas,:);  
           z_measure_true(m_meas(m).meas,1) = (C_m * x_status_true)';
        end
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est = VS.invG_HW * z_measure_error;
        err_est_state(:,z) = (x_status_est - x_status(:,5));
    
    elseif type_unc == 9
        for m = 1 : length(t_delay)
           x_status_true = x_status_m(m).x_status(:,1);
           C_m = C (m_meas(m).meas,:);  
           z_measure_true(m_meas(m).meas,1) = (C_m * x_status_true)';
        end
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est(:,1) = VS.invG_HW * z_measure_error;
        x_status_est(:,2) = Tmatrix * x_status_est(:,1);
        x_status_est(:,3) = Forz_matrix * u;
        x_status_est(:,5) = x_status_est(:,2) + x_status_est(:,3);
        err_est_state(:,z) = (x_status_est(:,5) - x_status(:,5));   
    
        
    end
    
    
    if type_unc == 0
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(size(SD.x_status,1),1);
        err_state(:,z) = (x_status_corrupt(:,1) - x_status(:,1));
    elseif type_unc == 1 || type_unc == 2 || type_unc == 4 || type_unc == 10
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(size(SD.x_status,1),1);
        err_state(:,z) = (x_status_corrupt(:,1) - x_status(:,5));
    elseif type_unc == 3
        x_status_corrupt(:,1) = x_status(:,1) + std_dev_status_absolute.*randn(size(SD.x_status,1),1);
        x_status_corrupt(:,2) = Tmatrix * x_status_corrupt(:,1);
        x_status_corrupt(:,3) = Forz_matrix * u;
        x_status_corrupt(:,5) = x_status_corrupt(:,2) + x_status_corrupt(:,3) ;
        err_state(:,z) = (x_status_corrupt(:,5) - x_status(:,5));
     elseif type_unc == 6
        x_status_corrupt(:,1) = x_status_w(Nw).x_status(:,1) + (diag(P_fin).^0.5).*randn(size(SD.x_status,1),1); %it is the last initial status of the window Nw
        x_status_corrupt(:,1) = x_status_w(Nw).x_status(:,1) + (diag(P_post).^0.5).*randn(size(SD.x_status,1),1);
        err_state(:,z) = (x_status_corrupt(:,1) - x_status_w(Nw).x_status(:,5));
    elseif type_unc == 7
        x_status_corrupt(:,1) = x_status_w(Nw).x_status(:,1) + (diag(P_fin).^0.5).*randn(size(SD.x_status,1),1); %it is the last initial status of the window Nw
        x_status_corrupt(:,2) = Tmatrix * x_status_corrupt(:,1);
        x_status_corrupt(:,3) = Forz_matrix * u;
        x_status_corrupt(:,5) = x_status_corrupt(:,2) + x_status_corrupt(:,3) ;
        err_state(:,z) = (x_status_corrupt(:,5) - x_status_w(Nw).x_status(:,5));
    elseif type_unc == 8 || type_unc == 9 
        err_state(:,z) = err_est_state(:,z);
    end
    
end %end z = 1 : N_MC



if type_unc == 1 
    unc_system = std(transpose(err_state));
    unc_SE = std(transpose(err_est_state));
else
    unc_system = rms(transpose(err_state));
    unc_SE = rms(transpose(err_est_state));
end


end

