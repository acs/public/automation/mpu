%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to integrate the dynamic system model into the distribution
% system state estimator
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Test_SetUp,Combination_devices,Accuracy,GridData ] = set_DSSE(DM)
% Combination_P = []; 
% Combination_Q = []; 
% Combination_Pseudo =  [2:DM.Nnode + DM.PCC];
% Combination_Vmagn =   [1]; 
% Combination_Vph =     [1];  
% Combination_Imagn =   [1:DM.Nnode + DM.PCC]; 
% Combination_Iph =     [1:DM.Nnode + DM.PCC]; %1:DM.Nnode + DM.PCC
% Combination_Pflow =   []; 
% Combination_Qflow =   [];

Combination_P = []; 
Combination_Q = []; %[2:DM.Nnode + DM.PCC]; %
Combination_Pseudo =  [2:DM.Nnode + DM.PCC];%[2,3,4,5,6,8,9,11];%
Combination_Vmagn =   [1,4,8];%[1]; %
Combination_Vph =     [1,4,8];%[1]; %[1,4,8];
Combination_Imagn =   [3,5,8]; %[6,9];%
Combination_Iph =     [3,5,8];%[6,9];% %1:DM.Nnode + DM.PCC
Combination_Pflow =   []; 
Combination_Qflow =   [];


unc_dev = 0.01;
unc_pseudo = 0.5;

common_delay_base = 0.5;
measurement_rate = 0.5;
time_delta_meas =   common_delay_base;%[0.5 0.4];%

Combination_P =       [Combination_P;common_delay_base * ones(1,size(Combination_P,2))]; 
Combination_Q =       [Combination_Q;common_delay_base * ones(1,size(Combination_Q,2))]; 
Combination_Vmagn =   [Combination_Vmagn;  common_delay_base *ones(1,size(Combination_Vmagn,2))]; 
Combination_Vph =     [Combination_Vph;   common_delay_base *ones(1,size(Combination_Vph,2))]; 
Combination_Imagn =   [Combination_Imagn; common_delay_base *ones(1,size(Combination_Imagn,2))]; 
Combination_Iph =     [Combination_Iph;   common_delay_base *ones(1,size(Combination_Iph,2))]; 
Combination_Pflow =   [Combination_Pflow; common_delay_base * ones(1,size(Combination_Pflow,2))]; 
Combination_Qflow =   [Combination_Qflow; common_delay_base * ones(1,size(Combination_Qflow,2))]; 
Combination_Pseudo =  [Combination_Pseudo;common_delay_base * ones(1,size(Combination_Pseudo,2))]; 


Combination_xstatus = [1:11*DM.NGF + 10*DM.NGS + 2*DM.Nload];
Combination_xstatus = [Combination_xstatus;common_delay_base * ones(1,size(Combination_xstatus,2))]; 
[GridData] = generate_DM_DSSE_GridData(DM);
GridData.DM = DM; %DM structure is integrated into GridData

Test_SetUp = struct;
Test_SetUp.N_MC = 50; %number of MC simulation, for reliable results set between 1000 and 10 000
Test_SetUp.limit1 = 0.000001; %treshold accuracy to interrupt newton rapson
Test_SetUp.limit2 = 50; %maximum number of iterations
Test_SetUp.time_steps = 1; %number of time step in each MC simulation

Combination_devices=struct;
Combination_devices.Combination_P = Combination_P;
Combination_devices.Combination_Q = Combination_Q;
Combination_devices.Combination_Vmagn = Combination_Vmagn;
Combination_devices.Combination_Vph = Combination_Vph;
Combination_devices.Combination_Imagn = Combination_Imagn;
Combination_devices.Combination_Iph = Combination_Iph;
Combination_devices.Combination_Pflow = Combination_Pflow;
Combination_devices.Combination_Qflow = Combination_Qflow;
Combination_devices.Combination_Pseudo = Combination_Pseudo;
Combination_devices.Combination_xstatus = Combination_xstatus;


Accuracy_P = sqrt(2*unc_dev^2); %accuracy of active power injection measurement
Accuracy_Q = sqrt(2*unc_dev^2); %accuracy of active power injection measurement
Accuracy_Vmagn = unc_dev; %accuracy of active power injection measurement
Accuracy_Vph = unc_dev; %accuracy of active power injection measurement)
Accuracy_Imagn = unc_dev; %branches with voltage phase angle measurement
Accuracy_Iph = unc_dev; %branches with voltage phase angle measurement
Accuracy_Pflow = sqrt(2*unc_dev^2); %branches with voltage phase angle measurement
Accuracy_Qflow = sqrt(2*unc_dev^2); %branches with voltage phase angle measurement
Accuracy_pseudo = unc_pseudo;

Accuracy=struct;
Accuracy.Accuracy_P=Accuracy_P;
Accuracy.Accuracy_Q=Accuracy_Q;
Accuracy.Accuracy_Vmagn=Accuracy_Vmagn;
Accuracy.Accuracy_Vph=Accuracy_Vph;
Accuracy.Accuracy_Imagn=Accuracy_Imagn;
Accuracy.Accuracy_Iph=Accuracy_Iph;
Accuracy.Accuracy_Pflow=Accuracy_Pflow;
Accuracy.Accuracy_Qflow=Accuracy_Qflow;
Accuracy.Accuracy_pseudo=Accuracy_pseudo;

Pseudo_measure=zeros(2,GridData.Nodes_num);
P_measure=zeros(2,GridData.Nodes_num);
Q_measure=zeros(2,GridData.Nodes_num);
Vmagn_measure=zeros(2,GridData.Nodes_num);
Vph_measure=zeros(2,GridData.Nodes_num);
Imagn_measure=zeros(2,GridData.Lines_num);
Iph_measure=zeros(2,GridData.Lines_num);
Pflow_measure=zeros(2,GridData.Lines_num);
Qflow_measure=zeros(2,GridData.Lines_num);
xstatus_measure=zeros(2,11*DM.NGF + 10*DM.NGS + 2*DM.Nload);
% in this loop we create a structure to better organize the measurements
% for the next functions
for n=1:GridData.Nodes_num
    if isempty(Combination_P)==0
        if  sum(ismember(Combination_P(1,:),n)) >0
            Loc = find(Combination_P(1,:)== n);
            P_measure(1,n) = 1;
            P_measure(2,n) = Combination_P(2,Loc);
        end
    end
    if isempty(Combination_Q)==0
        if  sum(ismember(Combination_Q(1,:),n)) >0
            Loc = find(Combination_Q(1,:)== n);
            Q_measure(1,n)=1;
            Q_measure(2,n) = Combination_Q(2,Loc);
        end
    end
    if isempty(Combination_Pseudo)==0
        if  sum(ismember(Combination_Pseudo(1,:),n)) >0
            Loc = find(Combination_Pseudo(1,:)== n);
            Pseudo_measure(1,n)=1;
            Pseudo_measure(2,n) = Combination_Pseudo(2,Loc);
        end
    end
    if isempty(Combination_Vmagn)==0
        if sum(ismember(Combination_Vmagn(1,:),n)) >0
            Loc = find(Combination_Vmagn(1,:)== n);
            Vmagn_measure(1,n) = 1;
            Vmagn_measure(2,n) = Combination_Vmagn(2,Loc);
        end
    end
    if isempty(Combination_Vph)==0
        if sum(ismember(Combination_Vph(1,:),n)) >0
            Loc = find(Combination_Vph(1,:)== n);
            Vph_measure(1,n)=1;
            Vph_measure(2,n) = Combination_Vph(2,Loc);
        end
    end
end

for m=1:GridData.Lines_num
    if isempty(Combination_Imagn)==0
        if sum(ismember(Combination_Imagn(1,:),m)) >0
            Loc = find(Combination_Imagn(1,:)== m);
            Imagn_measure(1,m)=1;
            Imagn_measure(2,m) = Combination_Imagn(2,Loc);
        end
    end
    if isempty(Combination_Iph)==0
        if sum(ismember(Combination_Iph(1,:),m)) >0
            Loc = find(Combination_Iph(1,:)== m);
            Iph_measure(1,m)=1;
            Iph_measure(2,m) = Combination_Iph(2,Loc);
        end
    end
    if isempty(Combination_Pflow)==0
        if sum(ismember(Combination_Pflow(1,:),m)) >0
            Loc = find(Combination_Pflow(1,:)== m);
            Pflow_measure(1,m)=1;
            Pflow_measure(2,m) = Combination_Pflow(2,Loc);
        end
    end
    if isempty(Combination_Qflow)==0
        if sum(ismember(Combination_Qflow(1,:),m)) >0
            Loc = find(Combination_Qflow(1,:)== m);
            Qflow_measure(1,m)=1;
            Qflow_measure(2,m) = Combination_Qflow(2,Loc);
        end
    end
end
for m = 1: 11*DM.NGF + 10*DM.NGS + 2*DM.Nload
     if sum(ismember(Combination_xstatus(1,:),m)) >0
         Loc = find(Combination_xstatus(1,:)== m);
            Combination_xstatus(1,m)=1;
            xstatus_measure(1,m) = 1;
            xstatus_measure(2,m) = Combination_xstatus(2,Loc);
     end
end
Combination_devices.P_measure = P_measure;
Combination_devices.Q_measure = Q_measure;
Combination_devices.Vmagn_measure = Vmagn_measure;
Combination_devices.Vph_measure = Vph_measure;
Combination_devices.Imagn_measure = Imagn_measure;
Combination_devices.Iph_measure = Iph_measure;
Combination_devices.Pflow_measure = Pflow_measure;
Combination_devices.Qflow_measure = Qflow_measure;
Combination_devices.Pseudo_measure = Pseudo_measure;
Combination_devices.xstatus_measure = xstatus_measure;
Combination_devices.time_delta_meas = time_delta_meas;
Combination_devices.common_delay_base     = common_delay_base;
Combination_devices.measurement_rate      = measurement_rate;
end

