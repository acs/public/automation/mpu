%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to select the type of test for propagation of uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [UncDM] = test_Function( Nnodes,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,dev_input,dev_status,filter_on,N_MC,Ts,Nt,Nw,t_delta_in, ...
    t_delta_fin,test_case,save_case)

load(save_case)
print_figure=1;

    
time_test = logspace(t_delta_in,t_delta_fin,Nt);


if  strcmp(test_case,'Only_uncertainty_model')
    type_unc = 8;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct;
    unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);
    for t = 1 : Nt
        time_delta = time_test(t);
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),...
            unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;
    UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;
    
elseif strcmp(test_case,'Unc_steady_state')    % Calculate standard deviation with steady state estimation and dynamic state
    type_unc = 0;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
    
    
elseif strcmp(test_case,'StdDev_static_batch')    % Calculate standard deviation with steady state estimation and dynamic state
    type_unc = 1;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
    
elseif strcmp(test_case,'Unc_static_batch')    % Calculate RMSE (uncertainty) with steady state estimation and dynamic state
    type_unc = 2;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
    
elseif strcmp(test_case,'Unc_dynamic_batch')     % Calculate RMSE (uncertainty) with steady state estimation and dynamic state
    type_unc = 3;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
    
elseif strcmp(test_case,'Unc_static_batch_non_ideal')
    type_unc = 4;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;

elseif strcmp(test_case,'Unc_static_recursive')
    type_unc = 6;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
  
    
elseif strcmp(test_case,'Unc_dynamic_recursive')
    type_unc = 7;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;

 elseif strcmp(test_case,'Unc_static_unsynch')
    type_unc = 8;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on,Accuracy);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
    
    elseif strcmp(test_case,'Unc_dynamic_unsynch')
    type_unc = 9;
    time_test = logspace(t_delta_in,t_delta_fin,Nt);
    UncDM = struct; unc_estimated_i_lineA = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineP=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineA0=zeros(1,Nt);unc_estimated_i_lineP0=zeros(1,Nt);unc_system_i_lineA =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineP =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineA0 =zeros(1,Nt);unc_system_i_lineP0 =zeros(1,Nt);unc_estimated_i_lineD = zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineQ=zeros(DM.Nline+DM.PCC,Nt);unc_estimated_i_lineD0=zeros(1,Nt);unc_estimated_i_lineQ0=zeros(1,Nt);unc_system_i_lineD =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineQ =zeros(DM.Nline+DM.PCC,Nt);unc_system_i_lineD0 =zeros(1,Nt);unc_system_i_lineQ0 =zeros(1,Nt);unc_DSSE_emp_i_lineA= zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineP = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineD = zeros(GridData.Lines_num,Nt);unc_DSSE_emp_i_lineQ = zeros(GridData.Lines_num,Nt);
    for t = 1 : Nt
        time_delta = time_test(t)
        [unc_estimated_i_lineA(:,t),unc_estimated_i_lineP(:,t),unc_estimated_i_lineD(:,t),unc_estimated_i_lineQ(:,t)] = ...
            uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on,Accuracy);
        [unc_system_i_lineA(:,t),unc_system_i_lineP(:,t),unc_system_i_lineD(:,t),unc_system_i_lineQ(:,t),...
            unc_DSSE_emp_i_lineA(:,t),unc_DSSE_emp_i_lineP(:,t),unc_DSSE_emp_i_lineD(:,t),unc_DSSE_emp_i_lineQ(:,t) ] = ...
            uncertainty_MC_DSSE(DM,SD,VS,N_MC,GridData,Accuracy,Combination_devices,Test_SetUp,dev_input,...
            type_unc,time_delta,Ts,filter_on,Nw);
    end
    UncDM.unc_estimated_i_lineD = unc_estimated_i_lineD;UncDM.unc_estimated_i_lineQ = unc_estimated_i_lineQ;UncDM.unc_system_i_lineD = unc_system_i_lineD;UncDM.unc_system_i_lineQ = unc_system_i_lineQ;UncDM.unc_DSSE_emp_i_lineD = unc_DSSE_emp_i_lineD;UncDM.unc_DSSE_emp_i_lineQ = unc_DSSE_emp_i_lineQ;UncDM.time_test = time_test;
     
elseif   strcmp(test_case,'test_DSSE_TimeInstant') % Test only the performance of the state estimator on single points in time    %
    type_est = 1; 
    % type_est == 1: steady  state stimple test
    % type_est == 2: steady  state versus evolved system
    % type_est == 3: steady  state recursive
    % type_est == 4: dynamic state recursive
    time_delta = 1e-4;
    Nw = 20;
    [UncDM] = test_DSSE_TimeInstant(N_MC,DM,VS,SD,GridData,Accuracy,Combination_devices,Test_SetUp,type_est,time_delta,Nw,dev_input);
    
elseif strcmp(test_case,'TestOnlyDSSE_TE') % Test only the performance of the state estimator with time evolutions
    
    plot_figure = 1;
    Nt = 50; time_delta = 0.0001;
    [VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy);
    type_est = 1; %1 steady IRDSE; %2 dyanmic IRDSE; %3 steady IRDSE with non-ideal batch; %4 IRDSE_steady recursive; %5 IRDSE dynamic recursive
    [UncDM] = test_DSSE_TimeEvolution( Nt, time_delta,DM,SD,VS,GridData,Accuracy,Combination_devices,Test_SetUp,plot_figure,type_est);
    
elseif strcmp(test_case,'Get_New_Regime')    % Calculate standard deviation with steady state estimation and dynamic state
    [DM,SD] = Dynamic_Model(Nnodes); % DM: dynamic model, SD: status data, VS: variance status
    [~,Combination_devices,Accuracy,GridData] = set_DSSE(DM); %Test_SetUp testing conditions of SE, Combination_devices installed, Accuracy of devices, Griddata for SE
   
    print_figure = 0;  time_delta = 1e-5; Ntime = 1*(time_delta^-1); unc_disturb_Eo1 = 0.00;unc_disturb_G1  = 0.00;unc_distur_PQ1 = 0.00;
    %[VS1] = calc_VS(DM,SD,unc_disturb_Eo1,unc_disturb_G1,unc_distur_PQ1,GridData,Combination_devices,Accuracy,filter_on);
    %[SD] = StatusRegime(Ntime,time_delta,DM,SD,VS1,print_figure);
    [VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy,filter_on);
    [~,~,~,CLINE_tot] = calc_A_B_matrices(DM,SD);
    [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
    [~,GridData] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
    name_save =  strcat('SimulationData_',num2str(Nnodes),'.mat');
    save(name_save,'DM','SD','VS','Test_SetUp','Combination_devices','Accuracy','GridData')
    
elseif strcmp(test_case,'Time_evolution')
    % Time evolution of the dynamic system
    
    time_delta = 1e-3;
    Nt = floor(1*(time_delta^-1));
    unc_disturb_Eo = 0.0;
    unc_disturb_G  = 0.0;
    unc_distur_PQ  = 0.0;
    [VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy);
    
    [~,vbD_t,vbQ_t,i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTestSlow (Nt,time_delta,DM,SD,VS);
    %[SD,vbD_t,vbQ_t,i_lineD_t,i_lineQ_t,ioD_t,ioQ_t] = TimeTestFast (Nt,time_delta,DM,SD,VS);
    save 'TimeTest' vbD_t vbQ_t i_lineD_t i_lineQ_t ioD_t ioQ_t unc_disturb_Eo unc_disturb_G unc_distur_PQ time_delta Nt GridData
    
elseif strcmp(test_case,'Check_Stability')
    [Atot,~,~,~] = calc_A_B_matrices(DM,SD);
    [~,~,~,~,~,eig_index] = calc_eig(Atot);
    UncDM = eig_index;
    
    

    
end

end

