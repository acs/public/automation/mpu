%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to obtain the model for the expected uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [unc_i_lineA,unc_i_lineP,unc_i_lineD,unc_i_lineQ] = uncertainty_model(DM,SD,VS,GridData,Combination_devices,dev_input,dev_status,time_delta,type_unc,Ts,Nw,filter_on,Accuracy)
%     type_unc = 0; 'Steady State estimation and system'
%     type_unc = 1; 'Steady State estimation and dynamic system with StdDev'
%     type_unc = 2; 'Steady State estimation and dynamic system with RMSE'
%     type_unc = 3; 'Dynamic State estimation and dynamic system with RMSE'
%     type_unc = 4; 'Steady State estimation and dynamic system with delay batch measurements with RMSE'
%     type_unc = 5; 'Steady State estimation and dynamic system with recursive estimator and StdDev'
%     type_unc = 6; 'Steady State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 7; 'Dynamic State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 8; 'Steady State estimation and dynamic system with delay splitted measurements with RMSE'
%     type_unc = 9; 'Dynamic State estimation and dynamic system with delay splitted measurements with RMSE'

[u] = create_input(DM,dev_input,1);
[Atot,B_input,B_disturbance,~] = calc_A_B_matrices(DM,SD);
if type_unc == 4
    time_delta = (1 + Combination_devices.common_delay_base ) * time_delta ;
end

[ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);

P_fin2 = zeros(size(VS.var_status_absolute));
Tmatrix_eq = eye(size(TmatrixTs)); %Tmatrix_eq = zeros(size(TmatrixTs));
Forz_matrix_eq= zeros(size(Forz_matrixTs));
Dist_matrix_eq= zeros(size(Dist_matrixTs));

ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;


if type_unc == 0 %standard deviation in static conditions
    P_fin  = VS.var_status_absolute ;
    
elseif type_unc == 1 %standard deviation in static conditions
    
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ;
    P_fin  = VS.var_status_absolute + P_fin2 ;
    
elseif  type_unc == 2 || type_unc == 4 %RMSE in dynamic conditions and steady state estimation with BATCH
    
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    P_fin3 = Forz_matrix * diag((u*dev_input).^2) * transpose(Forz_matrix) ;
    P_fin4 = (Tmatrix-eye(size(Tmatrix))) *diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    P_fin = VS.var_status_absolute + P_fin2 + P_fin3 + P_fin4 ;
    
elseif  type_unc == 3 %RMSE in dynamic conditions and dynamic state estimation with BATCH
    
    P_fin1 = Tmatrix * VS.var_status_absolute * transpose(Tmatrix); %covariance due to measurement error
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    P_fin = P_fin1 + P_fin2 ;
    
    
elseif  type_unc == 6 %RMSE in dynamic conditions and steady state estimation with recursive in block
    
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    P_fin4 = (Tmatrix-eye(size(Tmatrix))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    
    
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  = P_post + P_fin2 + P_fin4  ;
        else
            P_pre = diag(diag(P_pre));
            for x = 1 : GridData.Lines_num
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end
            inv_P_Pre = diag(diag(P_pre).^-1);
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF((w-1)*time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * diag(diag(VS.P_delta_x)) * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((u*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_fin4 = (Tmatrix-eye(size(Tmatrix))) * P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
            P_post = diag((diag(P_pre).^-1 + diag(VS.Gain_matrix)).^-1);
            
            for x = 1 : GridData.Lines_num
                P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
            end
            P_pre  = P_post + P_fin2 + P_fin4 ;
        end
    end
    P_fin = P_pre;
    
    
elseif  type_unc == 7 %RMSE in dynamic conditions and dynamic state estimation  with recursive (KALMAN FILTER)
    
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  = Tmatrix * P_post * transpose(Tmatrix) + P_fin2  ;
        else
            P_pre = diag(diag(P_pre));
            for x = 1 : GridData.Lines_num
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end
            
            %P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_post = diag((diag(P_pre).^-1 + diag(VS.Gain_matrix)).^-1);
            
            for x = 1 : GridData.Lines_num
                P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
            end
            P_post = diag(diag(P_post));
            P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
        end
    end
    P_fin = P_pre;
    
    
elseif type_unc == 8  %we have delay split in measurements with time_delta_meas vector - static estimator
    
    
    time_delta_meas = (sort(unique(Combination_devices.time_delta_meas),'descend'));
    % in case the system is overdetermined the model can be applied by it
    % is unaccurate. We delete some pseudo measurements untill the system
    % is equally determined.
    [~,~,~,CLINE_tot] = calc_A_B_matrices(DM,SD);
    [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
    [W,GridData,R] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
    [C] = Jacobian_m_IRIDSSE(GridData);

    C(:,ref+1)=[];
    C(ref+1,:)=[];
    W(:,ref+1)=[];R(:,ref+1)=[];
    W(ref+1,:)=[];R(ref+1,:)=[];
    inv_C = inv(C);
    VS.R = C *VS.var_status_absolute * transpose(C);
    VS.Rpu = R;
    Rtot = zeros(size(VS.R));
    GridData.DelayMeas(ref+1)=[];
    
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(GridData.DelayMeas == time_delta_meas(m));
    end
    t_delay(m+1) = 1;
    
    for m = 1 : length(time_delta_meas)%for every time update of the measurements
        Tmatrix_eq = eye(size(TmatrixTs));
        Forz_matrix_eq = zeros(size(Forz_matrixTs));
        Dist_matrix_eq = zeros(size(Dist_matrixTs));
        C_m = C (m_meas(m).meas,:); %we extract the corresponding element of the Jacobian
        if m > 1
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF(sum(t_delay(1:m-1)) *time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * P_delta_x * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((u*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_delta_x = diag(diag(P_delta_x));
        else
            P_delta_x = diag(diag(VS.P_delta_x));
        end
        [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
        Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
        Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
        Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
        
        for n = m + 1 : length(time_delta_meas) + 1
            [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF( t_delay(n)*time_delta,Atot,B_input,B_disturbance);
            Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
            Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
            Tmatrix_eq = Tmatrix_eq * Tmatrix_m;
        end
        
        P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
        P_delta3 =  Forz_matrix_eq * diag((u*dev_input).^2) * transpose(Forz_matrix_eq) ;
        P_delta4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * diag(diag(P_delta_x)) * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
        P_delta = P_delta2 + P_delta3 + P_delta4;
        
        Rtot(m_meas(m).meas,m_meas(m).meas) = Rtot(m_meas(m).meas,m_meas(m).meas) + C_m * P_delta * transpose(C_m);
    end
    P_fin = inv_C * (Rtot+VS.R) * inv_C';
    
    
    
elseif  type_unc == 9  %we have delay split in measurements with time_delta_meas vector - dynamic estimator
    time_delta_meas = (sort(unique(Combination_devices.time_delta_meas),'descend'));
    
    % in case the system is overdetermined the model can be applied by it
    % is unaccurate. We delete some pseudo measurements untill the system
    % is equally determined.
    [~,~,~,CLINE_tot] = calc_A_B_matrices(DM,SD);
    [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
    [W,GridData,~] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
    [C] = Jacobian_m_IRIDSSE(GridData);
    
    N_it = size(C,1); N_it1 = 0;
    while size(C,1) > size(C,2) && N_it1 < N_it
        N_it1 = N_it1 + 1;
        v_meas = GridData.TypeMeas == 3;
        if sum(v_meas) > 1 % additional v measure
            node_v_meas = GridData.LocationMeas(v_meas);
            Combination_devices.Pseudo_measure(1:2,node_v_meas) = 0;
            [W,GridData,~] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
            [C] = Jacobian_m_IRIDSSE(GridData);
        end
        c_meas = GridData.TypeMeas == 5;
        
        if sum(c_meas) > 0  % additional c measure
            line_c_meas = GridData.LocationMeas(c_meas);
            node_pseudo = GridData.topology(3,line_c_meas);
            Combination_devices.Pseudo_measure(1:2,node_pseudo) = 0;
            [W,GridData,~] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
            [C] = Jacobian_m_IRIDSSE(GridData);
        end
    end
    
    C(:,ref+1)=[];
    C(ref+1,:)=[];
    W(:,ref+1)=[];
    W(ref+1,:)=[];
    inv_C = inv(C);
    VS.R = C *VS.var_status_absolute * transpose(C);

    Rtot = zeros(size(VS.R));
    GridData.DelayMeas(ref+1)=[];
    

    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(GridData.DelayMeas == time_delta_meas(m));
    end
    t_delay(m+1) = 1;
    for m = 1 : length(time_delta_meas) %for every time update of the measurements
        Tmatrix_eq = eye(size(TmatrixTs));
        Forz_matrix_eq= zeros(size(Forz_matrixTs));
        Dist_matrix_eq= zeros(size(Dist_matrixTs));
        C_m = C (m_meas(m).meas,:); %we extract the corresponding element of the Jacobian
        if m > 1
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF(sum(t_delay(1:m-1)) *time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * diag(diag(VS.P_delta_x)) * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((u*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_delta_x = diag(diag(P_delta_x));
        else
            P_delta_x = diag(diag(VS.P_delta_x));
        end
        [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
        Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
        Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
        Tmatrix_eq     = Tmatrix_eq * Tmatrix_m;
        
        for n = m + 1 : length(time_delta_meas) 
            [Tmatrix_m,Forz_matrix_m,Dist_matrix_m] = calc_TDF( t_delay(n)*time_delta,Atot,B_input,B_disturbance);
            Dist_matrix_eq = Dist_matrix_m + Tmatrix_m * Dist_matrix_eq;
            Forz_matrix_eq = Forz_matrix_m + Tmatrix_m * Forz_matrix_eq;
            Tmatrix_eq     = Tmatrix_eq * Tmatrix_m;
        end
        
        P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
        P_delta3 =  Forz_matrix_eq * diag((u*dev_input).^2) * transpose(Forz_matrix_eq) ;
        P_delta4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * diag(diag(P_delta_x)) * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
        P_delta = P_delta2 + P_delta3 + P_delta4;
        Rtot(m_meas(m).meas,m_meas(m).meas) = Rtot(m_meas(m).meas,m_meas(m).meas) + C_m * P_delta * transpose(C_m);
    end
    P_in = inv_C * (Rtot+VS.R) * inv_C';
    
    P_fin1 = Tmatrix * P_in * transpose(Tmatrix); %covariance due to measurement error
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    
    P_fin = P_fin1 + P_fin2 ; 
end

for x = 1 : GridData.Lines_num
    P_fin(:,[ref+2*x-1,ref+2*x]) = P_fin(:,[ref+2*x,ref+2*x-1]);
    P_fin([ref+2*x-1,ref+2*x],:) = P_fin([ref+2*x,ref+2*x-1],:);
end

[unc_i_lineA,unc_i_lineP,unc_i_lineD,unc_i_lineQ] = convert_cov_unc( P_fin,DM,SD,GridData);

end

