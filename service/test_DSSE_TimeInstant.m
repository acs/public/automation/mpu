%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to test the state estimator in a single point in time
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ results_DSSE_Uncertainty ] = test_DSSE_TimeInstant(N_MC,DM,VS,SD,GridData,Accuracy,Combination_devices,Test_SetUp,type_est,time_delta,Nw,dev_input)
% type_est == 1: steady  state stimple test
% type_est == 2: steady  state versus evolved system
% type_est == 3: steady  state recursive
% type_est == 4: dynamic state recursive

results_DSSE=struct; results_DSSE.err_Vmagnitude=[];results_DSSE.err_Vphase=[];results_DSSE.Vmagn_status=[];results_DSSE.Vph_status=[];results_DSSE.Imagn_status=[];results_DSSE.Iph_status=[];results_DSSE.Vreal_status=[];results_DSSE.Vimag_status=[];results_DSSE.Ireal_status=[];results_DSSE.Iimag_status=[];results_DSSE.Vmagn_true=[];results_DSSE.Vph_true=[];results_DSSE.Imagn_true=[];results_DSSE.Iph_true=[];results_DSSE.Vreal_true=[];results_DSSE.Vimag_true=[];results_DSSE.Ireal_true=[];results_DSSE.Iimag_true=[];results_DSSE.err_Imagnitude=[];results_DSSE.err_Iphase=[];results_DSSE.err_Ireal=[];results_DSSE.err_Iimag=[];results_DSSE.err_Vreal=[];results_DSSE.err_Vimag=[];results_DSSE.err_Vmagnitude_in   =[];results_DSSE.err_Vphase_in=[];results_DSSE.err_Imagnitude_in  =[];results_DSSE.err_Iphase_in   =[];results_DSSE.err_Ireal_in   =[];results_DSSE.err_Iimag_in =[];
results_DSSE.err_Ireal_in=[];results_DSSE.err_Iimag_in=[];results_DSSE.err_Ireal_out=[];results_DSSE.err_Iimag_out=[];results_DSSE.err_inj_status=[];

[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
[W,GridData,R] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
[u] = create_input(DM,dev_input,1); T_init = 1; Ts = 1e4;

print_figure = 0;
P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
P_fin3 = Forz_matrix * diag((u*dev_input).^2) * transpose(Forz_matrix) ;
P_fin4 = (Tmatrix-eye(size(Tmatrix))) * diag(diag(VS.P_delta_x)) * transpose((Tmatrix-eye(size(Tmatrix)))) ;

ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;P_fin2b = P_fin2; P_fin3b = P_fin3; P_fin4b = P_fin4; %reshaped matrixes
for x = 1 : GridData.Lines_num
    P_fin2b(:,[ref+2*x-1,ref+2*x]) = P_fin2b(:,[ref+2*x,ref+2*x-1]);
    P_fin2b([ref+2*x-1,ref+2*x],:) = P_fin2b([ref+2*x,ref+2*x-1],:);
    P_fin3b(:,[ref+2*x-1,ref+2*x]) = P_fin3b(:,[ref+2*x,ref+2*x-1]);
    P_fin3b([ref+2*x-1,ref+2*x],:) = P_fin3b([ref+2*x,ref+2*x-1],:);
    P_fin4b(:,[ref+2*x-1,ref+2*x]) = P_fin4b(:,[ref+2*x,ref+2*x-1]);
    P_fin4b([ref+2*x-1,ref+2*x],:) = P_fin4b([ref+2*x,ref+2*x-1],:);
end

if type_est == 1
    P_fin = full(VS.var_status_absolute);
    
elseif type_est == 2
    P_fin1 = Tmatrix * VS.var_status_absolute * transpose(Tmatrix);
    P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ;
    P_fin  = P_fin1 + P_fin2 ;
    for x = 1 : GridData.Lines_num
        P_fin(:,[ref+2*x,ref+2*x-1]) = P_fin(:,[ref+2*x-1,ref+2*x]);
        P_fin([ref+2*x,ref+2*x-1],:) = P_fin([ref+2*x-1,ref+2*x],:);
    end
    
elseif type_est == 3
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  = P_post + P_fin2b ;
        else
            P_pre = diag(diag(P_pre));
            for x = 1 : GridData.Lines_num
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  = P_post + P_fin2b ;
        end
    end
    P_fin = P_pre;
    
elseif type_est == 4
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  = P_post + P_fin2b + P_fin3b + P_fin4b;
        else
            P_pre = diag(diag(P_pre));
            for x = 1 : GridData.Lines_num
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  = P_post + P_fin2b + P_fin3b + P_fin4b;
        end
    end
    P_fin = P_pre;
    
elseif type_est == 5
    for w = 1 : Nw
        if w == 1
            P_post_m = VS.var_status_absolute; %VS.var_status_absolute;
            for x = 1 : GridData.Lines_num
                P_post_m(:,[ref+2*x-1,ref+2*x]) = P_post_m(:,[ref+2*x,ref+2*x-1]);
                P_post_m([ref+2*x-1,ref+2*x],:) = P_post_m([ref+2*x,ref+2*x-1],:);
            end
            P_pre_m  = Tmatrix * P_post_m * transpose(Tmatrix) + P_fin2  ;
        else
            P_pre_m = diag(diag(P_pre_m));
            for x = 1 : GridData.Lines_num
                P_pre_m(:,[ref+2*x,ref+2*x-1]) = P_pre_m(:,[ref+2*x-1,ref+2*x]);
                P_pre_m([ref+2*x,ref+2*x-1],:) = P_pre_m([ref+2*x-1,ref+2*x],:);
            end
            P_pre_m = diag(diag(P_pre_m));
            P_post_m = inv( inv(P_pre_m) + VS.Gain_matrix );
            for x = 1 : GridData.Lines_num
                P_post_m(:,[ref+2*x-1,ref+2*x]) = P_post_m(:,[ref+2*x,ref+2*x-1]);
                P_post_m([ref+2*x-1,ref+2*x],:) = P_post_m([ref+2*x,ref+2*x-1],:);
            end
            P_pre_m  =  Tmatrix * P_post_m * transpose(Tmatrix) + P_fin2 ;
            
        end
    end
    for x = 1 : GridData.Lines_num
        P_pre_m(:,[ref+2*x,ref+2*x-1]) = P_pre_m(:,[ref+2*x-1,ref+2*x]);
        P_pre_m([ref+2*x,ref+2*x-1],:) = P_pre_m([ref+2*x-1,ref+2*x],:);
    end
    P_fin = P_pre_m;
end



for z = 1 : N_MC
    if type_est == 1 %test estimation test in single point in time
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData); 
        Meas_vector_external = mvnrnd(Meas_true_vector, R)';
        [Vmagn_status,Vph_status,~,~,inj_status]  = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [results_DSSE]             = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status);
        
    elseif type_est == 2 %test estimation test in single point in time and compared with system evolution
        [u] = create_input(DM,dev_input,2);    [d] = create_disturbance(DM,VS);
        x_status(:,1) = SD.x_status + (diag(diag(VS.P_delta_x)).^0.5) * randn(size(SD.x_status));
        x_status(:,2) = Tmatrix * x_status(:,1);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = mvnrnd(Meas_true_vector, R)';
        [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status]  = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [ Vmagn_status_in,Vph_status_in,~,~,inj_status_in ] = calc_xestimate( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE]             = Data_Output(Vmagn_status_in,Vph_status_in,results_DSSE,GridData,PowerData,inj_status_in);
        
        
    elseif type_est == 3 %test steady estimation recursive
        [u] = create_input(DM,dev_input,2);    [d] = create_disturbance(DM,VS);
        x_status(:,1) = SD.x_status;
        x_status(:,2) = Tmatrix * x_status(:,1);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = mvnrnd(Meas_true_vector, R)';
        [Vmagn_status,Vph_status,Imagn_status_in,Iph_status_in,inj_status_in]  = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        P_post = VS.var_status_absolute;
        P_pre  = P_post + P_fin2b + P_fin3b + P_fin4b;

        for w = 2 : Nw
            [u] = create_input(DM,dev_input,2);
            [d] = create_disturbance(DM,VS);
            x_status(:,1) = x_status(:,5);
            x_status(:,2) = Tmatrix * x_status(:,1);
            x_status(:,3) = Forz_matrix * u;
            x_status(:,4) = Dist_matrix * d;
            x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
            [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
            P_pre = diag(diag(P_pre));
            [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
            Meas_vector_external = mvnrnd(Meas_true_vector, R)';
            
            [Vmagn_status,Vph_status,Imagn_status_in,Iph_status_in,inj_status_in,P_post] = IRIDSSE_rec...
                (Meas_vector_external,W,GridData,Test_SetUp,Imagn_status_in,Iph_status_in,inj_status_in,P_pre);
            
            P_pre  = P_post + P_fin2b + P_fin3b + P_fin4b;
        end
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE]             = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status_in);
        
        
        
    elseif type_est == 4 %test dynamic estimation recursive
        
        [u] = create_input(DM,dev_input,1);
        [d] = create_disturbance(DM,VS);
        x_status(:,1) = SD.x_status + (diag(diag(VS.P_delta_x)).^0.5) * randn(size(SD.x_status));
        x_status(:,2) = Tmatrix * x_status(:,1);
        x_status(:,3) = Forz_matrix * u;
        x_status(:,4) = Dist_matrix * d;
        x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
        
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = mvnrnd(Meas_true_vector, R)';

        [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post]  = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        [ Vmagn_status_in,Vph_status_in,Imagn_status_in,Iph_status_in,inj_status_in ] = calc_xestimate( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
        P_post_m = VS.var_status_absolute; %VS.var_status_absolute;

        for x = 1 : GridData.Lines_num
            P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
            P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
            
            P_post_m(:,[ref+2*x-1,ref+2*x]) = P_post_m(:,[ref+2*x,ref+2*x-1]);
            P_post_m([ref+2*x-1,ref+2*x],:) = P_post_m([ref+2*x,ref+2*x-1],:);
        end %firstly we put it in the proper shape
        P_pre = Tmatrix * P_post * transpose(Tmatrix) + P_fin2;
        P_pre_m  = Tmatrix * P_post_m * transpose(Tmatrix) + P_fin2  ;
        for x = 1 : GridData.Lines_num
            P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
            P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            P_pre_m(:,[ref+2*x,ref+2*x-1]) = P_pre_m(:,[ref+2*x-1,ref+2*x]);
            P_pre_m([ref+2*x,ref+2*x-1],:) = P_pre_m([ref+2*x-1,ref+2*x],:);
        end %then we reinvert it to use it in the estimator
        
        
        for w = 2 : Nw
            [d] = create_disturbance(DM,VS);
            x_status(:,1) = x_status(:,5);
            x_status(:,2) = Tmatrix * x_status(:,1);
            x_status(:,3) = Forz_matrix * u;
            x_status(:,4) = Dist_matrix * d;
            x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
            [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,1));
            [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
            Meas_vector_external = mvnrnd(Meas_true_vector, R)';
            
            
            [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post] = IRIDSSE_rec...
                (Meas_vector_external,W,GridData,Test_SetUp,Imagn_status_in,Iph_status_in,inj_status_in,P_pre);
            [ Vmagn_status_in,Vph_status_in,Imagn_status_in,Iph_status_in,inj_status_in ] = calc_xestimate...
                ( DM, GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
            
            P_pre_m = diag(diag(P_pre_m));
            P_post_m = inv( inv(P_pre_m) + VS.Gain_matrix );
            %confronto 3: incertezza post seconda stima
            
            for x = 1 : GridData.Lines_num
                P_post_m(:,[ref+2*x-1,ref+2*x]) = P_post_m(:,[ref+2*x,ref+2*x-1]);
                P_post_m([ref+2*x-1,ref+2*x],:) = P_post_m([ref+2*x,ref+2*x-1],:);
                
                P_post(:,[ref+2*x-1,ref+2*x]) = P_post(:,[ref+2*x,ref+2*x-1]);
                P_post([ref+2*x-1,ref+2*x],:) = P_post([ref+2*x,ref+2*x-1],:);
            end %firstly we put it in the proper shape
            P_pre = Tmatrix * P_post * transpose(Tmatrix) + P_fin2;
            P_pre_m  =  Tmatrix * P_post_m * transpose(Tmatrix) + P_fin2 ;
            for x = 1 : GridData.Lines_num
                P_pre_m(:,[ref+2*x,ref+2*x-1]) = P_pre_m(:,[ref+2*x-1,ref+2*x]);
                P_pre_m([ref+2*x,ref+2*x-1],:) = P_pre_m([ref+2*x-1,ref+2*x],:);
                
                P_pre(:,[ref+2*x,ref+2*x-1]) = P_pre(:,[ref+2*x-1,ref+2*x]);
                P_pre([ref+2*x,ref+2*x-1],:) = P_pre([ref+2*x-1,ref+2*x],:);
            end %then we reinvert it to use it in the estimator
            
        end
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status(:,5));
        [results_DSSE]             = Data_Output(Vmagn_status_in,Vph_status_in,results_DSSE,GridData,PowerData,inj_status_in);
        
        
    end
end

[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);



[results_DSSE_Uncertainty] = calc_Unc_DSSE(results_DSSE,PowerData,GridData,Combination_devices,Accuracy,Test_SetUp);


if print_figure == 1
    
    N_hist = 50; % floor(N_MC/10);
    f = figure;
    movegui(f,'northeast');
    subplot(2,1,1)
    histogram (results_DSSE.err_Ireal(:,1))
    hold on
    for x = 2 : GridData.Lines_num
        histogram (results_DSSE.err_Ireal(:,x))
    end
    hold off
    S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
    C = regexp(S, '*', 'split');
    legend(C{1:end-1})
    grid on
    title('error emp output line current real')
    subplot(2,1,2)
    histogram (results_DSSE.err_Iimag(:,1))
    hold on
    for x = 2 : GridData.Lines_num
        histogram (results_DSSE.err_Iimag(:,x))
    end
    hold off
    S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
    C = regexp(S, '*', 'split');
    legend(C{1:end-1})
    grid on
    title('error emp output line current imaginary')
    
end

end


