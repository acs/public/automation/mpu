%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to test the DSSE during time evolution
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ results_DSSE_Uncertainty,SD,results_DSSE] = test_DSSE_TimeEvolution( Nt, time_delta,DM,SD1,VS,GridData,Accuracy,Combination_devices,Test_SetUp,plot_figure,type_est)
% code to test the perfomance of the state estimator in real time
%type_est == 1 :steady state estimator
%type_est == 2 :dynamic state estimator
%type_est == 3 :%non ideal batch steady estimator
%type_est == 4 :%recursive dynamic state estimator
calc_dyn_DSSE = 1;
results_DSSE=struct;results_DSSE.err_Vmagnitude=[];results_DSSE.err_Vphase=[];results_DSSE.Vmagn_status=[];results_DSSE.Vph_status=[];results_DSSE.Imagn_status=[];results_DSSE.Iph_status=[];results_DSSE.Vreal_status=[];results_DSSE.Vimag_status=[];results_DSSE.Ireal_status=[];results_DSSE.Iimag_status=[];results_DSSE.Vmagn_true=[];results_DSSE.Vph_true=[];results_DSSE.Imagn_true=[];results_DSSE.Iph_true=[];results_DSSE.Vreal_true=[];results_DSSE.Vimag_true=[];results_DSSE.Ireal_true=[];results_DSSE.Iimag_true=[];results_DSSE.err_Imagnitude=[];results_DSSE.err_Iphase=[];results_DSSE.err_Ireal=[];results_DSSE.err_Iimag=[];results_DSSE.err_Vreal=[];results_DSSE.err_Vimag=[];results_DSSE.err_Vmagnitude_in   =[];results_DSSE.err_Vphase_in=[];results_DSSE.err_Imagnitude_in  =[];results_DSSE.err_Iphase_in   =[];results_DSSE.err_Ireal_in   =[];results_DSSE.err_Iimag_in =[];
[Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD1);

Vmagn_status_t = zeros(GridData.Nodes_num,Nt);
Vph_status_t = zeros(GridData.Nodes_num,Nt);

if type_est == 1 || type_est == 2 || type_est == 4 || type_est == 5
    [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
elseif type_est == 3
    time_delta1 = (1 +  Combination_devices.common_delay_base )  * time_delta ;
    [ Tmatrix1,Forz_matrix1,Dist_matrix1] = calc_TDF(time_delta1,Atot,B_input,B_disturbance);
end

inv_count = find(DM.grid_supp+DM.grid_form,1,'last'); Gx = 0;
if (DM.PCC == 1) || sum(DM.grid_supp)>0
    if DM.PCC == 1; u(1,1) = DM.Eod;    u(2,1) = DM.Eoq;    end
    if sum(DM.grid_supp_PV)>0;  Gx = 1; u(2*DM.PCC+Gx,1) = DM.G;    end
    if sum(DM.grid_supp_PQ)>0;  for x = 1 : DM.Nnode;   u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,1) = DM.P_obj(x);   u(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,1) = DM.Q_obj(x);   end;
    end;    else; u = 0;    end;    if isempty(inv_count)==0;   for x = DM.ref : inv_count; if DM.grid_form(x)==1;  u(2*DM.PCC + Gx +2*sum(DM.grid_supp_PQ)+sum(DM.grid_form(1:x-1))+1,1) = DM.Voqn(x); end; end; end
u1 = u ;


for t = 1 : Nt
    [d] = create_disturbance(DM,VS);
    u = u1;
    if t > 20 || t < 25;  u = u1*2; end
    if t == 1 
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD1,CLINE_tot,GridData,SD1.x_status);
        SD = SD1;
    end
    if type_est == 1 %steady state estimator
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = Meas_true_vector + diag(sqrt(diag(R)))*randn(GridData.MeasNum,1);
       [Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status(:,t),Iph_status(:,t),inj_status(:,t)]   = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
                                                                                                             
    elseif type_est == 2 %dynamic state estimator
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = Meas_true_vector + diag(sqrt(diag(R)))*randn(GridData.MeasNum,1);
       [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status]   = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        
        [ Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status,Iph_status,inj_status ] = calc_xestimate(DM,GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
        
    elseif type_est == 3  %non ideal batch current estimator
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = Meas_true_vector + diag(sqrt(diag(R)))*randn(GridData.MeasNum,1);
        [Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status(:,t),Iph_status(:,t),inj_status(:,t)]   = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
        
        
    elseif type_est == 4  %recursive steady current estimator
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = Meas_true_vector + diag(sqrt(diag(R)))*randn(GridData.MeasNum,1);
        if t == 1
            [Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status(:,t),Iph_status(:,t),inj_status(:,t),P_preV] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
            P_pre = VS.var_status_absolute;
        else
            P_pre = P_pre + Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix);
            [Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status(:,t),Iph_status(:,t),inj_status(:,t),P_pre] = IRIDSSE_rec(Meas_vector_external,W,GridData,Test_SetUp,Imagn_status(:,t-1),Iph_status(:,t-1),inj_status_p(:,t-1),P_pre);
        end
        
    elseif type_est == 5  %recursive dynamic state estimator
        [Meas_true_vector] = calc_Mvector_external(GridData,PowerData);
        Meas_vector_external = Meas_true_vector + diag(sqrt(diag(R)))*randn(GridData.MeasNum,1);
        if t == 1
            [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);
            [ Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status(:,t),Iph_status(:,t),inj_status(:,t) ] = calc_xestimate(DM,GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
            
            P_pre = VS.var_status_absolute;
        else
            P_pre = Tmatrix * P_pre * transpose(Tmatrix) + Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix);
            [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_pre] = IRIDSSE_rec(Meas_vector_external,W,GridData,Test_SetUp,Imagn_status(:,t-1),Iph_status(:,t-1),inj_status(:,t-1),P_pre);
            [ Vmagn_status_t(:,t),Vph_status_t(:,t),Imagn_status(:,t),Iph_status(:,t),inj_status(:,t) ] = calc_xestimate(DM,GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u);
            
        end
    end
    
    
    if type_est == 1  || type_est == 2 || type_est == 4 || type_est == 5
        [SD] = calc_next_state_short(DM,SD,Tmatrix,Forz_matrix,Dist_matrix,CLINE_tot,time_delta,u,d);
        [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
        [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
        [results_DSSE]             = Data_Output(Vmagn_status_t(:,t),Vph_status_t(:,t),results_DSSE,GridData,PowerData);
        
    elseif type_est == 3
        SD1 = SD;
        [SD] = calc_next_state_short(DM,SD,Tmatrix1,Forz_matrix1,Dist_matrix1,CLINE_tot,time_delta1,u,d);%we move 1.3 ahead
        [Atot,B_input,B_disturbance,CLINE_tot] = calc_A_B_matrices(DM,SD);
        [ Tmatrix1,Forz_matrix1,Dist_matrix1] = calc_TDF(time_delta1,Atot,B_input,B_disturbance);
        [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
        [results_DSSE]             = Data_Output(Vmagn_status_t(:,t),Vph_status_t(:,t),results_DSSE,GridData,PowerData);
        [SD] = calc_next_state_short(DM,SD1,Tmatrix,Forz_matrix,Dist_matrix,CLINE_tot,time_delta,u,d);%we move 1 ahead
        [PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
    end
    
end
%this are the time evolution of the status

[results_DSSE_Uncertainty] = calc_Unc_DSSE(results_DSSE,PowerData,GridData,Combination_devices,Accuracy,Test_SetUp);

rms_err_Ireal_emp = results_DSSE_Uncertainty.rms_err_Ireal_emp
rms_err_Iimag_emp = results_DSSE_Uncertainty.rms_err_Iimag_emp

if plot_figure == 1
    time_test = linspace(0,time_delta*Nt,Nt);
    plot_estimation_true_figure(DM,GridData,results_DSSE,time_test)
end
case2='eigenvalues after time evolution'
[eigA,t_cost,damp,pulse,part_fact,eig_index] = calc_eig(Atot);

end
