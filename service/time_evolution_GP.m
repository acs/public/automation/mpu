%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to create single time simulations of actual and estimated
% states for a general purpose system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x_status_t_out,x_status_est_t_out] = time_evolution_GP(Atot,B_input,B_disturbance,C,SD,VS,dev_input,time_delta,type_unc,Ts,Nw,filter_on)
%     type_unc = 0; 'Steady State estimation and system'
%     type_unc = 1; 'Steady State estimation and dynamic system with StdDev'
%     type_unc = 2; 'Steady State estimation and dynamic system with RMSE'
%     type_unc = 3; 'Dynamic State estimation and dynamic system with RMSE'
%     type_unc = 4; 'Steady State estimation and dynamic system with delay batch measurements with RMSE'
%     type_unc = 5; 'Steady State estimation and steady system with recursive estimator'
%     type_unc = 6; 'Steady State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 7; 'Dynamic State estimation and dynamic system with recursive estimator and RMSE'
%     type_unc = 8; 'Steady State estimation and dynamic system with delay splitted measurements with RMSE'
%     type_unc = 9; 'Dynamic State estimation and dynamic system with delay splitted measurements with RMSE'


var_status_absolute = diag(VS.var_status_absolute);
std_dev_status_absolute = sqrt(var_status_absolute);

P_fin2 = zeros(size(VS.var_status_absolute));

[ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
Tmatrix_eq = eye(size(Tmatrix));
Forz_matrix_eq= zeros(size(Forz_matrix));
Dist_matrix_eq= zeros(size(Dist_matrix));
Rtot = zeros(size(VS.R));
if type_unc == 4
    time_delta = (1 +  VS.meas_delay ) * time_delta ;
    
elseif  type_unc == 6 %RMSE in dynamic conditions and dynamic state estimation  with recursive (KALMAN FILTER)
    Nt = ceil( time_delta / Ts);
    [ TmatrixTs,~,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
    if time_delta > Ts
        for t = 1 : Nt
            Tmatrix_eq = Tmatrix_eq * TmatrixTs;
            M_tot = zeros(size(VS.var_status_absolute));
            if filter_on == 1
                if t > 1
                    for s = 1 : t-1
                        V(s).V = VS.F_matrix^(s) * W(t-s).W; %correlation
                        M(s).M =  Dist_matrixTs * V(s).V * transpose(Dist_matrixTs) ;
                        M_tot =  M_tot + TmatrixTs * M(s).M + M(s).M * transpose(TmatrixTs) ;
                    end
                    W(t).W = VS.F_matrix * W(t-1).W * transpose(VS.F_matrix) + VS.var_disturbance_absolute;
                    P_fin2 = TmatrixTs * P_fin2 * transpose(TmatrixTs) + Dist_matrixTs * W(t).W * transpose(Dist_matrixTs) + M_tot;
                else
                    W(t).W = VS.var_disturbance_absolute;
                    P_fin2 = Dist_matrixTs * W(t).W * transpose(Dist_matrixTs);
                end
            else
                P_fin2 = TmatrixTs * P_fin2 * transpose(TmatrixTs) + Dist_matrixTs * VS.var_disturbance_absolute * transpose(Dist_matrixTs)  ;
            end
        end
        P_fin4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * VS.P_delta_x * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
    else
        P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
        P_fin4 = (Tmatrix-eye(size(Tmatrix))) * VS.P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
    end
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute; %VS.var_status_absolute;
            P_pre  =  diag(diag(P_post)) + P_fin2 + P_fin4  ;
        else
            [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF((w-1)*time_delta,Atot,B_input,B_disturbance);
            P_delta_x = Tmatrix_add      * VS.P_delta_x * transpose(Tmatrix_add)      + ...
                Forz_matrix_add  * diag((SD.input*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
                Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
            P_fin4w(w).P_fin4 = (Tmatrix-eye(size(Tmatrix))) * P_delta_x * transpose((Tmatrix-eye(size(Tmatrix)))) ;
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  = diag(diag(P_post)) + P_fin2 + P_fin4w(w).P_fin4 ;
        end
    end
    P_fin = P_post;
    
elseif  type_unc == 7 %RMSE in dynamic conditions and dynamic state estimation  with recursive (KALMAN FILTER)
    Nt = ceil( time_delta / Ts);
    [ TmatrixTs,~,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
    if time_delta > Ts
        for t = 1 : Nt
            M_tot = zeros(size(VS.var_status_absolute));
            if filter_on == 1
                if t > 1
                    for s = 1 : t-1
                        V(s).V = VS.F_matrix^(s) * W(t-s).W; %correlation
                        M(s).M =  Dist_matrixTs * V(s).V * transpose(Dist_matrixTs) ;
                        M_tot =  M_tot + TmatrixTs * M(s).M + M(s).M * transpose(TmatrixTs) ;
                    end
                    W(t).W = VS.F_matrix * W(t-1).W * transpose(VS.F_matrix) + VS.var_disturbance_absolute;
                    P_fin2 = TmatrixTs * P_fin2 * transpose(TmatrixTs) + Dist_matrixTs * W(t).W * transpose(Dist_matrixTs) + M_tot;
                else
                    W(t).W = VS.var_disturbance_absolute;
                    P_fin2 = Dist_matrixTs * W(t).W * transpose(Dist_matrixTs);
                end
            else
                P_fin2 = TmatrixTs * P_fin2 * transpose(TmatrixTs) + Dist_matrixTs * VS.var_disturbance_absolute * transpose(Dist_matrixTs)  ;
            end
        end
    else
        P_fin2 = Dist_matrix * VS.var_disturbance_absolute * transpose(Dist_matrix) ; %covariance due to external disturbances
    end
    for w = 1 : Nw
        if w == 1
            P_post = VS.var_status_absolute;
            P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
        else
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
            P_pre  =  Tmatrix * P_post * transpose(Tmatrix) + P_fin2 ;
        end
    end
    P_fin = P_post;
    
elseif type_unc == 8 %we have delay split in measurements with time_delta_meas vector
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(VS.time_delta_individual_meas == time_delta_meas(m));
        [ Tmatrix_m(m).Tmatrix,Forz_matrix_m(m).Forz_matrix,Dist_matrix_m(m).Dist_matrix] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
    end
    
    %%%%%%%% case where we adapt the covariance of the state estimator
%     for m = 1 : length(time_delta_meas) %for every time update of the measurements
%         C_m = C (m_meas(m).meas,:); %we extract the corresponding element of the Jacobian
%         
%         if m > 1
%             [ Tmatrix_add,Forz_matrix_add,Dist_matrix_add] = calc_TDF(sum(t_delay(1:m-1)) *time_delta,Atot,B_input,B_disturbance);
%             P_delta_x = Tmatrix_add      * VS.P_delta_x * transpose(Tmatrix_add)      + ...
%                 Forz_matrix_add  * diag((SD.input*dev_input).^2)      * transpose(Forz_matrix_add ) + ...
%                 Dist_matrix_add  * VS.var_disturbance_absolute        * transpose(Dist_matrix_add ) ;
%         else
%             P_delta_x = VS.P_delta_x;
%         end
%         [Tmatrix_t,Forz_matrix_t,Dist_matrix_t] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
%         Dist_matrix_eq = Dist_matrix_t + Tmatrix_t * Dist_matrix_eq;
%         Forz_matrix_eq = Forz_matrix_t + Tmatrix_t * Forz_matrix_eq;
%         Tmatrix_eq = Tmatrix_eq * Tmatrix_t;
%         
%         
%         for n = m + 1 : length(time_delta_meas)
%             [Tmatrix_t,Forz_matrix_t,Dist_matrix_t] = calc_TDF( t_delay(n)*time_delta,Atot,B_input,B_disturbance);
%             Dist_matrix_eq = Dist_matrix_t + Tmatrix_t * Dist_matrix_eq;
%             Forz_matrix_eq = Forz_matrix_t + Tmatrix_t * Forz_matrix_eq;
%             Tmatrix_eq = Tmatrix_eq * Tmatrix_t;
%         end
%         
%         P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
%         P_delta3 =  Forz_matrix_eq * diag((SD.input*dev_input).^2) * transpose(Forz_matrix_eq) ;
%         P_delta4 = (Tmatrix_eq-eye(size(Tmatrix_eq))) * P_delta_x * transpose((Tmatrix_eq-eye(size(Tmatrix_eq)))) ;
%         P_delta = P_delta2 + P_delta3 + P_delta4;
%         Rtot(m_meas(m).meas,m_meas(m).meas) = Rtot(m_meas(m).meas,m_meas(m).meas) + C_m * P_delta * transpose(C_m);
%     end
%     R = Rtot + VS.R;
%     G1 = C' * inv(R) * C;
%     VS.R = R;
%     VS.var_status_absolute = inv(G1);
%     VS.Gain_matrix = G1;
%     VS.invG_HW = VS.var_status_absolute * C' * inv(R);
    %%%%%%%
    
    
elseif type_unc == 9 %Dynamic case
    time_delta_meas = (sort(unique(VS.time_delta_individual_meas),'descend'));
    for m = 1 : length(time_delta_meas)
        if  m == 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        elseif m == 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m < length(time_delta_meas)
            t_delay(m) = time_delta_meas(m) - time_delta_meas(m+1);
        elseif m > 1 && m == length(time_delta_meas)
            t_delay(m) = time_delta_meas(m);
        end
        m_meas(m).meas = find(VS.time_delta_individual_meas == time_delta_meas(m));
        [ Tmatrix_m(m).Tmatrix,Forz_matrix_m(m).Forz_matrix,Dist_matrix_m(m).Dist_matrix] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
    end
    
%     %%%%%%%% case where we adapt the covariance of the state estimator
%     for m = 1 : length(time_delta_meas) %for every time update of the measurements
%         C_m = C (m_meas(m).meas,:); %we extract the corresponding element of the Jacobian
%         
%         [Tmatrix_t,~,Dist_matrix_t] = calc_TDF(t_delay(m)*time_delta,Atot,B_input,B_disturbance);
%         Dist_matrix_eq = Dist_matrix_t + Tmatrix_t * Dist_matrix_eq;
%         
%         
%         for n = m + 1 : length(time_delta_meas)
%             [Tmatrix_t,~,Dist_matrix_t] = calc_TDF( t_delay(n)*time_delta,Atot,B_input,B_disturbance);
%             Dist_matrix_eq = Dist_matrix_t + Tmatrix_t * Dist_matrix_eq;
%         end
%         
%         P_delta2 =  Dist_matrix_eq * VS.var_disturbance_absolute * transpose(Dist_matrix_eq) ;
%         P_delta = P_delta2 ;
%         Rtot(m_meas(m).meas,m_meas(m).meas) = Rtot(m_meas(m).meas,m_meas(m).meas) + C_m * P_delta * transpose(C_m);
%     end
%     R = Rtot + VS.R;
%     G1 = C' * inv(R) * C;
%     VS.R = R;
%     VS.var_status_absolute = inv(G1);
%     VS.Gain_matrix = G1;
%     VS.invG_HW = VS.var_status_absolute * C' * inv(R);
    %%%%%%%
end







if type_unc == 0 % steady state case
    x_status(:,1) = SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status)) ;
    x_status_t_out(:,1) = x_status(:,1);
    for w = 2 : Nw
        x_status_t_out(:,w) = x_status_t_out(:,w-1);
    end
    
elseif type_unc == 1 || type_unc == 2 || type_unc == 3 || type_unc == 4
    if type_unc == 1
        u = SD.input;
    else
        u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
    end 
    for w = 1 : Nw
        if time_delta > Ts
            Nt = ceil(time_delta / Ts);
            extra_time = time_delta - Nt*Ts;
            [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
            for t = 1 : Nt
                if filter_on == 0 || t == 1
                    d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
                else
                    d = d * VS.F_matrix + SD.unc_disturbance_white.*randn(size(B_disturbance,2),1);
                end
                if t == 1
                    if w == 1
                        x_status_t(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
                    else
                        x_status_t(:,1) =  x_status_t(:,5);
                    end
                else
                    x_status_t(:,1) = x_status_t(:,5);
                end
                if t == Nt
                    [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts + extra_time,Atot,B_input,B_disturbance);
                end
                x_status_t(:,2) = TmatrixTs * x_status_t(:,1);
                x_status_t(:,3) = Forz_matrixTs * u;
                x_status_t(:,4) = Dist_matrixTs * d;
                x_status_t(:,5) = x_status_t(:,2) + x_status_t(:,3) + x_status_t(:,4);
            end
            x_status(:,5) = x_status_t(:,5);
            
        else
            d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
            if w == 1
                x_status(:,1) = SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
            else
                x_status(:,1) = x_status(:,5);
            end
            x_status(:,2) = Tmatrix * x_status(:,1);
            x_status(:,3) = Forz_matrix * u;
            x_status(:,4) = Dist_matrix * d;
            x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
        end
        x_status_t_out(:,w) = x_status(:,5);
    end
    
elseif type_unc == 5
    u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
    d = zeros(size(B_disturbance,2),1);
    for w = 1 : Nw
        if time_delta > Ts
            Nt = ceil(time_delta / Ts);
            extra_time = time_delta - Nt*Ts;
            [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
            for t = 1 : Nt
                if t == 1
                    if w == 1
                        x_status_w(w).x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
                    else
                        x_status_w(w).x_status(:,1) = x_status_w(w-1).x_status(:,5);
                    end
                    x_status_t(:,1) = x_status_w(w).x_status(:,1);
                else
                    x_status_t(:,1) = x_status_t(:,5);
                end
                if t == Nt
                    [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts + extra_time,Atot,B_input,B_disturbance);
                end
                x_status_t(:,5) = x_status_t(:,1) ; %steady state
            end
            x_status_w(w).x_status(:,5) = x_status_t(:,5);
            
        else
            if w == 1
                x_status_w(w).x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
            else
                x_status_w(w).x_status(:,1) = x_status_w(w-1).x_status(:,5);
            end
            x_status_w(w).x_status(:,5) = x_status_w(w).x_status(:,1);
        end
        x_status_t_out(:,w) = x_status_w(w).x_status(:,5);
    end
    
elseif type_unc == 6 || type_unc == 7
    u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
    for w = 1 : Nw
        if time_delta > Ts
            Nt = ceil(time_delta / Ts);
            extra_time = time_delta - Nt*Ts;
            [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts,Atot,B_input,B_disturbance);
            for t = 1 : Nt
                if filter_on == 0 || t == 1
                    d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
                else
                    d = d * VS.F_matrix + SD.unc_disturbance_white.*randn(size(B_disturbance,2),1);
                end
                if t == 1
                    if w == 1
                        x_status_w(w).x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
                    else
                        x_status_w(w).x_status(:,1) = x_status_w(w-1).x_status(:,5);
                    end
                    x_status_t(:,1) = x_status_w(w).x_status(:,1);
                else
                    x_status_t(:,1) = x_status_t(:,5);
                end
                if t == Nt
                    [ TmatrixTs,Forz_matrixTs,Dist_matrixTs] = calc_TDF(Ts + extra_time,Atot,B_input,B_disturbance);
                end
                x_status_t(:,2) = TmatrixTs * x_status_t(:,1);
                x_status_t(:,3) = Forz_matrixTs * u;
                x_status_t(:,4) = Dist_matrixTs * d;
                x_status_t(:,5) = x_status_t(:,2) + x_status_t(:,3) + x_status_t(:,4);
            end
            x_status_w(w).x_status(:,5) = x_status_t(:,5);
            
        else
            d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
            if w == 1
                x_status_w(w).x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
            else
                x_status_w(w).x_status(:,1) = x_status_w(w-1).x_status(:,5);
            end
            x_status_w(w).x_status(:,2) = Tmatrix * x_status_w(w).x_status(:,1);
            x_status_w(w).x_status(:,3) = Forz_matrix * u;
            x_status_w(w).x_status(:,4) = Dist_matrix * d;
            x_status_w(w).x_status(:,5) = x_status_w(w).x_status(:,2) + x_status_w(w).x_status(:,3) + x_status_w(w).x_status(:,4);
        end
        x_status_t_out(:,w) = x_status_w(w).x_status(:,5);
    end
    
elseif type_unc == 8 || type_unc == 9
    u = SD.input.*(1+dev_input*randn(size(B_disturbance,2),1));
    d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
    for w = 1 : Nw
        if w == 1 
    x_status(:,1) =  SD.x_status  + (diag(VS.P_delta_x).^0.5) .* randn(size(SD.x_status));
        else
    x_status(:,1) =  x_status(:,5);
        end
    for m = 1 : length(t_delay)
        d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
        if m == 1
            x_status_m(m).x_status(:,1) = x_status(:,1);  
        else
            x_status_m(m).x_status(:,1) =  x_status_m(m-1).x_status(:,5);
        end
        x_status_w(w).x_status_m(m).x_status(:,1) = x_status_m(m).x_status(:,1);
        x_status_m(m).x_status(:,2) = Tmatrix_m(m).Tmatrix * x_status_m(m).x_status(:,1);
        x_status_m(m).x_status(:,3) = Forz_matrix_m(m).Forz_matrix * u;
        x_status_m(m).x_status(:,4) = Dist_matrix_m(m).Dist_matrix * d;
        x_status_m(m).x_status(:,5) = x_status_m(m).x_status(:,2) + x_status_m(m).x_status(:,3) + x_status_m(m).x_status(:,4);
    end
    x_status(:,2) = Tmatrix * x_status_m(length(t_delay)).x_status(:,5);
    x_status(:,3) = Forz_matrix * u;
    x_status(:,4) = Dist_matrix * d;
    x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
    
    x_status_t_out(:,w) = x_status(:,5);
    end
end

if type_unc == 0
    for w = 2 : Nw
        x_status_true = x_status_t_out(:,w-1);
        z_measure_true = C * x_status_true;
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est = VS.invG_HW * z_measure_error;
        x_status_est_t_out(:,w) = x_status_est;
    end
    x_status_est_t_out(:,1) = x_status_est_t_out(:,2);
    err_est_state(:,1) = (x_status_est - x_status(:,1));
    
elseif type_unc == 1 || type_unc == 2 || type_unc == 4
    for w = 2 : Nw
    x_status_true = x_status_t_out(:,w);
    z_measure_true = C * x_status_true;
    z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
    x_status_est = VS.invG_HW * z_measure_error;
    x_status_est_t_out(:,w) = x_status_est;
    end
    x_status_est_t_out(:,1) = x_status_est_t_out(:,2);
    err_est_state(:,1) = (x_status_est - x_status(:,5));
    
elseif type_unc == 3
    for w = 2 : Nw
        x_status_true = x_status_t_out(:,w-1);
        z_measure_true = C * x_status_true;
        z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
        x_status_est(:,1) = VS.invG_HW * z_measure_error;
        x_status_est(:,2) = Tmatrix * x_status_est(:,1);
        x_status_est(:,3) = Forz_matrix * u;
        x_status_est(:,5) = x_status_est(:,2) + x_status_est(:,3);
        x_status_est_t_out(:,w) = x_status_est(:,5);
    end
    x_status_est_t_out(:,1) = x_status_est_t_out(:,2);
    err_est_state(:,1) = (x_status_est(:,5) - x_status(:,5));
    
elseif  type_unc == 5
    
    for w = 1 : Nw
        x_status_true = x_status_w(w).x_status(:,1);
        if w == 1
            z_measure_true = C * x_status_true;
            z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
            x_status_est_post = VS.invG_HW * z_measure_error;
            P_post = VS.var_status_absolute;
        else
            %P_pre = diag(diag(P_post)) ;
            P_pre = P_post;
            x_status_est_pre = x_status_est_post;
            z_measure_true  = C * x_status_true;
            z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
            K = P_pre * transpose(C) * inv( C * P_pre * transpose(C) + VS.R );
            x_status_est_post = x_status_est_pre + K*(z_measure_error - C * x_status_est_pre);
            P_post = inv( inv(P_pre) + VS.Gain_matrix );
        end
        x_status_est_t_out(:,w) = x_status_est_post;
        err_est_state_out =  x_status_t_out(:,w)-x_status_est_t_out(:,w);
    end
    x_status_est_t_out(:,1) = x_status_est_t_out(:,2);
    x_status_est(:,1) = x_status_est_post;
    err_est_state(:,1) = (x_status_est(:,1) - x_status_w(Nw).x_status(:,5));
    
elseif type_unc == 6
    for w = 1 : Nw
        x_status_true = x_status_w(w).x_status(:,1);
        if w == 1
            z_measure_true = C * x_status_true;
            z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
            x_status_est_post = VS.invG_HW * z_measure_error;
            P_post = VS.var_status_absolute;
        else
            P_pre = P_post + P_fin2 + P_fin4w(w).P_fin4;
            %x_status_est_pre = Tmatrix * x_status_est_post + Forz_matrix * u;
            
            x_status_est_pre = x_status_est_post;
            
            z_measure_true = C * x_status_true;
            z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
            K = P_pre * transpose(C) * inv( C * P_pre * transpose(C) + VS.R);
            x_status_est_post = x_status_est_pre + K*(z_measure_error - C * x_status_est_pre);
            P_post = inv( inv(P_pre) + VS.Gain_matrix);
        end
        x_status_est_t_out(:,w) = x_status_est_post;
    end
    x_status_est(:,1)  = x_status_est_post;
    err_est_state(:,1) = (x_status_est(:,1) - x_status_w(Nw).x_status(:,5));
    
    
elseif type_unc == 7
    
    for w = 1 : Nw
        x_status_true = x_status_w(w).x_status(:,1);
        if w == 1
            z_measure_true = C * x_status_true;
            z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
            x_status_est_post = VS.invG_HW * z_measure_error;
            P_post = VS.var_status_absolute;
        else
            P_pre = Tmatrix *P_post* transpose(Tmatrix) + P_fin2;
            x_status_est_pre = Tmatrix * x_status_est_post + Forz_matrix * u;
            z_measure_true = C * x_status_true;
            z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
            K = P_pre * transpose(C) * inv( C * P_pre * transpose(C) + VS.R);
            x_status_est_post = x_status_est_pre + K*(z_measure_error - C * x_status_est_pre);
            P_post = inv( inv(P_pre) + VS.Gain_matrix);
        end
    x_status_est(:,1) = x_status_est_post;
    x_status_est(:,2) = Tmatrix * x_status_est(:,1);
    x_status_est(:,3) = Forz_matrix * u;
    x_status_est(:,5) = x_status_est(:,2) + x_status_est(:,3);
    x_status_est_t_out(:,w) = x_status_est(:,5);
    end

    
    err_est_state(:,1) = (x_status_est(:,5) - x_status_w(Nw).x_status(:,5));
    
elseif type_unc == 8
    for w = 1 : Nw
    for m = 1 : length(t_delay)
        x_status_true = x_status_w(w).x_status_m(m).x_status(:,1);
        C_m = C (m_meas(m).meas,:);
        z_measure_true(m_meas(m).meas,1) = (C_m * x_status_true)';
    end
    z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
    x_status_est = VS.invG_HW * z_measure_error;
    err_est_state(:,1) = (x_status_est - x_status(:,5));
    x_status_est_t_out(:,w) = x_status_est;
    end
elseif type_unc == 9
    for w = 1 : Nw
    for m = 1 : length(t_delay)
        x_status_true = x_status_w(w).x_status_m(m).x_status(:,1);
        C_m = C (m_meas(m).meas,:);
        z_measure_true(m_meas(m).meas,1) = (C_m * x_status_true)';
    end
    z_measure_error = z_measure_true .* ( 1 + VS.unc_measure.*randn(size(z_measure_true,1),1));
    x_status_est(:,1) = VS.invG_HW * z_measure_error;
    x_status_est(:,2) = Tmatrix * x_status_est(:,1);
    x_status_est(:,3) = Forz_matrix * u;
    x_status_est(:,5) = x_status_est(:,2) + x_status_est(:,3);
    err_est_state(:,1) = (x_status_est(:,5) - x_status(:,5));
     x_status_est_t_out(:,w) = x_status_est(:,5);
    end
end




end

