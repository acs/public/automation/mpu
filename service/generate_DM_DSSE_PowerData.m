%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate the power data for the dynamic system amd the distribution
% system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [PowerData,GridData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,x_status)

i_lineD=zeros(DM.Nline,1);i_lineQ=zeros(DM.Nline,1);i_lineD0=zeros(1,1);i_lineQ0=zeros(1,1);ioD=zeros(DM.Nnode,1);ioQ=zeros(DM.Nnode,1);vbA=zeros(DM.Nnode,1);vbP=zeros(DM.Nnode,1);i_lineA=zeros(DM.Nline,1);i_lineP=zeros(DM.Nline,1);i_lineA0=zeros(1,1);i_lineP0=zeros(1,1);ioA=zeros(DM.Nnode,1);ioP=zeros(DM.Nnode,1);

if DM.PCC == 1
    vbD = [DM.Eod;SD.vbD];
    vbQ = [DM.Eoq;SD.vbQ];
    vbA(1,1) = sqrt( vbD(1,1)^2 + vbQ(1,1)^2);
    vbP(1,1) = phase( vbD(1,1)+ 1i*vbQ(1,1));
    i_lineD0 = x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 1,1);
    i_lineQ0 = x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2,1);
    i_lineA0 = sqrt(i_lineD0^2 + i_lineQ0^2);
    i_lineP0 = phase( i_lineD0 + 1i*i_lineQ0);
    i_lineDQ(1,1) = i_lineD0;
    i_lineDQ(2,1) = i_lineQ0;
else
    vbD = SD.vbD;
    vbQ = SD.vbQ;
end
for x = 1 : DM.Nline
    i_lineD(x,1)=x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*DM.PCC + 2*(x-1)+1,1);
    i_lineQ(x,1)=x_status(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*DM.PCC + 2*(x-1)+2,1);
    i_lineA(x,1) = sqrt(i_lineD(x,1)^2 + i_lineQ(x,1)^2);
    i_lineP(x,1) = phase( i_lineD(x,1) + 1i*i_lineQ(x,1) );
    i_lineDQ(2*DM.PCC+2*(x-1)+1,1) = i_lineD(x,1);
    i_lineDQ(2*DM.PCC+2*(x-1)+2,1) = i_lineQ(x,1);
end
ioDQ = + CLINE_tot * i_lineDQ;
for x = 1 : DM.Nnode
    ioD(x+DM.PCC,1) = ioDQ(2*(x-1)+1);
    ioQ(x+DM.PCC,1) = ioDQ(2*(x-1)+2);
    ioA(x+DM.PCC,1) = sqrt( ioD(x+DM.PCC,1)^2 + ioQ(x+DM.PCC,1)^2);
    ioP(x+DM.PCC,1) = phase(ioD(x+DM.PCC,1) + 1i* ioQ(x+DM.PCC,1));
    vbA(x+DM.PCC,1) = sqrt( vbD(x+DM.PCC,1)^2 + vbQ(x+DM.PCC,1)^2);
    vbP(x+DM.PCC,1) = phase( vbD(x+DM.PCC,1)+ 1i*vbQ(x+DM.PCC,1)) ;
end
% we shift all the angles because in the DSSE we use the d axis as reference
vbP = vbP - pi/2;
ioP = ioP - pi/2;
i_lineP = i_lineP - pi/2;
i_lineP0 = i_lineP0 - pi/2;
delta_P = vbP(1,1); % also we have the angle of the slack bus = 0

for x = 1 : DM.Nnode+DM.PCC
    vbP(x,1) = (vbP(x,1) - delta_P);
    vbD(x,1) = (vbA(x,1)*cos(vbP(x,1)));
    vbQ(x,1) = (vbA(x,1)*sin(vbP(x,1)));
    ioP(x,1) = (ioP(x,1) - delta_P);
    ioD(x,1) = (ioA(x,1)*cos(ioP(x,1)));
    ioQ(x,1) = (ioA(x,1)*sin(ioP(x,1)));
end
vbD = vbD / GridData.base_voltage;
vbQ = vbQ / GridData.base_voltage;
ioD = ioD / GridData.base_current;
ioQ = ioQ / GridData.base_current;

for b = 1 : DM.Nline
    i_lineP(b,1) = (i_lineP(b,1) - delta_P);
    i_lineD(b,1) = i_lineA(b,1)*cos(i_lineP(b,1));
    i_lineQ(b,1) = i_lineA(b,1)*sin(i_lineP(b,1));
end
i_lineD = i_lineD / GridData.base_current;
i_lineQ = i_lineQ / GridData.base_current;
if DM.PCC == 1
    i_lineP0 = (i_lineP0 - delta_P);
    i_lineD0 = i_lineA0*cos(i_lineP0);
    i_lineQ0 = i_lineA0*sin(i_lineP0);
    i_lineD0 = i_lineD0 / GridData.base_current;
    i_lineQ0 = i_lineQ0 / GridData.base_current;
end


%all quantities have been recalculated as per units
%but considering the different convention used for loads and generators
ioD = - ioD;
ioQ = - ioQ;
i_lineA0 = sqrt(i_lineD0^2 + i_lineQ0^2);
i_lineP0 = phase(i_lineD0 + 1i*i_lineQ0);
i_lineA = sqrt(i_lineD.^2 + i_lineQ.^2);
i_lineP = phase(i_lineD + 1i*i_lineQ);
% we recalculate the voltages based on the currents
% this is because the voltage is not part of the system state
Volts(1,1) = vbD(1,1) + 1i*vbQ(1,1);
if DM.PCC == 1
    i_lineDQ =  [i_lineD0 + 1i*i_lineQ0; i_lineD + 1i*i_lineQ];
else
    i_lineDQ =  i_lineD + 1i*i_lineQ;
end
for b = 1 : DM.Nline+DM.PCC
    Volts(GridData.topology(3,b),1) = Volts(GridData.topology(2,b),1) - (GridData.R1(b) + 1i*GridData.X1(b))*i_lineDQ(b);
end
vbD = real(Volts);
vbQ = imag(Volts);
vbA = abs(Volts);
vbP = (phase(Volts));

PowerData = struct;
if DM.PCC == 1
    PowerData.Pflow(1,1) = (vbD(1,1).*i_lineD0 + vbQ(1,1).*i_lineQ0);
    PowerData.Qflow(1,1) = - (vbD(1,1).*i_lineQ0 - vbQ(1,1).*i_lineD0);
end

for b = 1 : DM.Nline
    PowerData.Pflow(b+DM.PCC,1) = vbD(DM.topology(1,b),1).*i_lineD(b,1) + vbQ(DM.topology(1,b),1).*i_lineQ(b,1);
    PowerData.Qflow(b+DM.PCC,1) = - (vbD(DM.topology(1,b),1).*i_lineQ(b,1) - vbQ(DM.topology(1,b),1).*i_lineD(b,1));
end

PowerData.Vmagn = vbA;
PowerData.Vph =  vbP;
PowerData.Pinj = vbD.*ioD + vbQ.*ioQ;
PowerData.Qinj = - (vbD.*ioQ - vbQ.*ioD);
if DM.PCC == 1
    PowerData.Imagn = [i_lineA0;i_lineA];
    PowerData.Iph   = [i_lineP0;i_lineP];
else
    PowerData.Imagn = i_lineA;
    PowerData.Iph = i_lineP;
end

for x = 1 : DM.Nnode
    if DM.grid_form(x)~=0
        PowerData.x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+1:11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+11,1) ...
            = x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+1:11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+11,1)./ ....
            GridData.base_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+1:11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))+11,1);
        
        
    elseif DM.grid_supp(x)~=0
        PowerData.x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))...
            +1:11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))...
            +10,1) = x_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))...
            +1:11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))...
            +10,1)./GridData.base_status(11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))...
            +1:11*sum(DM.grid_form(1:x-1))+10*sum(DM.grid_supp(1:x-1))...
            +10,1);
        
    end
end
for x = 1 : DM.Nnode
    if DM.plac_load(x)~=0
        PowerData.x_status(11*DM.NGF+10*DM.NGS...
            +2*sum(DM.plac_load(1:x-1))+1:11*DM.NGF+10*DM.NGS...
            +2*sum(DM.plac_load(1:x-1))+2,1) = x_status(11*DM.NGF+10*DM.NGS...
            +2*sum(DM.plac_load(1:x-1))+1:11*DM.NGF+10*DM.NGS...
            +2*sum(DM.plac_load(1:x-1))+2,1)./GridData.base_status(11*DM.NGF+10*DM.NGS...
            +2*sum(DM.plac_load(1:x-1))+1:11*DM.NGF+10*DM.NGS...
            +2*sum(DM.plac_load(1:x-1))+2,1);
    end
end
end