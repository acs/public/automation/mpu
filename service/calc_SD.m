%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate the initial status of the system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ SD ] = calc_SD( DM )
SD = struct; %status data

%this are the time initial guesses of the status (arbitrary value)
SD.vbD = 0* ones(DM.Nnode,1);
SD.vbQ = (DM.Vn* ones(DM.Nnode,1));
SD.delta = 0.0001+0* ones(DM.Nnode,1);
SD.P =  0* ones(DM.Nnode,1);
SD.Q =  0* ones(DM.Nnode,1);
SD.fi_d =  0* ones(DM.Nnode,1);
SD.fi_q =  0* ones(DM.Nnode,1);
SD.gamma_d =  0* ones(DM.Nnode,1);
SD.gamma_q =  0* ones(DM.Nnode,1);
SD.ild = 0* ones(DM.Nnode,1);
SD.ilq =  0* ones(DM.Nnode,1);
SD.vod =  0* ones(DM.Nnode,1);
SD.voq =  DM.Vn* ones(DM.Nnode,1);
SD.iod =  0* ones(DM.Nnode,1);
SD.ioq =  0* ones(DM.Nnode,1);
SD.fi_PLL =  0* ones(DM.Nnode,1);
SD.vod_f =  0* ones(DM.Nnode,1);
SD.i_loadD =  0* ones(DM.Nnode,1);
SD.i_loadQ =  0* ones(DM.Nnode,1);
SD.i_lineD =  0* ones(DM.Nline,1);
SD.i_lineQ =  0* ones(DM.Nline,1);
SD.i_lineD0 = 0;
SD.i_lineQ0 = 0;
SD.ioA=0*ones(DM.Nnode,1);
SD.ioP=0*ones(DM.Nnode,1);
SD.i_lineA0=0;
SD.i_lineP0=0;
SD.i_loadA=0*ones(DM.Nnode,1);
SD.i_loadP=0*ones(DM.Nnode,1);
SD.i_lineA=0*ones(DM.Nline,1);
SD.i_lineP=0*ones(DM.Nline,1);


SD.i_LD = SD.i_loadD; 
SD.i_LQ = SD.i_loadQ; 
SD.V0D = SD.vbD; 
SD.V0Q = SD.vbQ; 
SD.V0 = sqrt(SD.V0D.^2 + SD.V0Q.^2);

for x = 1 : DM.Nnode
    SD.Tsinv(x,:) = [cos(SD.delta(x)) sin(SD.delta(x)), -sin(SD.delta(x)) cos(SD.delta(x))];
    SD.ioD(x,1) = SD.Tsinv(x,1:2)*[SD.iod(x);SD.ioq(x)]; SD.ioQ(x,1) = SD.Tsinv(x,3:4)*[SD.iod(x);SD.ioq(x)];
end

% in this function the equilibrium initial values are calculated
[SD] = INIT_CONDITIONS_NL(DM,SD);

end

