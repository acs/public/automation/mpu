%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to build the vector SD.x_status from the status variables
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [SD]=calc_x_status(DM,SD)

Nnode = DM.Nnode ;
Nline = DM.Nline ;
Nload = DM.Nload ;
ref = DM.ref ;
NGF = DM.NGF ;
NGS = DM.NGS ;
PCC = DM.PCC ;
grid_form = DM.grid_form ;
grid_supp = DM.grid_supp ;
plac_load = DM.plac_load ;
P = SD.P ;
Q = SD.Q ;
fi_d = SD.fi_d ;
fi_q = SD.fi_q ;
gamma_d = SD.gamma_d ;
gamma_q = SD.gamma_q ;
ild = SD.ild ;
ilq = SD.ilq ;
vod = SD.vod ;
voq = SD.voq ;
fi_PLL = SD.fi_PLL ;
i_LD = SD.i_LD ;
i_LQ = SD.i_LQ ;
i_lineD = SD.i_lineD ;
i_lineQ = SD.i_lineQ ;
i_lineD0 = SD.i_lineD0 ;
i_lineQ0 = SD.i_lineQ0 ;

x_status = zeros(11*NGF+10*NGS+2*Nload+2*(Nline+PCC),1);
inv_count = find(grid_supp+grid_form,1,'last');
for x = ref : inv_count
    if grid_form(x)==1
        x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1:11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+11,1) = [fi_PLL(x);P(x);Q(x);fi_d(x);fi_q(x);gamma_d(x);gamma_q(x);ild(x);ilq(x);vod(x);voq(x)];
    elseif grid_supp(x)==1
        x_status(11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+1:11*sum(grid_form(1:x-1))+10*sum(grid_supp(1:x-1))+10,1) = [P(x);Q(x);fi_d(x);fi_q(x);gamma_d(x);gamma_q(x);ild(x);ilq(x);vod(x);voq(x)];
    end
end
for x = 1 : Nnode
    if plac_load(x)==1
        x_status(11*NGF+10*NGS+2*sum(plac_load(1:x-1)) +1:11*NGF+10*NGS+2*sum(plac_load(1:x-1)) +2,1) = [i_LD(x);i_LQ(x)];
    end
end
if PCC == 1
    x_status(11*NGF+10*NGS+2*Nload+1:11*NGF+10*NGS+2*Nload+2,1) = [i_lineD0;i_lineQ0];
end
for x = 1 : Nline
    x_status(11*NGF+10*NGS+2*Nload+2*PCC+2*(x-1)+1:11*NGF+10*NGS+2*Nload+2*PCC+2*x,1) = [i_lineD(x);i_lineQ(x)];
end
SD.x_status = x_status;
end