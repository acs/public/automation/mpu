%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate the variance of different variables in the system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [VS] = calc_VS(DM,SD,unc_disturb_Eo,unc_disturb_G,unc_distur_PQ,GridData,Combination_devices,Accuracy,filter_on)

% we define the variance of the disturbance noise
VS = struct; %variance_status
var_disturbance_relative = [];
if DM.PCC == 1
    var_disturbance_relative = (unc_disturb_Eo^2)*[1,0;0,1];
end
if sum(DM.grid_supp_PV)>0
    var_disturbance_relative = blkdiag(var_disturbance_relative,(unc_disturb_G^2));
end
for x = 1 : DM.Nnode
    if DM.grid_supp_PQ(x)==1
        var_disturbance_relative = blkdiag(var_disturbance_relative,(unc_distur_PQ^2)*[1,0;0,1]);
    end
end
VS.var_disturbance_relative = var_disturbance_relative;
VS.unc_disturb_Eo = unc_disturb_Eo;
VS.unc_disturb_G = unc_disturb_G;
VS.unc_distur_PQ = unc_distur_PQ;

Gx = 0;
if (DM.PCC == 1) || sum(DM.grid_supp)>0
    if DM.PCC == 1
        VS.var_disturbance_absolute(1,1) = VS.var_disturbance_relative(1,1) * (DM.Eod^2);
        VS.var_disturbance_absolute(2,2) = VS.var_disturbance_relative(2,2) * (DM.Eoq^2);
    end
    if sum(DM.grid_supp_PV)>0
        Gx = 1;
        VS.var_disturbance_absolute(2*DM.PCC+Gx,2*DM.PCC+Gx) = VS.var_disturbance_relative(2*DM.PCC+Gx,2*DM.PCC+Gx)*DM.G^2;
    end
    if sum(DM.grid_supp_PQ)>0
        for x = 1 : DM.Nnode
            if DM.grid_supp_PQ(x)==1
                VS.var_disturbance_absolute(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1) = VS.var_disturbance_relative(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1)*DM.P_obj(x)^2;
                VS.var_disturbance_absolute(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2) = VS.var_disturbance_relative(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2)*DM.Q_obj(x)^2;
            end
        end
    end
else
    VS.var_disturbance_absolute = 0;
end



if isfield(DM,'B_Eo') == 1
    B_Eo = DM.B_Eo;
else
    B_Eo    = 0.99900; % presence of noise
end

if isfield(DM,'B_G') == 1
    B_G = DM.B_G;
else
    B_G    = 0.99926; % slower noise than slack bus voltage
end

if isfield(DM,'B_PQ_MV') == 1
    B_PQ_MV = DM.B_PQ_MV;
else
    B_PQ_MV    = 0.99973; % slower noise than PV 
end

if isfield(DM,'B_PQ_LV') == 1
    B_PQ_LV = DM.B_PQ_LV;
else
    B_PQ_LV    = 0.99988; % slower noise than MV
end


[VS] = calc_filter(VS,DM,B_Eo,B_G,B_PQ_LV);

VS.var_disturbance_absolute_white = VS.var_disturbance_absolute;
if filter_on == 1
    Ntot = 1000;
    for n = 0 : Ntot
        if n == 0
            W(n+1).W = VS.var_disturbance_absolute_white;
        else
            W(n+1).W = VS.F_matrix * W(n+1-1).W * VS.F_matrix + VS.var_disturbance_absolute_white;
        end
    end

    
    %%%%% test 22.01.2019
    diag_ratio =  diag(VS.var_disturbance_absolute_white)./diag(W(n+1).W);
    %VS.var_disturbance_absolute = W(end).W; %covariance of colored noise
    %VS.var_disturbance_relative_white = VS.var_disturbance_relative;
    VS.var_disturbance_absolute = VS.var_disturbance_absolute_white;
    VS.var_disturbance_absolute_white = diag_ratio(end)*W(n+1).W;
    %%%%%
    
    Gx = 0;
    if (DM.PCC == 1) || sum(DM.grid_supp)>0
        if DM.PCC == 1
            if VS.var_disturbance_absolute(1,1) == 0
                VS.var_disturbance_relative(1,1) = 0;
            else
            VS.var_disturbance_relative(1,1) = VS.var_disturbance_absolute(1,1)/(DM.Eod^2);
            VS.var_disturbance_relative_white(1,1) = VS.var_disturbance_absolute_white(1,1)/(DM.Eod^2);
            end
            if VS.var_disturbance_absolute(2,2) == 0
                VS.var_disturbance_relative(2,2) = 0;
                VS.var_disturbance_relative_white(2,2) = 0;
            else
            VS.var_disturbance_relative(2,2) = VS.var_disturbance_absolute(2,2)/(DM.Eoq^2);
            VS.var_disturbance_relative_white(2,2) = VS.var_disturbance_absolute_white(2,2)/(DM.Eoq^2);
            end
        end
        if sum(DM.grid_supp_PV)>0
            Gx = 1;
            VS.var_disturbance_relative(2*DM.PCC+Gx,2*DM.PCC+Gx) = VS.var_disturbance_absolute(2*DM.PCC+Gx,2*DM.PCC+Gx)/DM.G^2;
            VS.var_disturbance_relative_white(2*DM.PCC+Gx,2*DM.PCC+Gx) = VS.var_disturbance_absolute_white(2*DM.PCC+Gx,2*DM.PCC+Gx)/DM.G^2;
        end
        if sum(DM.grid_supp_PQ)>0
            for x = 1 : DM.Nnode
                if DM.grid_supp_PQ(x)==1
                    VS.var_disturbance_relative(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1) = ...
                        VS.var_disturbance_absolute(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1)/DM.P_obj(x)^2;
                    VS.var_disturbance_relative(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2) = ...
                        VS.var_disturbance_absolute(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2)/DM.Q_obj(x)^2;
                
                
                     VS.var_disturbance_relative_white(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1) = ...
                        VS.var_disturbance_absolute_white(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +1)/DM.P_obj(x)^2;
                    VS.var_disturbance_relative_white(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2) = ...
                        VS.var_disturbance_absolute_white(2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2 ,2*DM.PCC + Gx + 2*sum(DM.grid_supp_PQ(1:x-1)) +2)/DM.Q_obj(x)^2;
                
                end
            end
        end
    else
        VS.var_disturbance_relative = 0;
        VS.var_disturbance_relative_white = 0;
    end
end




% we define the measurement noise variance
ref = 11*DM.NGF+10*DM.NGS+2*DM.Nload ;
[~,~,~,CLINE_tot] = calc_A_B_matrices(DM,SD);
[PowerData] = generate_DM_DSSE_PowerData(DM,SD,CLINE_tot,GridData,SD.x_status);
[W,GridData,R1] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
[C_IRI] = Jacobian_m_IRIDSSE(GridData);
G1 = C_IRI'*W*C_IRI;
G1(:,11*DM.NGF+10*DM.NGS+2*DM.Nload+1)=[]; 
G1(11*DM.NGF+10*DM.NGS+2*DM.Nload+1,:)=[];
var_err_rectangular_mat_IRI = inv(G1);

VS.Rpu = R1;

for x = 1 : 11*DM.NGF + 10*DM.NGS + 2*DM.Nload + 2*GridData.Lines_num
    var_err_rectangular_mat_IRI(:,x) = var_err_rectangular_mat_IRI(:,x) .* ([GridData.base_status; GridData.base_current*ones(2*GridData.Lines_num,1)] );
end
for x = 1 : 11*DM.NGF + 10*DM.NGS + 2*DM.Nload + 2*GridData.Lines_num
    var_err_rectangular_mat_IRI(x,:) = var_err_rectangular_mat_IRI(x,:) .* ([GridData.base_status; GridData.base_current*ones(2*GridData.Lines_num,1)] )';
end
for x = 1 : 11*DM.NGF + 10*DM.NGS + 2*DM.Nload + 2*GridData.Lines_num
    G1(:,x) = G1(:,x) .* ([GridData.base_status; GridData.base_current*ones(2*GridData.Lines_num,1)].^-1);
end
for x = 1 : 11*DM.NGF + 10*DM.NGS + 2*DM.Nload + 2*GridData.Lines_num
    G1(x,:) = G1(x,:) .* ([GridData.base_status; GridData.base_current*ones(2*GridData.Lines_num,1)].^-1)';
end

for x = 1 : GridData.Lines_num
    var_err_rectangular_mat_IRI(:,[ref+2*x-1,ref+2*x]) = var_err_rectangular_mat_IRI(:,[ref+2*x,ref+2*x-1]);
    var_err_rectangular_mat_IRI([ref+2*x-1,ref+2*x],:) = var_err_rectangular_mat_IRI([ref+2*x,ref+2*x-1],:);
end

VS.Gain_matrix = G1;
VS.var_status_absolute = var_err_rectangular_mat_IRI;
[C] = Jacobian_m_IRIDSSE_nopu(GridData);

C(:,11*DM.NGF+10*DM.NGS+2*DM.Nload+1)=[]; 
C(11*DM.NGF+10*DM.NGS+2*DM.Nload+1,:)=[];

VS.R = C * VS.var_status_absolute * transpose(C);
VS.R = blkdiag(VS.R(1:11*DM.NGF+10*DM.NGS+2*DM.Nload,1:11*DM.NGF+10*DM.NGS+2*DM.Nload),...
    (Accuracy.Accuracy_Vmagn*PowerData.Vmagn(1)*GridData.base_voltage)^2,...
    VS.R(11*DM.NGF+10*DM.NGS+2*DM.Nload+1:end,11*DM.NGF+10*DM.NGS+2*DM.Nload+1:end));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%here we define the variance of the initial status
[Atot,B_input,B_disturbance,~] = calc_A_B_matrices(DM,SD);
Tss = 100; Nts = 100; %1 seconds of white noise disturbance
P_delta_x = zeros(size(VS.var_status_absolute));
[ TmatrixTs,~,Dist_matrixTs] = calc_TDF(Tss,Atot,B_input,B_disturbance);
Tmatrix_eq = eye(size(TmatrixTs)); %Tmatrix_eq = zeros(size(TmatrixTs));
for t = 1 : Nts
    Tmatrix_eq = Tmatrix_eq*TmatrixTs;
    P_delta_x = TmatrixTs * P_delta_x * transpose(TmatrixTs) + Dist_matrixTs * VS.var_disturbance_absolute * transpose(Dist_matrixTs)  ;
end
VS.P_delta_x = P_delta_x; %this is the initial state uncertainty

end

