%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to obtain the system voltage, current and injection status from
% the x matrix
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status ] = calc_xestimate(DM,GridData,PowerData,Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,Tmatrix,Forz_matrix,u)
% propagates estimation over delta T
x_status_estimated(1:11*DM.NGF+10*DM.NGS+2*DM.Nload,1) = inj_status .* GridData.base_status;

for b = 1 : GridData.Lines_num
    x_status_estimated(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*(b-1) + 1  ,1) = GridData.base_current * Imagn_status(b,1) * cos(Iph_status(b,1) + pi/2);
    x_status_estimated(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*(b-1) + 2  ,1) = GridData.base_current * Imagn_status(b,1) * sin(Iph_status(b,1) + pi/2);
end
x_status_estimated(:,2) = Tmatrix * x_status_estimated(:,1);
x_status_estimated(:,3) = Forz_matrix * u;
x_status_estimated(:,5) = x_status_estimated(:,2) + x_status_estimated(:,3);
Volts(1,1) = Vmagn_status(1,1).*exp( + sqrt(-1)*(Vph_status(1,1))); %assuming that the slack bus estimation could not change over time
for b = 1 : GridData.Lines_num
    i_lineD = x_status_estimated(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*(b-1) + 1  ,5);
    i_lineQ = x_status_estimated(11*DM.NGF+10*DM.NGS+2*DM.Nload + 2*(b-1) + 2  ,5);
    i_lineDQ = (i_lineD + 1i*i_lineQ) / GridData.base_current ;
    i_lineDQ = i_lineDQ * exp( + 1i*(-pi/2));
    Imagn_status(b,1) = abs  (i_lineDQ) ;
    Iph_status(b,1)   = phase(i_lineDQ);
    Volts(GridData.topology(3,b),1) = Volts(GridData.topology(2,b),1) - (GridData.R1(b) + 1i*GridData.X1(b))*i_lineDQ;
end
Vmagn_status = abs(Volts);
Vph_status = wrapToPi(phase(Volts));
inj_status = x_status_estimated(1:11*DM.NGF+10*DM.NGS+2*DM.Nload,5) ./ GridData.base_status;
end

