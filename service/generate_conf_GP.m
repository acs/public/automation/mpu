%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate the scenarios to test the uncertainty models for any
% generic system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ST,N_conf] = generate_conf_GP(type_test)

%0 SES
%1 SED with white noise
%2 DED with white noise
%3 SED with colored noise
%4 DED with colored noise
%5 SED with homogeneous delay
%6 DED with homogeneous delay
%7 SED with heterogeneous delay
%8 DED with heterogeneous delay
%7 SED with heterogeneous delay
%8 DED with heterogeneous delay
%9  SRES
%10 SRED
%11 DRED

N_conf = 0;

if type_test == 0
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SES_GP');
    ST(N_conf).type_unc = 0;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 0;
    
elseif type_test == 1
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SED_GP');
    ST(N_conf).type_unc = 2;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 0;
    
elseif type_test == 2
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('DED_GP');
    ST(N_conf).type_unc = 3;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 0;
    
elseif type_test == 3
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SED_col_GP');
    ST(N_conf).type_unc = 2;
    ST(N_conf).filter_on = 1;
    ST(N_conf).Ts = 1e0;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 0;
    
elseif type_test == 4
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('DED_col_GP');
    ST(N_conf).type_unc = 3;
    ST(N_conf).filter_on = 1;
    ST(N_conf).Ts = 1e0;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 0;
    
    
elseif type_test == 5
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SED_delay_GP');
    ST(N_conf).type_unc = 8;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0.5 0.5];
    ST(N_conf).Nw = 0;
    
elseif type_test == 6
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('DED_delay_GP');
    ST(N_conf).type_unc = 9;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0.5 0.5];
    ST(N_conf).Nw = 0;
    
elseif type_test == 7
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SED_uns_GP');
    ST(N_conf).type_unc = 8;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0.5 0.2];
    ST(N_conf).Nw = 0;
    
elseif type_test == 8
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('DED_uns_GP');
    ST(N_conf).type_unc = 9;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0.5 0.2];
    ST(N_conf).Nw = 0;
    
    
elseif type_test == 9
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SRES_GP');
    ST(N_conf).type_unc = 5;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 10;
    
elseif type_test == 10
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('SRED_GP');
    ST(N_conf).type_unc = 6;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 10;
    
elseif type_test == 11
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('DRED_GP');
    ST(N_conf).type_unc = 7;
    ST(N_conf).filter_on = 0;
    ST(N_conf).Ts = 1e6;
    ST(N_conf).time_delta_individual_meas = [0 0];
    ST(N_conf).Nw = 10;
    
end


end