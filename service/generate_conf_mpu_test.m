%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate the scenarios to test the uncertainty models
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ST,N_conf] = generate_conf_mpu_test(BaseConf,type_unc)


for N_conf = 1 : length(type_unc)
    
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = [1,[2:ceil(ST(N_conf).Nnode/2)  ],2                                 ,[ceil(ST(N_conf).Nnode/2)+2:ST(N_conf).Nnode-1];...
        2,[3:ceil(ST(N_conf).Nnode/2)+1],ceil(ST(N_conf).Nnode/2)+2,[ceil(ST(N_conf).Nnode/2)+3:ST(N_conf).Nnode] ];
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = repmat([1,0],1,ST(N_conf).Nnode/2);
    ST(N_conf).plac_load =        repmat([0,1],1,ST(N_conf).Nnode/2);
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = zeros(1,ST(N_conf).Nnode);
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = BaseConf.V;
    ST(N_conf).I = BaseConf.I;
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = BaseConf.delay;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).Ts = BaseConf.Ts;
    ST(N_conf).filter = BaseConf.filter;
    
    if type_unc(N_conf) == 0
        ST(N_conf).Script = strcat('SES');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 0.1
        ST(N_conf).Script = strcat('SES_v');
        ST(N_conf).type_unc = 0;
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 2
        ST(N_conf).Script = strcat('SED');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 2.1
        ST(N_conf).filter = 0;
        ST(N_conf).Script = strcat('colorSED');
        ST(N_conf).type_unc = 2;
        ST(N_conf).Ts = 1;
        ST(N_conf).filter = 1;
        ST(N_conf).colored = 0.99;
    elseif type_unc(N_conf) == 3
        ST(N_conf).Script = strcat('DED');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 3.1
        ST(N_conf).Script = strcat('colorDED');
        ST(N_conf).type_unc = 3;
        ST(N_conf).Ts = 1;
        ST(N_conf).filter = 1;
        ST(N_conf).colored = 0.99;
    elseif type_unc(N_conf) == 6
        ST(N_conf).Script = strcat('SEDrec');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 7
        ST(N_conf).Script = strcat('DEDrec');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 8
        ST(N_conf).Script = strcat('delaySED');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).Pseudo =  [3:BaseConf.Nnode+1;0.5*ones(BaseConf.Nnode-1)];
        ST(N_conf).V= [1;0.5];
        ST(N_conf).I=[1;0.5];
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 8.1
        ST(N_conf).Script = strcat('unsSED');
        ST(N_conf).type_unc = 8;
        ST(N_conf).Pseudo =  [3:BaseConf.Nnode+1;0.5*ones(BaseConf.Nnode-1)];
        ST(N_conf).V=[1;0.4];
        ST(N_conf).I=[1;0.5];
        ST(N_conf).filter = 0;
    elseif type_unc(N_conf) == 9
        ST(N_conf).Script = strcat('delayDED');
        ST(N_conf).type_unc = type_unc(N_conf);
        ST(N_conf).Pseudo =  [3:BaseConf.Nnode+1;0.5*zeros(1,BaseConf.Nnode-1)];
        ST(N_conf).V=[1;0.5];
        ST(N_conf).I=[1;0.5];
        ST(N_conf).filter = 0;
%         ST(N_conf).Pseudo =  [3:BaseConf.Nnode+1;0*ones(1,BaseConf.Nnode-1)];
%         ST(N_conf).Script = strcat('delayDED');
%         ST(N_conf).type_unc = type_unc(N_conf);
%         ST(N_conf).filter = 0;
        
    elseif type_unc(N_conf) == 9.1
        ST(N_conf).Script = strcat('unsDED');
        ST(N_conf).Pseudo =  [3:BaseConf.Nnode+1;0.5*ones(BaseConf.Nnode-1)];
        ST(N_conf).type_unc = 9;
        ST(N_conf).V=[1;0.4];
        ST(N_conf).I=[1;0.5];
        ST(N_conf).filter = 0;
    end
    
    
    
end

end