%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script with base case parameters of LV grid
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
text_in = 'Pilot_RWTH_LV';
unc_disturb_Eo = 0.005; %from observation 2019
unc_disturb_G  = 0.16%0.16;%from observation 2019
unc_distur_PQ  = 0.06%0.06;%from observation 2019 - 1 day
dev_input      = 0.0;
dev_status     = 0.0;

BaseConf.Nnode = 6;
BaseConf.Nline = 1;
BaseConf.PQRL = 0.5;
BaseConf.Sn = 30000;

BaseConf.topology = [1,2,3,3,3;...
                     2,3,4,5,6];
% BaseConf.grid_supp_PQ = [0, 1,0, 0, 1, 1];
% BaseConf.plac_load    = [1, 0,1, 0, 0, 0];
% BaseConf.grid_supp_PV = [0, 0,0, 1, 0, 0];

BaseConf.grid_supp_PQ = [0, 1,0, 1, 1, 1];
BaseConf.plac_load    = [1, 0,1, 0, 0, 0];
BaseConf.grid_supp_PV = [0, 0,0, 0, 0, 0];


BaseConf.Sn =       1e3*[1,20,1,-2,20,5];



BaseConf.Pmax = 1e3*2;

BaseConf.f = 50;
BaseConf.fsw = 10000;
BaseConf.PF = [0.95,0.95,0.95,0.999,0.95,0.95]; %Power Factor, applied to RL loads and PV inverters
BaseConf.Vn = 230;

%%% to calculate the Rcc and Lcc
Vn_MV = 230;
Sn_MV = 100*10^6;
Rcc1 = sqrt(0.5)*(Vn_MV^2)/(Sn_MV);
Lcc1 = Rcc1/(2*pi*50);
BaseConf.Rcc = Rcc1 + 0.0031; %trafo RWTH
BaseConf.Lcc = Lcc1 + 0.0149/(2*pi*50); %trafo RWTH

%%%
%%% to calculate line impedance
rline_km =       [0.0754;0.0991;0.524;0.387;0.524]; %Line resistance per km
Lline_km =       [  0.1;   0.1;   0.1;  0.1;  0.1]./(2*pi*50);%Line inductance per km
length_km = 1e-3*[   40;  110;    60;   45;   60];

%%%
BaseConf.rline = rline_km .* length_km ;
BaseConf.Lline = Lline_km .* length_km ;
%%%
% G = graph(BaseConf.topology(:,1),BaseConf.topology(:,2));
% plot(G) 
BaseConf.delay = 0.1;

BaseConf.PQflow = [];
BaseConf.Pseudo = [2:BaseConf.Nnode+1;zeros(1,BaseConf.Nnode)];
BaseConf.PQ = [];
BaseConf.V = [1,6]; BaseConf.V = [BaseConf.V;BaseConf.delay*ones(size(BaseConf.V))];
BaseConf.I = [1,5]; BaseConf.I = [BaseConf.I;BaseConf.delay*ones(size(BaseConf.I))];
BaseConf.unc_dev =  0.01/3;
BaseConf.unc_pseudo = 0.5/3;


BaseConf.auto_V = 0.995; %autovariance of disturbance
BaseConf.auto_P = 0.987; %autovariance of disturbance
BaseConf.auto_G = 0.996; %autovariance of disturbance
BaseConf.Ts = 1e0;
BaseConf.filter = 1;
BaseConf.colored = 0.99;
BaseConf.dist = 1; %multiplier of standard deviation - leave 1