%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time independent uncertainties impact on the base
% case
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,test_type)

uncTD_1 = load(strcat(pwd,'\Results\',text_in,'_TD_test_type_0','\Result_test'));
uncTD_2 = load(strcat(pwd,'\Results\',text_in,'_TD_test_type_0','\Result_test'));


m = 1;
x_axis_unique= [1:uncTD_1.ST(m).Nnode];

legend_text = {'$\Delta T$ = 60 s','$\Delta T$ = 1 s'};

if uncTD_1.ST(m).Nnode > 20
    xangle = -90;
else
    xangle = 0;
end


index_a1 = find(time_test<=60);
index_b1 = find(time_test>=60);
index_1 = [max(index_a1),min(index_b1)];

index_a = find(time_test<=Ts);
index_b = find(time_test>=Ts);
index_2 = [max(index_a),min(index_b)];

if index_1(1) == index_1(2)
  
  unc_TD_out_I_A (1,:)=   uncTD_1.unc(m).unc_I_A(:,index_1(1));
  unc_TD_out_I_P(1,:) =   uncTD_1.unc(m).unc_I_P(:,index_1(1));  
  unc_TD_out_V_A(1,:) =   uncTD_1.unc(m).unc_V_A(:,index_1(1));
  unc_TD_out_V_P(1,:) =   uncTD_1.unc(m).unc_V_P(:,index_1(1));  
  
else
      % TD 
  unc_TD_out_I_Aa =   uncTD_1.unc(m).unc_I_A(:,index_1(1));
  unc_TD_out_I_Pa =   uncTD_1.unc(m).unc_I_P(:,index_1(1));  
  unc_TD_out_V_Aa =   uncTD_1.unc(m).unc_V_A(:,index_1(1));
  unc_TD_out_V_Pa =   uncTD_1.unc(m).unc_V_P(:,index_1(1));  
  
  unc_TD_out_I_Ab =   uncTD_1.unc(m).unc_I_A(:,index_1(2));
  unc_TD_out_I_Pb =   uncTD_1.unc(m).unc_I_P(:,index_1(2));  
  unc_TD_out_V_Ab =   uncTD_1.unc(m).unc_V_A(:,index_1(2));
  unc_TD_out_V_Pb =   uncTD_1.unc(m).unc_V_P(:,index_1(2));  
  
  unc_TD_out_I_A(1,:) = unc_TD_out_I_Aa + ((unc_TD_out_I_Aa- unc_TD_out_I_Ab)/(time_test(index_1(1)) - time_test(index_1(2))) ) * ( Ts - time_test(index_1(1)));
  unc_TD_out_I_P(1,:) = unc_TD_out_I_Pa + ((unc_TD_out_I_Pa- unc_TD_out_I_Pb)/(time_test(index_1(1)) - time_test(index_1(2))) ) * ( Ts - time_test(index_1(1)));
  unc_TD_out_V_A(1,:) = unc_TD_out_V_Aa + ((unc_TD_out_V_Aa- unc_TD_out_V_Ab)/(time_test(index_1(1)) - time_test(index_1(2))) ) * ( Ts - time_test(index_1(1)));
  unc_TD_out_V_P(1,:) = unc_TD_out_V_Pa + ((unc_TD_out_V_Pa- unc_TD_out_V_Pb)/(time_test(index_1(1)) - time_test(index_1(2))) ) * ( Ts - time_test(index_1(1)));

end


if index_2(1) == index_2(2)
  
    unc_TD_out_I_A (2,:)=   uncTD_2.unc(m).unc_I_A(:,index_2(1));
  unc_TD_out_I_P(2,:) =   uncTD_2.unc(m).unc_I_P(:,index_2(1));  
  unc_TD_out_V_A(2,:) =   uncTD_2.unc(m).unc_V_A(:,index_2(1));
  unc_TD_out_V_P(2,:) =   uncTD_2.unc(m).unc_V_P(:,index_2(1));  


else
    

  
    unc_TD_out_I_Aa =   uncTD_2.unc(m).unc_I_A(:,index_2(1));
  unc_TD_out_I_Pa =   uncTD_2.unc(m).unc_I_P(:,index_2(1));  
  unc_TD_out_V_Aa =   uncTD_2.unc(m).unc_V_A(:,index_2(1));
  unc_TD_out_V_Pa =   uncTD_2.unc(m).unc_V_P(:,index_2(1));  
  
  unc_TD_out_I_Ab =   uncTD_2.unc(m).unc_I_A(:,index_2(2));
  unc_TD_out_I_Pb =   uncTD_2.unc(m).unc_I_P(:,index_2(2));  
  unc_TD_out_V_Ab =   uncTD_2.unc(m).unc_V_A(:,index_2(2));
  unc_TD_out_V_Pb =   uncTD_2.unc(m).unc_V_P(:,index_2(2));  
  
  
   unc_TD_out_I_A(2,:) = unc_TD_out_I_Aa + ((unc_TD_out_I_Aa- unc_TD_out_I_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TD_out_I_P(2,:) = unc_TD_out_I_Pa + ((unc_TD_out_I_Pa- unc_TD_out_I_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TD_out_V_A(2,:) = unc_TD_out_V_Aa + ((unc_TD_out_V_Aa- unc_TD_out_V_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TD_out_V_P(2,:) = unc_TD_out_V_Pa + ((unc_TD_out_V_Pa- unc_TD_out_V_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  

  
end


% current
% amplitude and phase

figure
plot ([0:uncTD_1.ST(m).Nnode-1],unc_TD_out_I_A(1,:),'b')
hold on
plot ([0:uncTD_1.ST(m).Nnode-1],unc_TD_out_I_A(2,:),'b','LineWidth',1.5)
%plot ([0:uncTD_1.ST(m).Nnode-1],unc_TD_out_I_A(3,:),'b','LineWidth',4)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([0 uncTD_1.ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_TDcomp_current_A','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)





figure
plot ([0:uncTD_1.ST(m).Nnode-1],unc_TD_out_I_P(1,:))
hold on
plot ([0:uncTD_1.ST(m).Nnode-1],unc_TD_out_I_P(2,:),'b','LineWidth',1.5)
%plot ([0:uncTD_1.ST(m).Nnode-1],unc_TD_out_I_P(3,:),'b','LineWidth',3)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([0 uncTD_1.ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_TDcomp_current_P','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)




% voltage
% amplitude and phase

figure
plot ([1:uncTD_1.ST(m).Nnode],unc_TD_out_V_A(1,2:end))
hold on
plot ([1:uncTD_1.ST(m).Nnode],unc_TD_out_V_A(2,2:end),'b','LineWidth',1.5)
%plot ([1:uncTD_1.ST(m).Nnode],unc_TD_out_V_A(3,2:end),'b','LineWidth',3)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([1 uncTD_1.ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_TDcomp_voltage_A','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)



figure
plot ([1:uncTD_1.ST(m).Nnode],unc_TD_out_V_P(1,2:end))
hold on
plot ([1:uncTD_1.ST(m).Nnode],unc_TD_out_V_P(2,2:end),'b','LineWidth',1.5)
%plot ([1:uncTD_1.ST(m).Nnode],unc_TD_out_V_P(3,2:end),'b','LineWidth',3)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([1 uncTD_1.ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_TDcomp_voltage_P','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)


end