%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script with base case parameters of LV grid
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
text_in = 'Pilot_A2A_MV';
unc_disturb_Eo = 0.008; %from observation A2A
unc_disturb_G  = 0.67; %from observation RWTH
unc_distur_PQ  = 0.06; %from observation A2A
dev_input      = 0.0;
dev_status     = 0.0;

BaseConf.Nnode = 44;
BaseConf.Nline = 3;
BaseConf.PQRL = 0.5;



BaseConf.grid_supp_PQ = ones(1,BaseConf.Nnode);
BaseConf.plac_load    = zeros(1,BaseConf.Nnode);
BaseConf.grid_supp_PV = zeros(1,BaseConf.Nnode);


load_fact = 0.3;

                        %nodes=[1,2,3,4,  5,  6, 7, 8, *9,*10, 11,12, *13,*14, 15,16, 17, 18, 19, 20, 21, 22, 23, 24,**25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,43,  44];
BaseConf.Sn = load_fact*1e3 *  [5,5,5,5,630,100,50,50,160,717,250,150, 90,180,400, 5,100,250,250,250,630,400,400,400,-140,250,400,250,400,250,630,250,250,150,250,250,250,400,400,250,250,250,250,400];


%trafos
%630 
%400
%250
%150
%100 
%50 
BaseConf.Pmax = 1e3*2;

BaseConf.f = 50;
BaseConf.fsw = 10000;
BaseConf.PF = 0.95; %Power Factor, applied to RL loads and PV inverters
BaseConf.Vn = 8660;

%%% to calculate the Rcc and Lcc
Vn_HV = 8660;
Sn_HV = 5000*10^6;
Rcc1 = sqrt(1/11)*(Vn_HV^2)/(Sn_HV);
Lcc1 = 10*Rcc1/(2*pi*50);
BaseConf.Rcc = Rcc1 + 0.016;
BaseConf.Lcc = Lcc1 + 1.92/(2*pi*50);
%%%
%%% original impedances (meshed)
% BaseConf.topology = [1,2,4,3,3,4,5,8,9 ,10,11,12,12,14,15,16,15,18,19,20,21,22,23,24,  1,26,27,28,29,30,31,32,33,25,  1,35,36,37,38,39,40,41,42,43,44;...
%                      2,3,5,6,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25, 26,27,28,29,30,31,32,33,25,34, 35,36,37,38,39,40,41,42,43,44,34];
% rline_km =       [0.0492;1.918914;0.987714;0.23016;0.20060568;0.07161672;0.541036;0.193988;0.214948;0.35018;0.354522;0.789866;0.196402;1.176936;0.19179;6.23561;1.780614;1.21575;1.41614;0.81844;0.697512;0.385964;0.524072;1.148454;1.026502;0.54743;0.092888;0.0492;0.823724;0.166284;0.628682;0.66941;0.149076;1.01217;0.45207;1.053858;0.936826;0.979506;3.363492;1.712652;0.31796;0.55151;0.249286;0.249286;0.74989];;%Line;resistance;per;km
% Lline_km =       [0.02706;0.169896;0.2154832;0.024696;0.67888854;0.10128384;0.1350048;0.0694224;0.0746064;0.10299;0.0698214;0.028226;0.02418;0.142038;0.038892;0.241176;0.048246;0.19188;0.183612;0.098592;0.085124;0.04797;0.1230302;0.2011122;0.32188;0.0836;0.022682;0.02706;0.110396;0.030004;0.076296;0.070992;0.0819918;0.126156;0.0648846;0.1455366;0.125554;0.131274;0.159588;0.211864;0.071018;0.136404;0.0732708;0.0732708;0.148668]./(2*pi*50);%Line inductance per km

%%% impedance (radial)
topology(:,1) = [1;2;4;3;3;4;5;8;9 ;10;11;12;12;14;15;16;15;18;19;20;21;22;23;  1;25;26;27;28;29;30;31;32;33;  1;35;36;37;38;39;40;41;42;43];
topology(:,2) = [2;3;5;6;4;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24; 25;26;27;28;29;30;31;32;33;34; 35;36;37;38;39;40;41;42;43;44];
topology(:,3) = [0.0164 0.098097 0.146898 0.01974 0.05220072 0.021798 0.097662 0.045706 0.049466 0.07115 0.039308 0.022668 0.017071 0.106413 0.028435 0.195725 0.030107 0.114225 0.10976 0.07426 0.061706 0.032897 0.078657 0.197916 0.04544 0.013064 0.0164 0.058672 0.019212 0.055001 0.052945 0.049692 0.094225 0.034908 0.077772 0.066728 0.069768 0.091646 0.127671 0.04777 0.098935 0.050627 0.050627 ];   
topology(:,4) =  [0.0108 0.066783 0.072125 0.009408 0.12046617 0.01623342 0.042684 0.02514 0.026772 0.034932 0.027705 0.010823 0.009415 0.055104 0.012836 0.092483 0.018513 0.07074 0.072146 0.038236 0.033092 0.01871 0.048658 0.123329 0.033085 0.009016 0.0108 0.043618 0.010502 0.029603 0.023241 0.032724 0.048987 0.025659 0.057525 0.049607 0.051867 0.062214 0.078203 0.02419 0.042942 0.024846 0.024846 ]./(2*pi*50);     
topology = sortrows(topology);

BaseConf.topology = topology(:,1:2)';
BaseConf.rline = topology(:,3);
BaseConf.Lline = topology(:,4);
%%%
% G = graph(topology(:,1)',topology(:,2)');
% plot(G)
BaseConf.delay = 0.1;

BaseConf.PQflow = [1,2,3,7,9,26,27,28,29,30,31,32,33,34,35,36,38,39,40]; BaseConf.PQflow = [BaseConf.PQflow;BaseConf.delay*ones(size(BaseConf.PQflow))];
BaseConf.Pseudo = [2:BaseConf.Nnode+1;zeros(1,BaseConf.Nnode)];
BaseConf.PQ = [];
BaseConf.V = [1]; BaseConf.V = [BaseConf.V;BaseConf.delay*ones(size(BaseConf.V))];
BaseConf.I = [];
BaseConf.unc_dev =  0.01/3;
BaseConf.unc_pseudo = 0.5/3;


BaseConf.auto_V = 0.87; %autovariance of disturbance
BaseConf.auto_P = 0.9853; %autovariance of disturbance
BaseConf.auto_G = 0.9986; %autovariance of disturbance
BaseConf.Ts = 1e0;
BaseConf.filter = 1;
BaseConf.colored = 0.99;
BaseConf.dist = 1; %multiplier of standard deviation - leave 1