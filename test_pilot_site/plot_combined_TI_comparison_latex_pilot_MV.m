%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time independent uncertainties impact on the base
% case
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,test_type)

uncTI_1 = load(strcat(pwd,'\Results\',text_in,'_TI_test_type_2','\Result_test'));
uncTI_2 = load(strcat(pwd,'\Results\',text_in,'_TI_test_type_3','\Result_test'));

m = 1;
x_axis_unique= [1:uncTI_1.ST(m).Nnode];

legend_text = {'base case','base case + 3 voltage meas'};

if uncTI_1.ST(m).Nnode > 20
    xangle = -90;
else
    xangle = 0;
end


% current
% amplitude and phase

figure
plot ([0:uncTI_1.ST(m).Nnode-1],uncTI_1.unc.unc_I_A(:,1),'r')
hold on
plot ([0:uncTI_1.ST(m).Nnode-1],uncTI_2.unc.unc_I_A(:,1),'r','LineWidth',1.5)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([0 uncTI_1.ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\fig_uncertainty_TIcomp_current_A','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)





figure
plot ([0:uncTI_1.ST(m).Nnode-1],uncTI_1.unc.unc_I_P(:,1),'r')
hold on
plot ([0:uncTI_1.ST(m).Nnode-1],uncTI_2.unc.unc_I_P(:,1),'r','LineWidth',1.5)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([0 uncTI_1.ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\fig_uncertainty_TIcomp_current_P','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)




% voltage
% amplitude and phase

figure
plot ([1:uncTI_1.ST(m).Nnode],uncTI_1.unc.unc_V_A(2:end,1),'r')
hold on
plot ([1:uncTI_1.ST(m).Nnode],uncTI_2.unc.unc_V_A(2:end,1),'r','LineWidth',1.5)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([1 uncTI_1.ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\fig_uncertainty_TIcomp_voltage_A','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)



figure
plot ([1:uncTI_1.ST(m).Nnode],uncTI_1.unc.unc_V_P(2:end,1),'r')
hold on
plot ([1:uncTI_1.ST(m).Nnode],uncTI_2.unc.unc_V_P(2:end,1),'r','LineWidth',1.5)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([1 uncTI_1.ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\fig_uncertainty_TIcomp_voltage_P','_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');
savefig(save_case)


end