%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate scenarios to be tested for impact analysis
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ST,N_conf] = generate_conf_pilot(BaseConf,type_test)

% 0 base case
% 1 A2A with less delay 
% 2 A2A with less delay and 1 voltage devices in 1 feeder

%10 RWTH with 0 delay

%11 number current flow devices
%12 number P inj devices
%13 number P flow devices
%14 accuracy meas devices
%15 accuracy pseudo meas
%16 delay


N_conf = 0;

if type_test == 0 %base_case
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('base case');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = BaseConf.topology;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = BaseConf.grid_supp_PQ;
    ST(N_conf).plac_load =    BaseConf.plac_load;
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = BaseConf.grid_supp_PV;
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = BaseConf.V;
    ST(N_conf).I = BaseConf.I;
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = BaseConf.delay;
    ST(N_conf).Ts = BaseConf.Ts;
    ST(N_conf).filter = BaseConf.filter;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).dist = 1;
    ST(N_conf).dt = 60;

     elseif type_test == 1 
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('faster rate');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = BaseConf.topology;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = BaseConf.grid_supp_PQ;
    ST(N_conf).plac_load =    BaseConf.plac_load;
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = BaseConf.grid_supp_PV;
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = BaseConf.V;
    ST(N_conf).I = BaseConf.I;
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = 0.1;
    ST(N_conf).Ts = 1;
    ST(N_conf).filter = BaseConf.filter;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).dist = 1;
    ST(N_conf).dt = 1;
    
    elseif type_test == 2 % A2A with 3 current PMUs
    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('A2A with 3 PMUs current');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = BaseConf.topology;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = BaseConf.grid_supp_PQ;
    ST(N_conf).plac_load =    BaseConf.plac_load;
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = BaseConf.grid_supp_PV;
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = BaseConf.V;
    ST(N_conf).I = [12,17,21]; ST(N_conf).I = [ST(N_conf).I;BaseConf.delay*ones(size(ST(N_conf).I))];

    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = 0.1;
    ST(N_conf).Ts = BaseConf.Ts;
    ST(N_conf).filter = BaseConf.filter;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).dist = 1;
    
elseif type_test == 3 % A2A with 3 voltage PMUs

     N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('A2A with 3 PMUs voltage');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = BaseConf.topology;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = BaseConf.grid_supp_PQ;
    ST(N_conf).plac_load =    BaseConf.plac_load;
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = BaseConf.grid_supp_PV;
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = [1,10,15,19]; ST(N_conf).V = [ST(N_conf).V;BaseConf.delay*ones(size(ST(N_conf).V))];
    ST(N_conf).I = [12,17,21]; ST(N_conf).I = [ST(N_conf).I;BaseConf.delay*ones(size(ST(N_conf).I))];
    
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = 0.1;
    ST(N_conf).Ts = BaseConf.Ts;
    ST(N_conf).filter = BaseConf.filter;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).dist = 1;
    
    
elseif type_test == 10 %RWTH with 1 more PMU

    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('LV + 1 PMU');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = BaseConf.topology;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = BaseConf.grid_supp_PQ;
    ST(N_conf).plac_load =    BaseConf.plac_load;
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = BaseConf.grid_supp_PV;
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = [1,3,6]; ST(N_conf).V = [ST(N_conf).V;BaseConf.delay*ones(size(ST(N_conf).V))];
    ST(N_conf).I = [1,3,5]; ST(N_conf).I = [ST(N_conf).I;BaseConf.delay*ones(size(ST(N_conf).I))];
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = BaseConf.delay;
    ST(N_conf).Ts = BaseConf.Ts;
    ST(N_conf).filter = BaseConf.filter;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).dist = 1;
    ST(N_conf).dt = 1;
 
    elseif type_test == 11 %RWTH with 1 more PMU + faster rate

    N_conf = N_conf + 1;
    ST(N_conf).Script = strcat('LV + 1 PMU');
    ST(N_conf).Nnode = BaseConf.Nnode;
    ST(N_conf).PQRL = BaseConf.PQRL;
    ST(N_conf).Sn = BaseConf.Sn;
    ST(N_conf).Vn = BaseConf.Vn;
    ST(N_conf).PF = BaseConf.PF;
    ST(N_conf).Nline = BaseConf.Nline;
    ST(N_conf).topology = BaseConf.topology;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).fsw = BaseConf.fsw;
    ST(N_conf).Rcc = BaseConf.Rcc;
    ST(N_conf).Lcc = BaseConf.Lcc;
    ST(N_conf).rline = BaseConf.rline;
    ST(N_conf).Lline = BaseConf.Lline;
    ST(N_conf).f = BaseConf.f;
    ST(N_conf).grid_supp_PQ = BaseConf.grid_supp_PQ;
    ST(N_conf).plac_load =    BaseConf.plac_load;
    ST(N_conf).grid_form =    zeros(1,ST(N_conf).Nnode);
    ST(N_conf).grid_supp_PV = BaseConf.grid_supp_PV;
    
    ST(N_conf).PQflow = BaseConf.PQflow;
    ST(N_conf).Pseudo = BaseConf.Pseudo;
    ST(N_conf).PQ = BaseConf.PQ;
    ST(N_conf).V = [1,3,6]; ST(N_conf).V = [ST(N_conf).V;BaseConf.delay*ones(size(ST(N_conf).V))];
    ST(N_conf).I = [1,3,5]; ST(N_conf).I = [ST(N_conf).I;BaseConf.delay*ones(size(ST(N_conf).I))];
    ST(N_conf).unc_dev = BaseConf.unc_dev;
    ST(N_conf).unc_pseudo = BaseConf.unc_pseudo;
    ST(N_conf).delay = BaseConf.delay;
    ST(N_conf).Ts = 1;
    ST(N_conf).filter = BaseConf.filter;
    ST(N_conf).colored = BaseConf.colored;
    ST(N_conf).dist = 1;
    ST(N_conf).dt = 0.1;
    
end


end