%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time dependent uncertainties impact on the base case
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts)

load(strcat(pwd,'\Results\',text_in,'_TD_test_type_10\Result_test'))
print_time = 1;

m=1;
x_axis_unique= [1:ST(m).Nnode];

unc(m).unc_I_P = unc(m).unc_I_P*100; %to convert it in crad
unc(m).unc_V_P = unc(m).unc_V_P*100; %to convert it in crad


index_a = find(time_test<Ts);
index_b = find(time_test>Ts);
index_2 = [max(index_a),min(index_b)];

if index_2(1) == index_2(2)
  unc_out_I_A =   unc(m).unc_I_A(:,index_2(1));
  unc_out_I_P =   unc(m).unc_I_P(:,index_2(1));  
  unc_out_V_A =   unc(m).unc_V_A(:,index_2(1));
  unc_out_V_P =   unc(m).unc_V_P(:,index_2(1));  
else
   
  unc_out_I_Aa =   unc(m).unc_I_A(:,index_2(1));
  unc_out_I_Pa =   unc(m).unc_I_P(:,index_2(1));  
  unc_out_V_Aa =   unc(m).unc_V_A(:,index_2(1));
  unc_out_V_Pa =   unc(m).unc_V_P(:,index_2(1));  
  
  unc_out_I_Ab =   unc(m).unc_I_A(:,index_2(2));
  unc_out_I_Pb =   unc(m).unc_I_P(:,index_2(2));  
  unc_out_V_Ab =   unc(m).unc_V_A(:,index_2(2));
  unc_out_V_Pb =   unc(m).unc_V_P(:,index_2(2));  
  
  unc_out_I_A = unc_out_I_Aa + ((unc_out_I_Aa- unc_out_I_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_out_I_P = unc_out_I_Pa + ((unc_out_I_Pa- unc_out_I_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_out_V_A = unc_out_V_Aa + ((unc_out_V_Aa- unc_out_V_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_out_V_P = unc_out_V_Pa + ((unc_out_V_Pa- unc_out_V_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
end


S = sprintf('branch=%d*', [0:size(unc(m).unc_I_P,1)-1]);
legend_branch = regexp(S, '*', 'split');

S = sprintf('node=%d*', [0:size(unc(m).unc_V_P,1)]);
legend_node = regexp(S, '*', 'split');


if print_time == 1
    
    figure
    semilogx (time_test,unc(m).unc_I_A')
    grid on
    xlim([time_test(1) time_test(end)])
    xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
    ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
    set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
    set(gca,'LineWidth', 0.5);
    set(findall(gcf,'type','text'),'FontSize',fnt);
    set(gcf, 'Units','centimeters');
    figrsz=get(gcf, 'Position');
    set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
    set(gcf,'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A5')
    papersize = get(gcf, 'PaperSize');
    left = (papersize(1)- width)/2;
    bottom = (papersize(2)- height)/2;
    myfiguresize = [left, bottom, width, height];
    set(gcf,'PaperPosition', myfiguresize);
    ylim([0 max(max(unc(m).unc_I_A))*1.25])
    legend(legend_branch);
 
    save_case =  strcat(pwd,'\fig_uncertainty_current_A_',text_in);
    print(save_case,'-depsc');  %Print the file figure in eps
    savefig(save_case)
    
    
    figure
    semilogx (time_test,unc(m).unc_I_P')
    grid on
    xlim([time_test(1) time_test(end)])
    xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
    ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
    set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
    set(gca,'LineWidth', 0.5);
    set(findall(gcf,'type','text'),'FontSize',fnt);
    set(gcf, 'Units','centimeters');
    figrsz=get(gcf, 'Position');
    set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
    set(gcf,'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A5')
    papersize = get(gcf, 'PaperSize');
    left = (papersize(1)- width)/2;
    bottom = (papersize(2)- height)/2;
    myfiguresize = [left, bottom, width, height];
    set(gcf,'PaperPosition', myfiguresize);
    ylim([0 max(max(unc(m).unc_I_P))*1.25])
    legend(legend_branch);
    
    save_case =  strcat(pwd,'\fig_uncertainty_current_P_',text_in);
    print(save_case,'-depsc');  %Print the file figure in eps
    savefig(save_case)
    
    % voltage
    % % amplitude and phase
    
    figure
    semilogx (time_test,unc(m).unc_V_A')
    grid on
    xlim([time_test(1) time_test(end)])
    xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
    ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
    set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
    set(gca,'LineWidth', 0.5);
    set(findall(gcf,'type','text'),'FontSize',fnt);
    set(gcf, 'Units','centimeters');
    figrsz=get(gcf, 'Position');
    set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
    set(gcf,'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A5')
    papersize = get(gcf, 'PaperSize');
    left = (papersize(1)- width)/2;
    bottom = (papersize(2)- height)/2;
    myfiguresize = [left, bottom, width, height];
    set(gcf,'PaperPosition', myfiguresize);
    ylim([0 max(max(unc(m).unc_V_A))*1.25])
    legend(legend_node);
    
    save_case =  strcat(pwd,'\fig_uncertainty_voltage_A_',text_in);
    print(save_case,'-depsc');  %Print the file figure in eps
    savefig(save_case)
    
    
    figure
    semilogx (time_test,unc(m).unc_V_P')
    grid on
    xlim([time_test(1) time_test(end)])
    xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
    ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
    set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
    set(gca,'LineWidth', 0.5);
    set(findall(gcf,'type','text'),'FontSize',fnt);
    set(gcf, 'Units','centimeters');
    figrsz=get(gcf, 'Position');
    set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
    set(gcf,'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A5')
    papersize = get(gcf, 'PaperSize');
    left = (papersize(1)- width)/2;
    bottom = (papersize(2)- height)/2;
    myfiguresize = [left, bottom, width, height];
    set(gcf,'PaperPosition', myfiguresize);
    ylim([0 max(max(unc(m).unc_V_P))*1.25])
    legend(legend_node);
    
    save_case =  strcat(pwd,'\fig_uncertainty_voltage_P_',text_in);
    print(save_case,'-depsc');  %Print the file figure in eps
    savefig(save_case)
    
elseif print_time == 0

%%%%%%%%%%%%%


figure
plot ([0:ST(m).Nnode-1],unc_out_I_A)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_uncertainty_current_A_TD_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
plot ([0:ST(m).Nnode-1],unc_out_I_P)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_uncertainty_current_P_TD_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage
% amplitude and phase

figure
plot ([0:ST(m).Nnode],unc_out_V_A)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_uncertainty_voltage_A_TD_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
plot ([0:ST(m).Nnode],unc_out_V_P)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_uncertainty_voltage_P_TD_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)
end
end