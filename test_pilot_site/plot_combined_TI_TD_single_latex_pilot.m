%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time independent uncertainties impact on the base
% case
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_combined_TI_TD_single_latex_pilot(width,height,alw,fnt,fontlegend,time_test,text_in,Ts,test_type)

uncTD = load(strcat(pwd,'\Results\',text_in,'_TD_test_type_',num2str(test_type),'\Result_test'));
uncTI = load(strcat(pwd,'\Results\',text_in,'_TI_test_type_',num2str(test_type),'\Result_test'));

uncTOT = [];

uncTOT.unc.unc_V_A = sqrt(uncTD.unc.unc_V_A.^2 +  uncTI.unc.unc_V_A.^2);
uncTOT.unc.unc_V_P = sqrt(uncTD.unc.unc_V_P.^2 +  uncTI.unc.unc_V_P.^2);
uncTOT.unc.unc_V_D = sqrt(uncTD.unc.unc_V_D.^2 +  uncTI.unc.unc_V_D.^2);
uncTOT.unc.unc_V_Q = sqrt(uncTD.unc.unc_V_Q.^2 +  uncTI.unc.unc_V_Q.^2);

uncTOT.unc.unc_I_A = sqrt(uncTD.unc.unc_I_A.^2 +  uncTI.unc.unc_I_A.^2);
uncTOT.unc.unc_I_P = sqrt(uncTD.unc.unc_I_P.^2 +  uncTI.unc.unc_I_P.^2);
uncTOT.unc.unc_I_D = sqrt(uncTD.unc.unc_I_D.^2 +  uncTI.unc.unc_I_D.^2);
uncTOT.unc.unc_I_Q = sqrt(uncTD.unc.unc_I_Q.^2 +  uncTI.unc.unc_I_Q.^2);

m = 1;
x_axis_unique= [1:uncTD.ST(m).Nnode];

legend_text = {'TD unc.','TI unc.','TOT unc.'};

if uncTD.ST(m).Nnode > 20
    xangle = -90;
else
    xangle = 0;
end


% 
% max_Vmagn = max(max(unc(m).unc_V_A))
% max_Vph = max(max(unc(m).unc_V_P))
% max_Imagn = max(max(unc(m).unc_I_A))
% max_Iph = max(max(unc(m).unc_I_P))

index_a = find(time_test<=Ts);
index_b = find(time_test>=Ts);
index_2 = [max(index_a),min(index_b)];

if index_2(1) == index_2(2)
    
  unc_TD_out_I_A =   uncTD.unc(m).unc_I_A(:,index_2(1));
  unc_TD_out_I_P =   uncTD.unc(m).unc_I_P(:,index_2(1));  
  unc_TD_out_V_A =   uncTD.unc(m).unc_V_A(:,index_2(1));
  unc_TD_out_V_P =   uncTD.unc(m).unc_V_P(:,index_2(1));  
  
  unc_TI_out_I_A =   uncTI.unc(m).unc_I_A;
  unc_TI_out_I_P =   uncTI.unc(m).unc_I_P;  
  unc_TI_out_V_A =   uncTI.unc(m).unc_V_A;
  unc_TI_out_V_P =   uncTI.unc(m).unc_V_P;  

  unc_TOT_out_I_A =   uncTOT.unc(m).unc_I_A(:,index_2(1));
  unc_TOT_out_I_P =   uncTOT.unc(m).unc_I_P(:,index_2(1));  
  unc_TOT_out_V_A =   uncTOT.unc(m).unc_V_A(:,index_2(1));
  unc_TOT_out_V_P =   uncTOT.unc(m).unc_V_P(:,index_2(1));  
  
else
    
  % TD 
  unc_TD_out_I_Aa =   uncTD.unc(m).unc_I_A(:,index_2(1));
  unc_TD_out_I_Pa =   uncTD.unc(m).unc_I_P(:,index_2(1));  
  unc_TD_out_V_Aa =   uncTD.unc(m).unc_V_A(:,index_2(1));
  unc_TD_out_V_Pa =   uncTD.unc(m).unc_V_P(:,index_2(1));  
  
  unc_TD_out_I_Ab =   uncTD.unc(m).unc_I_A(:,index_2(2));
  unc_TD_out_I_Pb =   uncTD.unc(m).unc_I_P(:,index_2(2));  
  unc_TD_out_V_Ab =   uncTD.unc(m).unc_V_A(:,index_2(2));
  unc_TD_out_V_Pb =   uncTD.unc(m).unc_V_P(:,index_2(2));  
  
  unc_TD_out_I_A = unc_TD_out_I_Aa + ((unc_TD_out_I_Aa- unc_TD_out_I_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TD_out_I_P = unc_TD_out_I_Pa + ((unc_TD_out_I_Pa- unc_TD_out_I_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TD_out_V_A = unc_TD_out_V_Aa + ((unc_TD_out_V_Aa- unc_TD_out_V_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TD_out_V_P = unc_TD_out_V_Pa + ((unc_TD_out_V_Pa- unc_TD_out_V_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  
  
  %TI
  unc_TI_out_I_A =   uncTI.unc(m).unc_I_A(:,1);
  unc_TI_out_I_P =   uncTI.unc(m).unc_I_P(:,1);  
  unc_TI_out_V_A =   uncTI.unc(m).unc_V_A(:,1);
  unc_TI_out_V_P =   uncTI.unc(m).unc_V_P(:,1);  
  
  
  
  %TOT
  unc_TOT_out_I_Aa =   uncTOT.unc(m).unc_I_A(:,index_2(1));
  unc_TOT_out_I_Pa =   uncTOT.unc(m).unc_I_P(:,index_2(1));  
  unc_TOT_out_V_Aa =   uncTOT.unc(m).unc_V_A(:,index_2(1));
  unc_TOT_out_V_Pa =   uncTOT.unc(m).unc_V_P(:,index_2(1));  
  
  unc_TOT_out_I_Ab =   uncTOT.unc(m).unc_I_A(:,index_2(2));
  unc_TOT_out_I_Pb =   uncTOT.unc(m).unc_I_P(:,index_2(2));  
  unc_TOT_out_V_Ab =   uncTOT.unc(m).unc_V_A(:,index_2(2));
  unc_TOT_out_V_Pb =   uncTOT.unc(m).unc_V_P(:,index_2(2));  
  
  unc_TOT_out_I_A = unc_TOT_out_I_Aa + ((unc_TOT_out_I_Aa- unc_TOT_out_I_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TOT_out_I_P = unc_TOT_out_I_Pa + ((unc_TOT_out_I_Pa- unc_TOT_out_I_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TOT_out_V_A = unc_TOT_out_V_Aa + ((unc_TOT_out_V_Aa- unc_TOT_out_V_Ab)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  unc_TOT_out_V_P = unc_TOT_out_V_Pa + ((unc_TOT_out_V_Pa- unc_TOT_out_V_Pb)/(time_test(index_2(1)) - time_test(index_2(2))) ) * ( Ts - time_test(index_2(1)));
  
  
end


% current
% amplitude and phase

figure
plot ([0:uncTD.ST(m).Nnode-1],unc_TD_out_I_A,'b')
hold on
plot ([0:uncTD.ST(m).Nnode-1],unc_TI_out_I_A,'r')
plot ([0:uncTD.ST(m).Nnode-1],unc_TOT_out_I_A,'k')
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([0 uncTD.ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_current_A_',num2str(test_type),'_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng'); 

savefig(save_case)





figure
plot ([0:uncTD.ST(m).Nnode-1],unc_TD_out_I_P)
hold on
plot ([0:uncTD.ST(m).Nnode-1],unc_TI_out_I_P,'r')
plot ([0:uncTD.ST(m).Nnode-1],unc_TOT_out_I_P,'k')
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([0 uncTD.ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_current_P_',num2str(test_type),'_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');

savefig(save_case)




% voltage
% amplitude and phase

figure
plot ([1:uncTD.ST(m).Nnode],unc_TD_out_V_A(2:end))
hold on
plot ([1:uncTD.ST(m).Nnode],unc_TI_out_V_A(2:end),'r')
plot ([1:uncTD.ST(m).Nnode],unc_TOT_out_V_A(2:end),'k')
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([1 uncTD.ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_voltage_A_',num2str(test_type),'_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng'); 

savefig(save_case)



figure
plot ([1:uncTD.ST(m).Nnode],unc_TD_out_V_P(2:end))
hold on
plot ([1:uncTD.ST(m).Nnode],unc_TI_out_V_P(2:end),'r')
plot ([1:uncTD.ST(m).Nnode],unc_TOT_out_V_P(2:end),'k')
xticks(x_axis_unique); xticklabels(x_axis_unique);

xtickangle(xangle)

xlim([1 uncTD.ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_text);
set(legend,'FontSize',fontlegend,'Interpreter','latex');

save_case =  strcat(pwd,'\Results\fig_uncertainty_voltage_P_',num2str(test_type),'_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
print(save_case,'-dsvg');  
print(save_case,'-dpng');

savefig(save_case)


end