# Model for propagation of uncertainty of monitoring systems

This repository contains the following material:

- Matlab code to calculate the expected uncertainty of distribution system state estimation "generic_test_mode_MC_DSSE"
- Matlab code to calculate the expected uncertainty of generic monitoring systems "generic_test_mode_MC_GP"
- Matlab code to run the tests of the IEEE paper "Models for Propagation of State Estimation Uncertainty in Static and Dynamic Scenarios": "test_model_MC_GP_paper_IEEE_IMS"
- Matlab code to run impact analysis test of uncertainty on distribution systems "Input_impact_test"
- Matlab code to run impact analysis on LV pilot site "Test_Pilot_LV"
- Matlab code to run impact analysis on MV pilot site "Test_Pilot_MV"

This repository has some interdependences with two further open repositories:

- Distribution system state estimation code:
  https://git.rwth-aachen.de/acs/public/automation/dsse

- Model of dynamic distribution system code:
  https://git.rwth-aachen.de/acs/public/simulation/ssmd


## Copyright

2018, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](LICENSE).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](https://git.rwth-aachen.de/acs/public/villas/VILLASnode/raw/develop/doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Andrea Angioni <aangioni@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de) 




