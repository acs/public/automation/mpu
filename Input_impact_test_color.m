%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main Script to run the impact analysis test
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
clear all
close all


Ntot = 9^2; %evenly spaced among decades
t_delta_in = -6 ; 
t_delta_fin = 4; %so is 10 intervals per decade
time_test = logspace(t_delta_in,t_delta_fin,Ntot);
Vref = 0.99; Tref = 1;
[diag_ratio,V] = update_Vdisturbance(Vref,Tref,time_test);

width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;
text_in = 'Comparison';
%run base_case_LV
run base_case_MV

%type_test = [0,1,2,3,4,5,6,7,8,10,11,12,13,14,15,16];

%0 base case
%1 number nodes
%2 number lines
%3 power consumption
%4 line impedances
%5 ratio of inverters installed
%6 switching frequency
%7 colored noise Ts
%8 colored noise autovariance
%9 power factor

%10 number voltage devices
%11 number current flow devices
%12 number P inj devices
%13 number P flow devices
%14 accuracy meas devices
%15 accuracy pseudo meas
%16 delay
%17 standard deviation noise
%18 delay_colnoise
%23 sign of power

%% Time Indipendent Uncertainty Preparation Data
type_test = [0,1,2,3,4,9,10,11,12,13,14,15,23];

for Ntest = 1 : length(type_test)
    display(strcat('Test TI number: ',num2str(type_test(Ntest))));
    [ST,N_conf] = generate_conf(BaseConf,type_test(Ntest));
    run Test_Impact_Scenarios_TI.m
    save_dir = strcat(pwd,'\Results\Comparison_TI_test_type_',num2str(type_test(Ntest)));
    mkdir(save_dir);
    save_cat = strcat(save_dir,'\Result_Test');
    save(save_cat,'Test_rep_IDQ','Test_rep_VDQ','Test_rep_IAP','Test_rep_VAP','ST','conf_test','P','unc');
end

%% Time Dependent Uncertainty Preparation Data
type_test = [0,0.1,1,2,3,4,5,6,8,9,16,17,23]

for Ntest = 1 : length(type_test)
    display(strcat('Test TD number: ',num2str(type_test(Ntest))));
    [ST,N_conf] = generate_conf(BaseConf,type_test(Ntest));
    run Test_Impact_Scenarios_TDcolor.m
    save_dir =strcat(pwd,'\Results\Comparison_TD_test_type_',num2str(type_test(Ntest)));
    mkdir(save_dir);
    save_cat = strcat(save_dir,'\Metric_Test');
    save(save_cat,'ST','conf_test','P','unc','eigA_m','time_test');
end

%%
% Time Dependent Uncertainty Generation Indexes
type_test = [0,0.1,1,2,3,4,5,6,8,9,16,17,23]

for Ntest = 1 : length(type_test)
    Ntest
    save_dir =strcat(pwd,'\Results\Comparison_TD_test_type_',num2str(type_test(Ntest)),'\Metric_Test');
    load(save_dir)
    run Generate_Test_Metrics_TDcolor.m
    save_dir = strcat(pwd,'\Results\Comparison_TD_test_type_',num2str(type_test(Ntest)),'\Result_Test');
    save(save_dir,'Test_rep_IDQ','Test_rep_VDQ','Test_rep_IAP','Test_rep_VAP','unc','conf_test','ST')
end



%% plot base case for TI & TD
plot_TI_single_latex(width,height,alw,fnt,fontlegend,text_in)
%%
plot_TD_single_latex(width,height,alw,fnt,fontlegend,time_test,text_in)
%%
plot_TD_single_latex_color(width,height,alw,fnt,fontlegend,time_test,text_in)
%%
plot_TD_single_delay_latex(width,height,alw,fnt,fontlegend,time_test,text_in)
%%
plot_TD_single_delay_colored_latex(width,height,alw,fnt,fontlegend,time_test)
%%
plot_TD_single_colored_auto_latex(width,height,alw,fnt,fontlegend,time_test)
%%
plot_TD_single_colored_TS_latex(width,height,alw,fnt,fontlegend,time_test)
%% plot scenarios TI & TD

width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;

% to plot TI
% TI=1; TD=0;
% type_test = [1,2,3,4,9,10,11,12,13,14,15];
% type_test = 15;

% to plot TD
TI=0; TD=1;
type_test = [1,2,3,4,5,6,9,17,18,23];
type_test = 1;

if      type_test == 1;  tt = 'No.node';
elseif  type_test == 2;  tt = 'No.line';
elseif  type_test == 3;  tt = 'kVA';
elseif  type_test == 4;  tt = '\Omega';
elseif  type_test == 5;  tt = 'rate.inv.';
elseif  type_test == 6;  tt = 'Hz';
elseif  type_test == 7;  tt = 's';
elseif  type_test == 8;  tt = 'auto';
elseif  type_test == 9;  tt = 'PF';
elseif  type_test == 10; tt = 'No.v.';
elseif  type_test == 11; tt = 'No.c.';
elseif  type_test == 12; tt = 'No.PQ.inj';
elseif  type_test == 13; tt = 'No.PQ.flow';
elseif  type_test == 14; tt = 'unc dev';
elseif  type_test == 15; tt = 'unc pseudo';
elseif  type_test == 16; tt = 's';
elseif  type_test == 17; tt = '-'; %std_noise
elseif  type_test == 18; tt = 's';
elseif  type_test == 23; tt = 'kVA';%sign power
end

tt

[Test_rep_I,Test_rep_V,Test_rep_IAP,Test_rep_VAP,x_axis] = Test_Impact_Merge(BaseConf,type_test,TI,TD);

coeff1_I    = zeros(size(Test_rep_I'));
bias_I      = zeros(size(Test_rep_I'));
poli1_I     = zeros(size(Test_rep_I'));
coeff1_IAP  = zeros(size(Test_rep_I'));
bias_IAP    = zeros(size(Test_rep_I'));
slope_IAP   = zeros(size(Test_rep_I'));
poli1_IAP   = zeros(size(Test_rep_I'));

coeff1_V  = zeros(size(Test_rep_V'));
bias_V    = zeros(size(Test_rep_V'));
slope_V   = zeros(size(Test_rep_V'));
poli1_V   = zeros(size(Test_rep_V'));

coeff1_VAP = zeros(size(Test_rep_V'));
bias_VAP   = zeros(size(Test_rep_V'));
slope_VAP  = zeros(size(Test_rep_V'));
poli1_VAP  = zeros(size(Test_rep_V'));

for z = 1 : size(Test_rep_I,2)
    coeff1_I(z,1:2) = polyfit(x_axis,Test_rep_I(:,z),1);
    bias_I(z,1)  =  coeff1_I(z,2);
    slope_I(z,1) =  coeff1_I(z,1);
    poli1_I(z,:) = coeff1_I(z,2) + coeff1_I(z,1)*x_axis;
    
    coeff1_IAP(z,1:2) = polyfit(x_axis,Test_rep_IAP(:,z),1);
    bias_IAP(z,1)  =  coeff1_IAP(z,2);
    slope_IAP(z,1) =  coeff1_IAP(z,1);
    poli1_IAP(z,:) = coeff1_IAP(z,2) + coeff1_IAP(z,1)*x_axis;
    
    coeff1_V(z,1:2) = polyfit(x_axis,Test_rep_V(:,z),1);
    bias_V(z,1)  =  coeff1_V(z,2);
    slope_V(z,1) =  coeff1_V(z,1);
    poli1_V(z,:) = coeff1_V(z,2) + coeff1_V(z,1)*x_axis;
    
    coeff1_VAP(z,1:2) = polyfit(x_axis,Test_rep_VAP(:,z),1);
    bias_VAP(z,1)  =  coeff1_VAP(z,2);
    slope_VAP(z,1) =  coeff1_VAP(z,1);
    poli1_VAP(z,:) =  coeff1_VAP(z,2) + coeff1_VAP(z,1)*x_axis;
end

if type_test == 3 || type_test == 23
    slope_VAP = slope_VAP*1000;
    slope_IAP = slope_IAP*1000;
end


%plot_impact_color(Test_rep_I,Test_rep_V,Test_rep_IAP,Test_rep_VAP,poli1_I,poli1_V,poli1_IAP,poli1_VAP,x_axis,type_test,TI,TD,width,height,alw,fnt,fontlegend)
x_axis = [];

if TI == 1
    display(strcat('bias_amplitude = ',{' '},num2str(bias_VAP(1,1),'%10.1e'),{' V'},{' '},'&',{' '},num2str(bias_IAP(1,1),'%10.1e'),{' A'}));
    display(strcat('bias_phangle   = ',{' '},num2str(bias_VAP(2,1),'%10.1e'),{' crad'},{' '},'&',{' '},num2str(bias_IAP(2,1),'%10.1e'),{' crad'}));
    display(strcat('slope_amplitude = ',{' '},num2str(slope_VAP(1,1),'%10.1e'),' $\nicefrac{V}{',tt,'}$',{' '},'&',{' '},num2str(slope_IAP(1,1),'%10.1e'),' $\nicefrac{A}{',tt,'}$'));
    display(strcat('slope_phangle   = ',{' '},num2str(slope_VAP(2,1),'%10.1e'),' $\nicefrac{crad}{',tt,'}$',{' '},'&',{' '},num2str(slope_IAP(2,1),'%10.1e'),' $\nicefrac{crad}{',tt,'}$'));
else
    disp(strcat('bias  & ',{' '},num2str(bias_VAP(5,1),'%10.1e'),{'s'},{' '},'&',{' '},num2str(bias_VAP(6,1),'%10.1e'),{'s'}));
    display(strcat('slope & ',{' '},num2str(slope_VAP(5,1),'%10.1e'),{'s'},{' '},'&',{' '},num2str(slope_VAP(6,1),'%10.1e'),{'s'}));
    
    display(strcat('bias & ',{' '},num2str(bias_VAP(3,1),'%10.1e'),'V',{' '},'&',{' '},num2str(bias_IAP(3,1),'%10.1e'),'A',{' '},'&',{' '}...
        ,num2str(bias_VAP(4,1),'%10.1e'),'crad',{' '},'&',{' '},num2str(bias_IAP(4,1),'%10.1e'),'crad'));
    display(strcat('slope & ',{' '},num2str(slope_VAP(3,1),'%10.1e'),' $\nicefrac{V}{',tt,'}$',{' '},'&',{' '},num2str(slope_IAP(3,1),'%10.1e'),' $\nicefrac{A}{',tt,'}$',{' '},'&',{' '}...
    ,num2str(slope_VAP(4,1),'%10.1e'),' $\nicefrac{crad}{',tt,'}$',{' '},'&',{' '},num2str(slope_IAP(4,1),'%10.1e'),' $\nicefrac{crad}{',tt,'}$'));
end






