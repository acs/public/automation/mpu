%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to run the test to model and test the uncertainty of a simple
% second order system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
format longg

dev_input       = 0.0;   %relative standard deviation in the input
dev_status      = 0.0;   %relative standard deviation in the initial status
unc_disturbance = 0.01;  %relative standard deviation in the status
unc_measure     = 0.01;  %relative standard deviation of the measurements
input = 5; %value of the input

N_MC = 10000; %100000 number of MC for each scenario
Ts = 1e6; %for Ts = 1e6 in test white; 1e0 in colored test
Nt = 30; %30 number of time windows to be tested
t_delta_in = -7 ; %-7 smallest window 10^t_delta_in
t_delta_fin =  2; %2 largest window 10^t_delta_fin
time_test = logspace(t_delta_in,t_delta_fin,Nt); %all the windows are obtained with the logaritimic spacing



%system matrices
Atot          = [0 1 ; -35 -3.5];
eig_A = eig(Atot)
B_input       = [ 0.28   ;   35];
B_disturbance = [ 0.28   ;   35];
C = 0.1 * [ 1 0.2 ;-0.3 1.4]; %this is equivalent to the Jacobian


Vref = 0.9; Tref = 1;
[diag_ratio,V] = update_Vdisturbance(Vref,Tref,time_test);


VS = struct; SD = struct; %structure with variance and status data
VS.diag_ratio = diag_ratio; VS.V = V; 
% get to regime
SD.x_status = [0 0]; %starting from a arbitrary initial point we bring the system to regime
x_status(:,1) = SD.x_status;
SD.input = input;
SD.unc_disturbance = unc_disturbance; %
SD.dev_status = dev_status;
SD.dev_input = dev_input;
time_delta = 1e-3; %time resolution of the simulation
Ns = 20; %number of seconds to be simulated
[ Tmatrix,Forz_matrix,~] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
for t = 1 : Ns*inv(time_delta)
    x_status(:,2) = Tmatrix * x_status(:,1);
    x_status(:,3) = Forz_matrix * SD.input;
    x_status(:,5) = x_status(:,2) + x_status(:,3);
    x_status(:,1) = x_status(:,5);
end
SD.x_status = x_status(:,5);%such value is used as initial value for the following simulations


z_measure_true = C * SD.x_status; %measurement vector
LM = 1e-10; %limit minimum
for x = 1 : size(z_measure_true,1)
    Rtemp = (unc_measure*z_measure_true(x,1)).^2;
    if Rtemp < LM %if the element of the covariance is too small it is subsistuted with a predefined value
        Rtemp = LM;
    end
    R(x,x) = Rtemp; %covariance
end
%R = ((unc_measure*z_measure_true).^2).*eye(2);
G1 = C' * inv(R) * C; %gain matrix of state estimation
VS.R = R;
VS.var_status_absolute = inv(G1);
VS.Gain_matrix = G1;
VS.invG_HW = VS.var_status_absolute * C' * inv(R);
VS.var_disturbance_absolute_white =  (unc_disturbance^2)*eye(1);
VS.var_disturbance_absolute =  VS.var_disturbance_absolute_white; 
VS.unc_measure = unc_measure;
VS.meas_delay = 0.5; %delay between the arrival of the masurement and the state estimation - batch case
VS.F_matrix = 0.99 ; %transfer function in case we have colored noise (filter_on = 1);

    %in order to get the initial status covariance, 10 seconds with the
    %disturbances are simulated
    Tss = 1e4; %time resolution of the disturbance
    Nts = 50; %10 seconds of white noise disturbance
    P_delta_x = zeros(size(VS.var_status_absolute));
    [ TmatrixTs,~,Dist_matrixTs] = calc_TDF(Tss,Atot,B_input,B_disturbance);
    Tmatrix_eq = eye(size(TmatrixTs)); %Tmatrix_eq = zeros(size(TmatrixTs));
    time_delta = 1e-3; %time resolution of the simulation
    for t = 1 :  Nts
        Tmatrix_eq = Tmatrix_eq*TmatrixTs;
        P_delta_x = TmatrixTs * P_delta_x * transpose(TmatrixTs) + Dist_matrixTs * VS.var_disturbance_absolute * transpose(Dist_matrixTs)  ;
    end
    VS.P_delta_x = diag(diag(P_delta_x)); %this is the initial state covariance
    %%%





%% test uncertainty model
type_test= [0:11];


for x = 1 : length(type_test)
    type_test(x)
    [ST,N_conf] = generate_conf_GP(type_test(x));
    %%% small variation of model depending on test case
    VS.time_delta_individual_meas = ST.time_delta_individual_meas; %delays associated to each single measurement


    if ST.type_unc ~= 5 
        fprintf(strcat('\n',ST(N_conf).Script,'\n','starting at','_',datestr( datetime('now'))))
        fprintf('\n Monte Carlo tests of state estimation');
        for t = 1 : Nt
            time_delta = time_test(1,t);
            if ST.filter_on == 1
                VS.var_disturbance_absolute = VS.diag_ratio(t)*VS.var_disturbance_absolute_white;
                SD.unc_disturbance = sqrt(VS.var_disturbance_absolute);
            end
            [unc_estimated(:,t)] = uncertainty_model_GP(Atot,B_input,B_disturbance,C,SD,VS,dev_input,time_delta,ST.type_unc,ST.Ts,ST.Nw,ST.filter_on);
            [unc_system(:,t),unc_SE(:,t)] = uncertainty_MC_SE_GP(Atot,B_input,B_disturbance,C,SD,VS,dev_input,time_delta,ST.type_unc,ST.Ts,ST.Nw,N_MC,ST.filter_on);
        end
        plot_3fig_uncertainty_GP_latex(unc_estimated,unc_system,unc_SE,time_test,ST(N_conf).Script)
    end
    
    % test time simulation estimated and actual
    time_delta = 1e-1; %time resolution of the simulation
    Nw = 100;
    fprintf('\n Time tests of state estimation');
    [x_status_t,x_status_est_t] = time_evolution_GP(Atot,B_input,B_disturbance,C,SD,VS,dev_input,time_delta,ST.type_unc,ST.Ts,Nw,ST.filter_on);
    plot_3fig_time_GP_latex(x_status_t,x_status_est_t,time_delta,Nw,ST(N_conf).Script)
    
end



%% test time simulation
% fprintf('\n Simulation in time of dynamic system');
% if Ts <= 1e-2
%     time_delta = Ts;
% else
%     time_delta = 1e-3;
% end
%
% Ns = 20;
% u = SD.input;
% [ Tmatrix,Forz_matrix,Dist_matrix] = calc_TDF(time_delta,Atot,B_input,B_disturbance);
% x_status(:,1) = SD.x_status;
%
% for t = 1 : Ns*inv(time_delta)
%     d = SD.unc_disturbance.*randn(size(B_disturbance,2),1);
%
%     x_status(:,2) = Tmatrix*x_status(:,1);
%     x_status(:,3) = Forz_matrix*u;
%     x_status(:,4) = Dist_matrix*d;
%     x_status(:,5) = x_status(:,2) + x_status(:,3) + x_status(:,4);
%     x_status(:,1) = x_status(:,5);
%     x_status_t(:,t) = x_status(:,5);
%
% end
% figure
% plot(time_delta*[1:Ns*inv(time_delta)],x_status_t)
% legend('status 1','status 2')
