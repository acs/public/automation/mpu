%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program to plot the uncertainties of the system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_3fig_uncertainty_GP_latex(unc_estimated,unc_system,unc_SE,time_test,script_in)

fprintf(strcat('\n','plotting results of the simulation'))

width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;



figure;
%movegui(f,'southwest');
%set(f, 'Position', [5, 400, 450, 280])
semilogx (time_test(1:end),unc_estimated,'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northeast')
grid on
%title('unc estimated model')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_unc_est_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southeast');
%set(f, 'Position', [5, 40, 450, 280])
%semilogx (time_test(1:end),(unc_SE - unc_system)./unc_SE ,'-s')
semilogx (time_test(1:end),(unc_SE - unc_system) ,'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northeast')
grid on
%title('deviation SE - MC uncertainty')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Difference Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_diff_unc_SE_MC_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southeast');
%set(f, 'Position', [5, 40, 450, 280])
%semilogx (time_test(1:end),(unc_SE - unc_system)./unc_SE ,'-s')
semilogx (time_test(1:end),unc_SE ,'-s')
S = sprintf('SE status %d*', 1:size(unc_estimated,1));
%C = regexp(S, '*', 'split');
hold on
semilogx (time_test(1:end), unc_system ,'--o','markers',4)
S = [S,sprintf('MC status %d*', 1:size(unc_estimated,1))];
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'west')
grid on
%title('deviation SE - MC uncertainty')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_together_unc_SE_MC_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southwest');
%set(f, 'Position', [425, 400, 450, 280])
semilogx (time_test(1:end),unc_system,'-s')
S = sprintf('status %d*', 1:size(unc_system,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northeast')
grid on
%title('unc test model with MC')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_unc_MC_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southeast');
%set(f, 'Position', [425, 40, 450, 280])
%semilogx (time_test(1:end),(unc_system - unc_estimated)./unc_system ,'-s')
semilogx (time_test(1:end),(unc_system - unc_estimated) ,'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northeast')
grid on
%title('deviation MC - estimated uncertainty')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Difference Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_diff_unc_MC_est_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southeast');
%set(f, 'Position', [425, 40, 450, 280])
%semilogx (time_test(1:end),(unc_system - unc_estimated)./unc_system ,'-s')
semilogx (time_test(1:end), unc_estimated ,'-s')
S = sprintf('model %d*', 1:size(unc_estimated,1));
%C = regexp(S, '*', 'split');
hold on
semilogx (time_test(1:end),unc_system  ,'--o','markers',4)
S = [S,sprintf('MC %d*', 1:size(unc_estimated,1))];
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'west')
grid on
%title('deviation MC - estimated uncertainty')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_togheter_unc_MC_est_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure;
%movegui(f,'southwest');
%set(f, 'Position', [855, 400, 450, 280])
semilogx (time_test(1:end),unc_SE,'-s')
S = sprintf('status %d*', 1:size(unc_SE,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northeast')
grid on
%title('unc test model with MC and SE')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_unc_SE_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southeast');
%set(f, 'Position', [855, 40, 450, 280])
%semilogx (time_test(1:end),(unc_SE - unc_estimated)./unc_SE ,'-s')
semilogx (time_test(1:end),(unc_SE - unc_estimated),'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northeast')
grid on
%title('deviation SE - estimated uncertainty')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Difference Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_lin_diff_unc_SE_est_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

figure
%movegui(f,'southeast');
%set(f, 'Position', [855, 40, 450, 280])
%semilogx (time_test(1:end),(unc_SE - unc_estimated)./unc_SE ,'-s')
semilogx (time_test(1:end),unc_estimated,'-s')
S = sprintf('estimated %d*', 1:size(unc_estimated,1));
%C = regexp(S, '*', 'split');
hold on
semilogx (time_test(1:end),unc_SE ,'--o','markers',4)
S = [S,sprintf('SE %d*', 1:size(unc_estimated,1))];
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'west')
grid on
%title('deviation SE - estimated uncertainty')
xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty [-]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
xlim([time_test(1) time_test(end)])
save_case =  strcat(pwd,'\fig_lin_togheter_unc_SE_est_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

diff1 = (unc_SE - unc_estimated)./unc_SE;
c = [diff1(1,:),diff1(2,:)];
xlim1 = 1.33*max(abs(c'));

figure
histc = histogram(c,20,'FaceColor',[221, 64, 45] / 256,'Normalization','probability');
ylim1 = 1.33*max(histc.Values);
xlabel('e_{unc} [-]','FontSize',fnt,'Interpreter','tex')
ylabel('Probability [-]','FontSize',fnt,'Interpreter','latex')
xlim([-xlim1 xlim1]) 
ylim([0 ylim1]) 

set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_histogram_error_',script_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

end

