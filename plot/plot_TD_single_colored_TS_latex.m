%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time dependent uncertainties impact of colored noise
% time resolution
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_TD_single_colored_Ts_latex(width,height,alw,fnt,fontlegend,time_test)

load(pwd,'\Results\Comparison_TD_test_type_7\Result_test')
ref1 = load(pwd,'\Results\Comparison_TD_test_type_0\Result_Test');

for m = 1 : length(conf_test)
    unc(m).unc_I_P = unc(m).unc_I_P*100; %to convert it in crad
    unc(m).unc_V_P = unc(m).unc_V_P*100; %to convert it in crad
    for t = 1 : length(time_test)
        max_I_D(m,t) = max(unc(m).unc_I_D(:,t));
        max_I_Q(m,t) = max(unc(m).unc_I_Q(:,t));
        max_I_A(m,t) = max(unc(m).unc_I_A(:,t));
        max_I_P(m,t) = max(unc(m).unc_I_P(:,t));
        
        max_V_D(m,t) = max(unc(m).unc_V_D(:,t));
        max_V_Q(m,t) = max(unc(m).unc_V_Q(:,t));
        max_V_A(m,t) = max(unc(m).unc_V_A(:,t));
        max_V_P(m,t) = max(unc(m).unc_V_P(:,t));
    end
end



%S = sprintf('Ts %0.2f*', [ST(2).Ts,ST(3).Ts,ST(4).Ts,ST(5).Ts,ST(6).Ts] );
%legend_x = regexp(S, '*', 'split');
%legend_x = ['white',legend_x(1:end-1)];
legend_x = {'white';'\Delta t_s = 0.1';'\Delta t_s = 0.32';'\Delta t_s = 1';'\Delta t_s = 3.2';'\Delta t_s = 10'};
%Ts_x =     [1e6 1e-1 0.3162 1e0 3.162 1e1]; 

% amplitude and phase

figure
semilogx (time_test,max_I_A(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_I_A(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_I_A(3,:),'LineWidth',1.5,'Color',[0.86,0.25,0.18])
semilogx (time_test,max_I_A(4,:),'LineWidth',1.5,'Color','blue')
semilogx (time_test,max_I_A(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
semilogx (time_test,max_I_A(6,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
y_time_a = [min(min(max_I_A)) max(max(max_I_A))*1.2];
txt = {'\leftarrow start dyn.','     region'}; 
text(ref1.Test_rep_IAP(1,5),max(max(max_I_A))*1,txt,'Color','red','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
y_time_b = [min(min(max_I_A)) max(max(max_I_A))*1.15];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(ref1.Test_rep_IAP(1,6),max(max(max_I_A))*1.15,txt,'Color','red','Interpreter','tex')

x_time_a1 = [1e-1 1e-1];
y_time_a1 = [min(min(max_I_A)) max(max(max_I_A))*0.6];
x_time_a2 = [0.3162 0.3162];
y_time_a2 = [min(min(max_I_A)) max(max(max_I_A))*1];
x_time_a3 = [1e0 1e0];
y_time_a3 = [min(min(max_I_A)) max(max(max_I_A))*0.8];
x_time_a4 = [3.162 3.162];
y_time_a4 = [min(min(max_I_A)) max(max(max_I_A))*0.9];
x_time_a5 = [1e1 1e1];
y_time_a5 = [min(min(max_I_A)) max(max(max_I_A))*1];

line(x_time_a1,y_time_a1,'Color','green','LineStyle','--')
line(x_time_a2,y_time_a2,'Color',[0.86,0.25,0.18],'LineStyle','--')
line(x_time_a3,y_time_a3,'Color','blue','LineStyle','--')
line(x_time_a4,y_time_a4,'Color',[0.580, 0.188,1],'LineStyle','--')
line(x_time_a5,y_time_a5,'Color',[0.39,  0.86,0.90],'LineStyle','--')

txt = '0.1'; 
text(1e-1,max(max(max_I_A))*0.6,txt,'Color','green','Interpreter','tex')
txt = ' 0.32'; 
text(0.3162,max(max(max_I_A))*0.95,txt,'Color',[0.86,0.25,0.18],'Interpreter','tex')
txt = '1'; 
text(1e0,max(max(max_I_A))*0.8,txt,'Color','blue','Interpreter','tex')
txt = '3.2'; 
text(3.162,max(max(max_I_A))*0.9,txt,'Color',[0.580, 0.188,1],'Interpreter','tex')
txt = '10'; 
text(1e1,max(max(max_I_A))*1,txt,'Color',[0.39,  0.86,0.90],'Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','tex','Location','NorthWest');
%ylim([0 max(max(max_I_A))*1.20])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_TS_current_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
semilogx (time_test,max_I_P(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_I_P(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_I_P(3,:),'LineWidth',1.5,'Color',[0.86,0.25,0.18])
semilogx (time_test,max_I_P(4,:),'LineWidth',1.5,'Color','blue')
semilogx (time_test,max_I_P(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
semilogx (time_test,max_I_P(6,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
y_time_a = [min(min(max_I_P)) max(max(max_I_P))*1.2];
txt = {'\leftarrow start dyn.','     region'};
text(ref1.Test_rep_IAP(1,5),max(max(max_I_P))*1,txt,'Color','red','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
y_time_b = [min(min(max_I_P)) max(max(max_I_P))*1.15];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(ref1.Test_rep_IAP(1,6),max(max(max_I_P))*1.15,txt,'Color','red','Interpreter','tex')

x_time_a1 = [1e-1 1e-1];
y_time_a1 = [min(min(max_I_P)) max(max(max_I_P))*0.6];
x_time_a2 = [0.3162 0.3162];
y_time_a2 = [min(min(max_I_P)) max(max(max_I_P))*1];
x_time_a3 = [1e0 1e0];
y_time_a3 = [min(min(max_I_P)) max(max(max_I_P))*0.8];
x_time_a4 = [3.162 3.162];
y_time_a4 = [min(min(max_I_P)) max(max(max_I_P))*0.9];
x_time_a5 = [1e1 1e1];
y_time_a5 = [min(min(max_I_P)) max(max(max_I_P))*1];

line(x_time_a1,y_time_a1,'Color','green','LineStyle','--')
line(x_time_a2,y_time_a2,'Color',[0.86,0.25,0.18],'LineStyle','--')
line(x_time_a3,y_time_a3,'Color','blue','LineStyle','--')
line(x_time_a4,y_time_a4,'Color',[0.580, 0.188,1],'LineStyle','--')
line(x_time_a5,y_time_a5,'Color',[0.39,  0.86,0.90],'LineStyle','--')

txt = '0.1'; 
text(1e-1,max(max(max_I_P))*0.6,txt,'Color','green','Interpreter','tex')
txt = ' 0.32'; 
text(0.3162,max(max(max_I_P))*0.95,txt,'Color',[0.86,0.25,0.18],'Interpreter','tex')
txt = '1'; 
text(1e0,max(max(max_I_P))*0.8,txt,'Color','blue','Interpreter','tex')
txt = '3.2'; 
text(3.162,max(max(max_I_P))*0.9,txt,'Color',[0.580, 0.188,1],'Interpreter','tex')
txt = '10'; 
text(1e1,max(max(max_I_P))*1,txt,'Color',[0.39,  0.86,0.90],'Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','tex','Location','NorthWest');
%ylim([0 max(max(max_I_P))*1.60])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_TS_current_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage




% amplitude and phase

figure
semilogx (time_test,max_V_A(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_V_A(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_V_A(3,:),'LineWidth',1.5,'Color',[0.86,0.25,0.18])
semilogx (time_test,max_V_A(4,:),'LineWidth',1.5,'Color','blue')
semilogx (time_test,max_V_A(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
semilogx (time_test,max_V_A(6,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
y_time_a = [min(min(max_V_A)) max(max(max_V_A))*1.2];
txt = {'\leftarrow start dyn.','     region'};
text(ref1.Test_rep_IAP(1,5),max(max(max_V_A))*1,txt,'Color','red','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
y_time_b = [min(min(max_V_A)) max(max(max_V_A))*1.15];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(ref1.Test_rep_IAP(1,6),max(max(max_V_A))*1.15,txt,'Color','red','Interpreter','tex')

x_time_a1 = [1e-1 1e-1];
y_time_a1 = [min(min(max_V_A)) max(max(max_V_A))*0.6];
x_time_a2 = [0.3162 0.3162];
y_time_a2 = [min(min(max_V_A)) max(max(max_V_A))*1];
x_time_a3 = [1e0 1e0];
y_time_a3 = [min(min(max_V_A)) max(max(max_V_A))*0.8];
x_time_a4 = [3.162 3.162];
y_time_a4 = [min(min(max_V_A)) max(max(max_V_A))*0.9];
x_time_a5 = [1e1 1e1];
y_time_a5 = [min(min(max_V_A)) max(max(max_V_A))*1];

line(x_time_a1,y_time_a1,'Color','green','LineStyle','--')
line(x_time_a2,y_time_a2,'Color',[0.86,0.25,0.18],'LineStyle','--')
line(x_time_a3,y_time_a3,'Color','blue','LineStyle','--')
line(x_time_a4,y_time_a4,'Color',[0.580, 0.188,1],'LineStyle','--')
line(x_time_a5,y_time_a5,'Color',[0.39,  0.86,0.90],'LineStyle','--')

txt = '0.1'; 
text(1e-1,max(max(max_V_A))*0.6,txt,'Color','green','Interpreter','tex')
txt = ' 0.32'; 
text(0.3162,max(max(max_V_A))*0.95,txt,'Color',[0.86,0.25,0.18],'Interpreter','tex')
txt = '1'; 
text(1e0,max(max(max_V_A))*0.8,txt,'Color','blue','Interpreter','tex')
txt = '3.2'; 
text(3.162,max(max(max_V_A))*0.9,txt,'Color',[0.580, 0.188,1],'Interpreter','tex')
txt = '10'; 
text(1e1,max(max(max_V_A))*1,txt,'Color',[0.39,  0.86,0.90],'Interpreter','tex')


grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','tex','Location','NorthWest');
%ylim([0 max(max(max_V_A))*1.60])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_TS_voltage_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




figure
semilogx (time_test,max_V_P(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_V_P(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_V_P(3,:),'LineWidth',1.5,'Color',[0.86,0.25,0.18])
semilogx (time_test,max_V_P(4,:),'LineWidth',1.5,'Color','blue')
semilogx (time_test,max_V_P(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
semilogx (time_test,max_V_P(6,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
y_time_a = [min(min(max_V_P)) max(max(max_V_P))*1.2];
txt = {'\leftarrow start dyn.','     region'};
text(ref1.Test_rep_IAP(1,5),max(max(max_V_P))*1,txt,'Color','red','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
y_time_b = [min(min(max_V_P)) max(max(max_V_P))*1.15];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(ref1.Test_rep_IAP(1,6),max(max(max_V_P))*1.15,txt,'Color','red','Interpreter','tex')

x_time_a1 = [1e-1 1e-1];
y_time_a1 = [min(min(max_V_P)) max(max(max_V_P))*0.6];
x_time_a2 = [0.3162 0.3162];
y_time_a2 = [min(min(max_V_P)) max(max(max_V_P))*1];
x_time_a3 = [1e0 1e0];
y_time_a3 = [min(min(max_V_P)) max(max(max_V_P))*0.8];
x_time_a4 = [3.162 3.162];
y_time_a4 = [min(min(max_V_P)) max(max(max_V_P))*0.9];
x_time_a5 = [1e1 1e1];
y_time_a5 = [min(min(max_V_P)) max(max(max_V_P))*1];

line(x_time_a1,y_time_a1,'Color','green','LineStyle','--')
line(x_time_a2,y_time_a2,'Color',[0.86,0.25,0.18],'LineStyle','--')
line(x_time_a3,y_time_a3,'Color','blue','LineStyle','--')
line(x_time_a4,y_time_a4,'Color',[0.580, 0.188,1],'LineStyle','--')
line(x_time_a5,y_time_a5,'Color',[0.39,  0.86,0.90],'LineStyle','--')

txt = '0.1'; 
text(1e-1,max(max(max_V_P))*0.6,txt,'Color','green','Interpreter','tex')
txt = ' 0.32'; 
text(0.3162,max(max(max_V_P))*0.95,txt,'Color',[0.86,0.25,0.18],'Interpreter','tex')
txt = '1'; 
text(1e0,max(max(max_V_P))*0.8,txt,'Color','blue','Interpreter','tex')
txt = '3.2'; 
text(3.162,max(max(max_V_P))*0.9,txt,'Color',[0.580, 0.188,1],'Interpreter','tex')
txt = '10'; 
text(1e1,max(max(max_V_P))*1,txt,'Color',[0.39,  0.86,0.90],'Interpreter','tex')


grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','tex','Location','NorthWest');
%ylim([0 max(max(max_V_P))*1.60])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_TS_voltage_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)









end