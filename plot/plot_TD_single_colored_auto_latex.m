%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time dependent uncertainties impact of colored noise
% autocovariance
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_TD_single_colored_auto_latex(width,height,alw,fnt,fontlegend,time_test)

load(strcat(pwd,'\Results\Comparison_TD_test_type_8\Result_Test'))
ref1 = load(strcat(pwd,'\Results\Comparison_TD_test_type_0\Result_Test'));

%height= width/1.618;



for m = 1 : length(conf_test)
    unc(m).unc_I_P = unc(m).unc_I_P*100; %to convert it in crad
    unc(m).unc_V_P = unc(m).unc_V_P*100; %to convert it in crad
    for t = 1 : length(time_test)
        max_I_D(m,t) = max(unc(m).unc_I_D(:,t));
        max_I_Q(m,t) = max(unc(m).unc_I_Q(:,t));
        max_I_A(m,t) = max(unc(m).unc_I_A(:,t));
        max_I_P(m,t) = max(unc(m).unc_I_P(:,t));
        
        max_V_D(m,t) = max(unc(m).unc_V_D(:,t));
        max_V_Q(m,t) = max(unc(m).unc_V_Q(:,t));
        max_V_A(m,t) = max(unc(m).unc_V_A(:,t));
        max_V_P(m,t) = max(unc(m).unc_V_P(:,t));
    end
end

% t_in = 97;
% slope_V_A = (max_V_A(3:end,end) - max_V_A(3:end,t_in))./(time_test(end)-time_test(t_in));
% slope_V_P = (max_V_P(3:end,end) - max_V_P(3:end,t_in))./(time_test(end)-time_test(t_in));
% 
% slope_I_A = (max_I_A(3:end,end) - max_I_A(3:end,t_in))./(time_test(end)-time_test(t_in));
% slope_I_P = (max_I_P(3:end,end) - max_I_P(3:end,t_in))./(time_test(end)-time_test(t_in));
% slope_matrix = [slope_V_A,slope_I_A,slope_V_P,slope_I_P];
% % slope_V_A = num2str(slope_V_A,2)
% % slope_V_P = num2str(slope_V_P,2)
% % slope_I_A = num2str(slope_I_A,2)
% % slope_I_P = num2str(slope_I_P,2)
% num2str(slope_matrix,2)
%     Ts_x =      [1e6 1e0   1e0 1e0 1e0  1e0   1e0 1e0]; 
%     colored_x = [1     1     0 0.5 0.9 0.99 0.999   1]; 
%     filter_x =  [0     0     1   1   1    1     1   1];

S = sprintf('F = %0.3f*', [ST(1).colored,ST(2).colored,ST(3).colored,ST(4).colored] );
legend_x = regexp(S, '*', 'split');
legend_x = legend_x(1:end-1);

% amplitude and phase

figure
semilogx (time_test,max_I_A(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_I_A(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_I_A(3,:),'LineWidth',1.5,'Color','red')
semilogx (time_test,max_I_A(4,:),'LineWidth',1.5,'Color','blue')
%semilogx (time_test,max_I_A(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
%semilogx (time_test,max_I_A(8,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

% x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
% y_time_a = [min(min(max_I_A)) max(max(max_I_A))*1.2];
% txt = '\leftarrow start dyn. region'; 
% text(ref1.Test_rep_IAP(1,5),max(max(max_I_A))*1.15,txt,'Color','red','Interpreter','tex')
% line(x_time_a,y_time_a,'Color','red','LineStyle','--')
% x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
% y_time_b = [min(min(max_I_A)) max_I_A(1,end)*1.15];
% line(x_time_b,y_time_b,'Color','red','LineStyle','--')
% txt = '\leftarrow end dyn. region'; 
% text(ref1.Test_rep_IAP(1,6),max_I_A(1,end)*0.8,txt,'Color','red','Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
ylim([0 max(max(max_I_A))*1.60])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_current_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
semilogx (time_test,max_I_P(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_I_P(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_I_P(3,:),'LineWidth',1.5,'Color','red')
semilogx (time_test,max_I_P(4,:),'LineWidth',1.5,'Color','blue')
%semilogx (time_test,max_I_P(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
%semilogx (time_test,max_I_P(8,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

% x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
% y_time_a = [min(min(max_I_P)) max(max(max_I_P))*1.2];
% txt = '\leftarrow start dyn. region'; 
% text(ref1.Test_rep_IAP(1,5),max(max(max_I_P))*1.15,txt,'Color','red','Interpreter','tex')
% line(x_time_a,y_time_a,'Color','red','LineStyle','--')
% x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
% y_time_b = [min(min(max_I_P)) max_I_P(1,end)*1.15];
% line(x_time_b,y_time_b,'Color','red','LineStyle','--')
% txt = '\leftarrow end dyn. region'; 
% text(ref1.Test_rep_IAP(1,6),max_I_P(1,end)*0.8,txt,'Color','red','Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
ylim([0 max(max(max_I_P))*1.20])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_current_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage

% amplitude and phase

figure
semilogx (time_test,max_V_A(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_V_A(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_V_A(3,:),'LineWidth',1.5,'Color','red')
semilogx (time_test,max_V_A(4,:),'LineWidth',1.5,'Color','blue')
%semilogx (time_test,max_V_A(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
%semilogx (time_test,max_V_A(8,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

% x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
% y_time_a = [min(min(max_V_A)) max(max(max_V_A))*1.2];
% txt = '\leftarrow start dyn. region'; 
% text(ref1.Test_rep_IAP(1,5),max(max(max_V_A))*1.15,txt,'Color','red','Interpreter','tex')
% line(x_time_a,y_time_a,'Color','red','LineStyle','--')
% x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
% y_time_b = [min(min(max_V_A)) max_V_A(1,end)*1.15];
% line(x_time_b,y_time_b,'Color','red','LineStyle','--')
% txt = '\leftarrow end dyn. region'; 
% text(ref1.Test_rep_IAP(1,6),max_V_A(1,end)*0.8,txt,'Color','red','Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
%ylim([0 max(max(max_V_A))])
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_voltage_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




figure
semilogx (time_test,max_V_P(1,:),'LineWidth',1.5,'Color','black')
hold on
semilogx (time_test,max_V_P(2,:),'LineWidth',1.5,'Color','green')
semilogx (time_test,max_V_P(3,:),'LineWidth',1.5,'Color','red')
semilogx (time_test,max_V_P(4,:),'LineWidth',1.5,'Color','blue')
%semilogx (time_test,max_V_P(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
%semilogx (time_test,max_V_P(8,:),'LineWidth',1.5,'Color',[0.39,  0.86,0.90])

% x_time_a = [ref1.Test_rep_IAP(1,5) ref1.Test_rep_IAP(1,5)];
% y_time_a = [min(min(max_V_P)) max(max(max_V_P))*1.2];
% txt = '\leftarrow start dyn. region'; 
% text(ref1.Test_rep_IAP(1,5),max(max(max_V_P))*1.15,txt,'Color','red','Interpreter','tex')
% line(x_time_a,y_time_a,'Color','red','LineStyle','--')
% x_time_b = [ref1.Test_rep_IAP(1,6) ref1.Test_rep_IAP(1,6)];
% y_time_b = [min(min(max_V_P)) max_V_P(1,end)*1.15];
% line(x_time_b,y_time_b,'Color','red','LineStyle','--')
% txt = '\leftarrow end dyn. region'; 
% text(ref1.Test_rep_IAP(1,6),max_V_P(1,end)*0.8,txt,'Color','red','Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
ylim([0 max(max(max_V_P))*1.60])
save_case =  strcat(pwd,'\Results\fig_uncertainty_colored_voltage_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


% figure
% semilogx (time_test,max_I_D(1,:),'LineWidth',1.5,'Color',[0.39,0.86,0.90])
% hold on
% semilogx (time_test,max_I_D(2,:),'LineWidth',1.5,'Color','blue')
% semilogx (time_test,max_I_D(3,:),'LineWidth',1.5,'Color','red')
% semilogx (time_test,max_I_D(4,:),'LineWidth',1.5,'Color','black')
% semilogx (time_test,max_I_D(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
% 
% grid on
% xlim([time_test(1) time_test(end)])
% xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
% ylabel('Uncertainty D axis [A]','FontSize',fnt,'Interpreter','latex')
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% legend(legend_x)
% set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
% save_case =  strcat(pwd,'\fig_uncertainty_colored_current_D');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)
% 
% 
% 
% figure
% semilogx (time_test,max_I_Q(1,:),'LineWidth',1.5,'Color',[0.39,0.86,0.90])
% hold on
% semilogx (time_test,max_I_Q(2,:),'LineWidth',1.5,'Color','blue')
% semilogx (time_test,max_I_Q(3,:),'LineWidth',1.5,'Color','red')
% semilogx (time_test,max_I_Q(4,:),'LineWidth',1.5,'Color','black')
% semilogx (time_test,max_I_Q(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
% grid on
% xlim([time_test(1) time_test(end)])
% xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
% ylabel('Uncertainty Q axis [A]','FontSize',fnt,'Interpreter','latex')
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% legend(legend_x)
% set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
% save_case =  strcat(pwd,'\fig_uncertainty_colored_current_Q');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)


% figure
% semilogx (time_test,max_V_D(1,:),'LineWidth',1.5,'Color',[0.39,0.86,0.90])
% hold on
% semilogx (time_test,max_V_D(2,:),'LineWidth',1.5,'Color','blue')
% semilogx (time_test,max_V_D(3,:),'LineWidth',1.5,'Color','red')
% semilogx (time_test,max_V_D(4,:),'LineWidth',1.5,'Color','black')
% semilogx (time_test,max_V_D(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
% grid on
% xlim([time_test(1) time_test(end)])
% xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
% ylabel('Uncertainty D axis [V]','FontSize',fnt,'Interpreter','latex')
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% legend(legend_x)
% set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
% 
% save_case =  strcat(pwd,'\fig_uncertainty_colored_voltage_D');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)
% 
% 
% figure
% semilogx (time_test,max_V_Q(1,:),'LineWidth',1.5,'Color',[0.39,0.86,0.90])
% hold on
% semilogx (time_test,max_V_Q(2,:),'LineWidth',1.5,'Color','blue')
% semilogx (time_test,max_V_Q(3,:),'LineWidth',1.5,'Color','red')
% semilogx (time_test,max_V_Q(4,:),'LineWidth',1.5,'Color','black')
% semilogx (time_test,max_V_Q(5,:),'LineWidth',1.5,'Color',[0.580, 0.188,1])
% grid on
% xlim([time_test(1) time_test(end)])
% xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
% ylabel('Uncertainty Q axis [V]','FontSize',fnt,'Interpreter','latex')
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% legend(legend_x)
% set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthEast');
% 
% save_case =  strcat(pwd,'\fig_uncertainty_colored_voltage_Q');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)
end