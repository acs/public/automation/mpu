%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the behaviour over different grid and monitoring
% scenario of TD and TI uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_impact_color(Test_rep_I,Test_rep_V,Test_rep_IAP,Test_rep_VAP,poli1_I,poli1_V,poli1_IAP,poli1_VAP,x_axis,t_test,TI,TD,width,height,alw,fnt,fontlegend)




height= width/1.618;


if t_test == 1
    xtext = 'number of nodes [-]';
    xsave = 'node';
    xformat1 = '%.0f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 2
    xtext = 'number of lines [-]';
    xsave = 'line';
    xformat1 = '%.0f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 3
    xtext = 'Power Consumption [kVA]';
    xsave = 'Sn';
    xformat1 = '%.2f';
    xangle = -90;
    x_axis = x_axis/1000;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 4
    xtext = 'impedance absolute value [$\Omega$]';
    xsave = 'RL';
    xformat1 = '%.3f';
    xangle = -90;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 5
    xtext = 'ratio inverters [-]';
    xsave = 'PQRL';
    xformat1 = '%.3f';
    xangle = -90;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 6
    xtext = 'switching frequency [kHz]';
    xsave = 'fsw';
    xformat1 = '%.3f';
    xangle = -90;
    x_axis = x_axis/1000;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 8
    xtext = 'autocovariance [-]';
    xsave = 'auto';
    xformat1 = '%.3f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
    x_axis = [1:length(x_axis)];
elseif t_test == 9
    xtext = 'power factor [-]';
    xsave = 'PF';
    xformat1 = '%.2f';
    xangle = -90;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 10
    xtext = 'number of voltage devices [-]';
    xsave = 'vdevice';
    xformat1 = '%.0f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 11
    xtext = 'number of current devices [-]';
    xsave = 'cdevice';
    xformat1 = '%.0f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 12
    xtext = 'number of power injection devices [-]';
    xsave = 'PQdevice';
    xformat1 = '%.0f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 13
    xtext =  'number of power flow devices [-]';
    xsave = 'PQFdevice';
    xformat1 = '%.0f';
    xangle = 0;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 14
    xtext = 'device uncertainty [\%]';
    xsave = 'devacc';
    xformat1 = '%.1f';
    xangle = -90;
    x_axis = x_axis*100;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 15
    xtext = 'pseudo measurement uncertainty [\%]';
    xsave = 'pseudoacc';
    xformat1 = '%.0f';
    xangle = -90;
    x_axis = x_axis*100;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 16
    xtext = 'delay [s]';
    xsave = 'delay';
    xformat1 = '%.3f';
    xangle = -90;
    [x_axis_unique,~] = unique(x_axis);
    %x_axis = [1:length(x_axis)];
elseif t_test == 17
    xtext = 'std_dev';
    xsave = 'std_dev_noise';
    xformat1 = '%.1f';
    xangle = -90;
    [x_axis_unique,~] = unique(x_axis);
elseif t_test == 18
    xtext = 'delay [s]';
    xsave = 'delay';
    xformat1 = '%.3f';
    xangle = -90;
    [x_axis_unique,~] = unique(x_axis);
    %x_axis = [1:length(x_axis)];
elseif t_test == 23
    xtext = 'Power Consumption [kVA]';
    xsave = 'Sn_sign';
    xformat1 = '%.2f';
    xangle = -90;
    x_axis = x_axis/1000;
    [x_axis_unique,~] = unique(x_axis);
end

if TI == 1
    xtext2 = 'TI';
elseif TD == 1
    xtext2 = 'TD';
end



if TI == 1
% current A and P
f = figure;
set(f, 'Position', [855, 400, 450, 280])
plot(x_axis,Test_rep_IAP(:,1),'-s')
hold on
plot(x_axis,poli1_IAP(1,:),'--')
xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
xtickformat(xformat1);
xtickangle(xangle)
xlim([x_axis(1) x_axis(end)])
grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [A]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_I_amplitude_TI_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

f = figure;
set(f, 'Position', [855, 400, 450, 280])
plot(x_axis,Test_rep_IAP(:,2),'-s')
hold on
plot(x_axis,poli1_IAP(2,:),'--')
xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
xtickformat(xformat1);
xtickangle(xangle)
xlim([x_axis(1) x_axis(end)])
grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [crad]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_I_phase_TI_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)



% voltage A and P
f = figure;
set(f, 'Position', [855, 40, 450, 280])
plot(x_axis,Test_rep_VAP(:,1),'-s')
hold on
plot(x_axis,poli1_VAP(1,:),'--')
xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
xtickformat(xformat1);
xtickangle(xangle)
xlim([x_axis(1) x_axis(end)])
grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [V]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_V_amplitude_TI_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

f = figure;
set(f, 'Position', [855, 40, 450, 280])
plot(x_axis,Test_rep_VAP(:,2),'-s')
hold on
plot(x_axis,poli1_VAP(2,:),'--')
xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
xtickformat(xformat1);
xtickangle(xangle)
xlim([x_axis(1) x_axis(end)])
grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [crad]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_V_phase_TI_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)






else %TD case

%times voltage -current - AP initial
f = figure;
set(f, 'Position', [5, 400, 450, 280])
plot(x_axis,Test_rep_VAP(:,5),'-s')
hold on
plot(x_axis,poli1_VAP(5,:),'--')
xticks(x_axis); 
xtickangle(xangle)
xtickformat(xformat1);
xticklabels(x_axis_unique);


xlim([x_axis(1) x_axis(end)])
grid on
legend('samples','linear regression','Location','NorthEast');
ylim([0.98*min([Test_rep_VAP(:,5)',poli1_VAP(5,:)]) 1.02*max([Test_rep_VAP(:,5)',poli1_VAP(5,:)])])

grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('t_{in,din} [s]','FontSize',fnt,'Interpreter','tex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_time_initial_VI_AP_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

%times voltage-current AP final
f = figure;
set(f, 'Position', [5, 40, 450, 280])
set(f, 'Position', [5, 400, 450, 280])
plot(x_axis,Test_rep_VAP(:,6),'-s')
hold on
plot(x_axis,poli1_VAP(6,:),'--')

xticks(x_axis); 
xtickangle(xangle)
xtickformat(xformat1);
xticklabels(x_axis_unique);
xlim([x_axis(1) x_axis(end)])
grid on
legend('samples','linear regression','Location','NorthWest');

ylim([0.98*min([Test_rep_VAP(:,6)',poli1_VAP(6,:)]) 1.02*max([Test_rep_VAP(:,6)',poli1_VAP(6,:)])])

grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('t_{fin,din} [s]','FontSize',fnt,'Interpreter','tex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_time_final_VI_AP_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


% current A and P
% f = figure;
% set(f, 'Position', [855, 400, 450, 280])
% plot(x_axis,Test_rep_IAP(:,1),'-s')
% hold on
% plot(x_axis,poli1_IAP(1,:),'--')
% xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
% xtickformat(xformat1);
% xtickangle(xangle)
% xlim([x_axis(1) x_axis(end)])
% grid on
% xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
% ylabel('uncertainty [A]','FontSize',fnt,'Interpreter','latex')
% set(legend,'FontSize',fontlegend,'Interpreter','latex');
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% save_case =  strcat(pwd,'\Results\fig_unc_I_amplitude_TD_dyn_',xsave,'_MV');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)
% 
% f = figure;
% set(f, 'Position', [855, 400, 450, 280])
% plot(x_axis,Test_rep_IAP(:,2),'-s')
% hold on
% plot(x_axis,poli1_IAP(2,:),'--')
% xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
% xtickformat(xformat1);
% xtickangle(xangle)
% xlim([x_axis(1) x_axis(end)])
% grid on
% xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
% ylabel('uncertainty [crad]','FontSize',fnt,'Interpreter','latex')
% set(legend,'FontSize',fontlegend,'Interpreter','latex');
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% save_case =  strcat(pwd,'\Results\fig_unc_I_phase_TD_dyn_',xsave,'_MV');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)

f = figure;
set(f, 'Position', [855, 400, 450, 280])
plot(x_axis,Test_rep_IAP(:,3),'-s')
hold on
plot(x_axis,poli1_IAP(3,:),'--')
xticks(x_axis); 
xtickangle(xangle)
xtickformat(xformat1);
xticklabels(x_axis_unique);
xlim([x_axis(1) x_axis(end)])
legend('samples','linear regression','Location','NorthWest');
ylim([0.98*min([Test_rep_IAP(:,3)',poli1_IAP(3,:)]) 1.02*max([Test_rep_IAP(:,3)',poli1_IAP(3,:)])])

grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [A]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_I_amplitude_TD_dam_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

f = figure;
set(f, 'Position', [855, 400, 450, 280])
plot(x_axis,Test_rep_IAP(:,4),'-s')
hold on
plot(x_axis,poli1_IAP(4,:),'--')
xticks(x_axis); 
xtickangle(xangle)
xtickformat(xformat1);
xticklabels(x_axis_unique);
xlim([x_axis(1) x_axis(end)])
legend('samples','linear regression','Location','NorthWest');
%ylim([0.98*min([Test_rep_IAP(:,4)',poli1_IAP(4,:)]) 1.02*max([Test_rep_IAP(:,4)',poli1_IAP(4,:)])])

grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [crad]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_I_phase_TD_dam_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage A and P
% f = figure;
% set(f, 'Position', [855, 40, 450, 280])
% plot(x_axis,Test_rep_VAP(:,1),'-s')
% hold on
% plot(x_axis,poli1_VAP(1,:),'--')
% xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
% xtickformat(xformat1);
% xtickangle(xangle)
% xlim([x_axis(1) x_axis(end)])
% grid on
% xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
% ylabel('uncertainty [V]','FontSize',fnt,'Interpreter','latex')
% set(legend,'FontSize',fontlegend,'Interpreter','latex');
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% save_case =  strcat(pwd,'\Results\fig_unc_V_amplitude_TD_dyn_',xsave,'_MV');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)
% 
% f = figure;
% set(f, 'Position', [855, 40, 450, 280])
% plot(x_axis,Test_rep_VAP(:,2),'-s')
% hold on
% plot(x_axis,poli1_VAP(2,:),'--')
% xticks(x_axis_unique); xticklabels(x_axis_unique);legend('samples','linear regression','Location','NorthWest');xtickformat(xformat1);xtickangle(xangle)
% xtickformat(xformat1);
% xtickangle(xangle)
% xlim([x_axis(1) x_axis(end)])
% grid on
% xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
% ylabel('uncertainty [crad]','FontSize',fnt,'Interpreter','latex')
% set(legend,'FontSize',fontlegend,'Interpreter','latex');
% set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
% set(gca,'LineWidth', 0.5);
% set(findall(gcf,'type','text'),'FontSize',fnt);
% set(gcf, 'Units','centimeters');
% figrsz=get(gcf, 'Position');
% set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
% set(gcf,'PaperUnits', 'centimeters');
% set(gcf, 'PaperType', 'A5')
% papersize = get(gcf, 'PaperSize');
% left = (papersize(1)- width)/2;
% bottom = (papersize(2)- height)/2;
% myfiguresize = [left, bottom, width, height];
% set(gcf,'PaperPosition', myfiguresize);
% save_case =  strcat(pwd,'\Results\fig_unc_V_phase_TD_dyn_',xsave,'_MV');
% print(save_case,'-depsc');  %Print the file figure in eps
% savefig(save_case)

f = figure;
set(f, 'Position', [855, 40, 450, 280])
plot(x_axis,Test_rep_VAP(:,3),'-s')
hold on
plot(x_axis,poli1_VAP(3,:),'--')
%xticks(x_axis_unique); 
xtickangle(xangle)
xtickformat(xformat1);
xticklabels(x_axis_unique);
xlim([x_axis(1) x_axis(end)])
legend('samples','linear regression','Location','NorthWest');
%ylim([0.98*min([Test_rep_VAP(:,3)',poli1_VAP(3,:)]) 1.02*max([Test_rep_VAP(:,3)',poli1_VAP(3,:)])])

grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [V]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_V_amplitude_TD_dam_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)

f = figure;
set(f, 'Position', [855, 40, 450, 280])
plot(x_axis,Test_rep_VAP(:,4),'-s')
hold on
plot(x_axis,poli1_VAP(4,:),'--')
%xticks(x_axis_unique); 
xtickangle(xangle)
xtickformat(xformat1);
xticklabels(x_axis_unique);
xlim([x_axis(1) x_axis(end)])
legend('samples','linear regression','Location','NorthWest');
%ylim([0.98*min([Test_rep_VAP(:,4)',poli1_VAP(4,:)]) 1.02*max([Test_rep_VAP(:,4)',poli1_VAP(4,:)])])

grid on
xlabel(xtext,'FontSize',fnt,'Interpreter','latex')
ylabel('uncertainty [crad]','FontSize',fnt,'Interpreter','latex')
set(legend,'FontSize',fontlegend,'Interpreter','latex');
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_unc_V_phase_TD_dam_',xsave,'_MV');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)
    
end


end

