%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the histogram of the error distributions
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function[] =  plot_histogram_latex_2St(in_text,UncDM)

width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;

open(UncDM)
D = get(gca,'Children');
YData=get(D,'Ydata');
y_values = [YData{1,:};YData{2,:};YData{3,:};YData{4,:}];

diffD = (y_values(1,:) - y_values(3,:))./y_values(3,:);
diffQ = (y_values(2,:) - y_values(4,:))./y_values(4,:);
a = diffD(:);
b = diffQ(:);
c = [a;b];
xlim1 = 1.33*max(abs(c));
%

figure
histc = histogram(c,20,'FaceColor',[221, 64, 45] / 256,'Normalization','probability');
ylim1 = 1.33*max(histc.Values);
xlabel('e_{unc} [-]','FontSize',fnt,'Interpreter','tex')
ylabel('Probability [-]','FontSize',fnt,'Interpreter','latex')
xlim([-xlim1 xlim1]) 
ylim([0 ylim1]) 

set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\fig_histogram_error_',in_text);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)
end