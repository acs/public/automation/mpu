%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_3fig_variance(unc_estimated_i_lineD,unc_estimated_i_lineQ,unc_system_i_lineD,unc_system_i_lineQ,...
    unc_DSSE_emp_i_lineD,unc_DSSE_emp_i_lineQ,time_test,DM,save_dir)

f = figure;
%movegui(f,'southwest');
set(f, 'Position', [5, 400, 450, 280])
subplot(2,1,1)
semilogx (time_test(1:end),unc_estimated_i_lineD.^2,'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc estimated current real')
subplot(2,1,2)
semilogx (time_test(1:end),unc_estimated_i_lineQ.^2,'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc estimated current imaginary')
save_title = strcat(save_dir,'\unc_estimated_current');
saveas(f,save_title)
save_title = strcat(save_title,'.png');
saveas(f,save_title)

f = figure;
%movegui(f,'southeast');
set(f, 'Position', [5, 40, 450, 280])
subplot(2,1,1)
semilogx (time_test(1:end),(unc_DSSE_emp_i_lineD.^2 - unc_system_i_lineD.^2)./abs(unc_DSSE_emp_i_lineD.^2),'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation DSSE - MC current real')
subplot(2,1,2)
semilogx (time_test(1:end),(unc_DSSE_emp_i_lineQ.^2 - unc_system_i_lineQ.^2)./abs(unc_DSSE_emp_i_lineQ.^2),'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation DSSE - MC current imaginary')
save_title = strcat(save_dir,'\deviation_DSSE_MC_current');
saveas(f,save_title)
save_title = strcat(save_title,'.png');
saveas(f,save_title)

f = figure;
%movegui(f,'south');
set(f, 'Position', [425, 400, 450, 280])
subplot(2,1,1)
semilogx (time_test(1:end),unc_system_i_lineD.^2,'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc actual system current real')
subplot(2,1,2)
semilogx (time_test(1:end),unc_system_i_lineQ.^2,'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc actual system current imaginary')
save_title = strcat(save_dir,'\unc_actual_system_current');
saveas(f,save_title)
save_title = strcat(save_title,'.png');
saveas(f,save_title)

f = figure;
%movegui(f,'south');
set(f, 'Position', [425, 40, 450, 280])
subplot(2,1,1)
semilogx (time_test(1:end),(unc_system_i_lineD.^2 - unc_estimated_i_lineD.^2)./abs(unc_system_i_lineD.^2),'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation MC system - estimated current real')
subplot(2,1,2)
semilogx (time_test(1:end),(unc_system_i_lineQ.^2 - unc_estimated_i_lineQ.^2)./abs(unc_system_i_lineQ.^2),'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation MC system - estimated current imaginary')
save_title = strcat(save_dir,'\deviation_MC_system_estimated_current');
saveas(f,save_title)
save_title = strcat(save_title,'.png');
saveas(f,save_title)

f = figure;
%movegui(f,'southeast');
set(f, 'Position', [855, 400, 450, 280])
subplot(2,1,1)
semilogx (time_test(1:end),unc_DSSE_emp_i_lineD.^2,'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc DSSE MC line current real')
subplot(2,1,2)
semilogx (time_test(1:end),unc_DSSE_emp_i_lineQ.^2,'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc  DSSE MC current imaginary')
save_title = strcat(save_dir,'\unc_DSSE_MC_current');
saveas(f,save_title)
save_title = strcat(save_title,'.png');
saveas(f,save_title)

f = figure;
%movegui(f,'southeast');
set(f, 'Position', [855, 40, 450, 280])
subplot(2,1,1)
semilogx (time_test(1:end),(unc_DSSE_emp_i_lineD.^2 - unc_estimated_i_lineD.^2)./abs([unc_DSSE_emp_i_lineD.^2]),'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation DSSE - estimated current real')
subplot(2,1,2)
semilogx (time_test(1:end),(unc_DSSE_emp_i_lineQ.^2 - unc_estimated_i_lineQ.^2)./abs([unc_DSSE_emp_i_lineQ.^2]),'-s')
S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
C = regexp(S, '*', 'split');
%legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation DSSE - estimated current imaginary')
save_title = strcat(save_dir,'\deviation_DSSE_estimated_current');
saveas(f,save_title)
save_title = strcat(save_title,'.png');
saveas(f,save_title)

end

