%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the uncertainty
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_1fig_uncertainty(unc_estimated_i_lineD0,unc_estimated_i_lineD,unc_estimated_i_lineQ0,unc_estimated_i_lineQ,time_test,DM)

    f = figure;
    set(f, 'Position', [5, 400, 450, 280])
    subplot(2,1,1)
    semilogx (time_test(1:end),[unc_estimated_i_lineD0;unc_estimated_i_lineD],'-s')
    S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
    C = regexp(S, '*', 'split');
    legend(C{1:end-1},'Location', 'northwest')
    grid on
    title('uncertainty estimated line current real')
    subplot(2,1,2)
    semilogx (time_test(1:end),[unc_estimated_i_lineQ0;unc_estimated_i_lineQ],'-s')
    S = sprintf('line %d*', 1:DM.Nline+DM.PCC);
    C = regexp(S, '*', 'split');
    legend(C{1:end-1},'Location', 'northwest')
    grid on
    title('uncertainty estimated line current imaginary')

end

