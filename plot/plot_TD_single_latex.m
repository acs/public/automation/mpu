%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time dependent uncertainties impact on the base case
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_TD_single_latex(width,height,alw,fnt,fontlegend,time_test,text_in)

load(strcat(pwd,'\Results\',text_in,'_TD_test_type_0.1\Result_test'))

m=1;
unc(m).unc_I_P = unc(m).unc_I_P*100; %to convert it in crad
unc(m).unc_V_P = unc(m).unc_V_P*100; %to convert it in crad
% amplitude and phase

figure
semilogx (time_test,unc(m).unc_I_A')
hold on
semilogx (time_test,max(unc(m).unc_I_A)','LineWidth',1.5,'Color','black')


x_time_a = [Test_rep_IAP(1,5) Test_rep_IAP(1,5)];
y_time_a = [min(min(unc(m).unc_I_A)) max(max(unc(m).unc_I_A))*1.2];
x_time_a2 = [Test_rep_IAP(1,7) Test_rep_IAP(1,7)];
y_time_a2 = [min(min(unc(m).unc_I_A)) max(max(unc(m).unc_I_A))*1.1];
txt = '\leftarrow start dyn. region'; 
text(Test_rep_IAP(1,5),max(max(unc(m).unc_I_A))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated start dyn. region';
text(Test_rep_IAP(1,7),max(max(unc(m).unc_I_A))*1.05,txt,'Color','blue','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
line(x_time_a2,y_time_a2,'Color','blue','LineStyle','--')
x_time_b = [Test_rep_IAP(1,6) Test_rep_IAP(1,6)];
y_time_b = [min(min(unc(m).unc_I_A)) max(max(unc(m).unc_I_A))*1.2];
x_time_b2 = [Test_rep_IAP(1,8) Test_rep_IAP(1,8)*1.1];
y_time_b2 = [min(min(unc(m).unc_I_A)) max(max(unc(m).unc_I_A))*1.1];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
line(x_time_b2,y_time_b2,'Color','blue','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(Test_rep_IAP(1,6),max(max(unc(m).unc_I_A))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated end dyn. region';
text(Test_rep_IAP(1,8),max(max(unc(m).unc_I_A))*1.05,txt,'Color','blue','Interpreter','tex')


grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
ylim([0 max(max(unc(m).unc_I_A))*1.25])
save_case =  strcat(pwd,'\Results\fig_uncertainty_white_current_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
semilogx (time_test,unc(m).unc_I_P')
hold on
semilogx (time_test,max(unc(m).unc_I_P)','LineWidth',1.5,'Color','black')

x_time_a = [Test_rep_IAP(1,5) Test_rep_IAP(1,5)];
y_time_a = [min(min(unc(m).unc_I_P)) max(max(unc(m).unc_I_P))*1.2];
x_time_a2 = [Test_rep_IAP(1,7) Test_rep_IAP(1,7)];
y_time_a2 = [min(min(unc(m).unc_I_P)) max(max(unc(m).unc_I_P))*1.1];
txt = '\leftarrow start dyn. region'; 
text(Test_rep_IAP(1,5),max(max(unc(m).unc_I_P))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated start dyn. region';
text(Test_rep_IAP(1,7),max(max(unc(m).unc_I_P))*1.05,txt,'Color','blue','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
line(x_time_a2,y_time_a2,'Color','blue','LineStyle','--')
x_time_b = [Test_rep_IAP(1,6) Test_rep_IAP(1,6)];
y_time_b = [min(min(unc(m).unc_I_P)) max(max(unc(m).unc_I_P))*1.2];
x_time_b2 = [Test_rep_IAP(1,8) Test_rep_IAP(1,8)*1.1];
y_time_b2 = [min(min(unc(m).unc_I_P)) max(max(unc(m).unc_I_P))*1.1];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
line(x_time_b2,y_time_b2,'Color','blue','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(Test_rep_IAP(1,6),max(max(unc(m).unc_I_P))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated end dyn. region';
text(Test_rep_IAP(1,8),max(max(unc(m).unc_I_P))*1.05,txt,'Color','blue','Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
ylim([0 max(max(unc(m).unc_I_P))*1.25])
save_case =  strcat(pwd,'\Results\fig_uncertainty_white_current_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage
% % amplitude and phase

figure
semilogx (time_test,unc(m).unc_V_A')
hold on
semilogx (time_test,max(unc(m).unc_V_A)','LineWidth',1.5,'Color','black')

x_time_a = [Test_rep_IAP(1,5) Test_rep_IAP(1,5)];
y_time_a = [min(min(unc(m).unc_V_A)) max(max(unc(m).unc_V_A))*1.2];
x_time_a2 = [Test_rep_IAP(1,7) Test_rep_IAP(1,7)];
y_time_a2 = [min(min(unc(m).unc_V_A)) max(max(unc(m).unc_V_A))*1.1];
txt = '\leftarrow start dyn. region'; 
text(Test_rep_IAP(1,5),max(max(unc(m).unc_V_A))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated start dyn. region';
text(Test_rep_IAP(1,7),max(max(unc(m).unc_V_A))*1.05,txt,'Color','blue','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
line(x_time_a2,y_time_a2,'Color','blue','LineStyle','--')
x_time_b = [Test_rep_IAP(1,6) Test_rep_IAP(1,6)];
y_time_b = [min(min(unc(m).unc_V_A)) max(max(unc(m).unc_V_A))*1.2];
x_time_b2 = [Test_rep_IAP(1,8) Test_rep_IAP(1,8)*1.1];
y_time_b2 = [min(min(unc(m).unc_V_A)) max(max(unc(m).unc_V_A))*1.1];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
line(x_time_b2,y_time_b2,'Color','blue','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(Test_rep_IAP(1,6),max(max(unc(m).unc_V_A))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated end dyn. region';
text(Test_rep_IAP(1,8),max(max(unc(m).unc_V_A))*1.05,txt,'Color','blue','Interpreter','tex')


grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
ylim([0 max(max(unc(m).unc_V_A))*1.25])
save_case =  strcat(pwd,'\Results\fig_uncertainty_white_voltage_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
semilogx (time_test,unc(m).unc_V_P')
hold on
semilogx (time_test,max(unc(m).unc_V_P)','LineWidth',1.5,'Color','black')

x_time_a = [Test_rep_IAP(1,5) Test_rep_IAP(1,5)];
y_time_a = [min(min(unc(m).unc_V_P)) max(max(unc(m).unc_V_P))*1.2];
x_time_a2 = [Test_rep_IAP(1,7) Test_rep_IAP(1,7)];
y_time_a2 = [min(min(unc(m).unc_V_P)) max(max(unc(m).unc_V_P))*1.1];
txt = '\leftarrow start dyn. region'; 
text(Test_rep_IAP(1,5),max(max(unc(m).unc_V_P))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated start dyn. region';
text(Test_rep_IAP(1,7),max(max(unc(m).unc_V_P))*1.05,txt,'Color','blue','Interpreter','tex')
line(x_time_a,y_time_a,'Color','red','LineStyle','--')
line(x_time_a2,y_time_a2,'Color','blue','LineStyle','--')
x_time_b = [Test_rep_IAP(1,6) Test_rep_IAP(1,6)];
y_time_b = [min(min(unc(m).unc_V_P)) max(max(unc(m).unc_V_P))*1.2];
x_time_b2 = [Test_rep_IAP(1,8) Test_rep_IAP(1,8)*1.1];
y_time_b2 = [min(min(unc(m).unc_V_P)) max(max(unc(m).unc_V_P))*1.1];
line(x_time_b,y_time_b,'Color','red','LineStyle','--')
line(x_time_b2,y_time_b2,'Color','blue','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
text(Test_rep_IAP(1,6),max(max(unc(m).unc_V_P))*1.15,txt,'Color','red','Interpreter','tex')
txt = '\leftarrow estimated end dyn. region';
text(Test_rep_IAP(1,8),max(max(unc(m).unc_V_P))*1.05,txt,'Color','blue','Interpreter','tex')

grid on
xlim([time_test(1) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
ylim([0 max(max(unc(m).unc_V_P))*1.25])
save_case =  strcat(pwd,'\Results\fig_uncertainty_white_voltage_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)



end