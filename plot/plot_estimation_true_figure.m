%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot estimated and actual values of the state over time
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_estimation_true_figure(DM,GridData,results_DSSE,time_test)
f=figure;
set(f, 'Position', [5, 400, 450, 280])
col1 = hsv(GridData.Lines_num);
subplot(2,1,1)
if DM.PCC == 1
plot(time_test(1:end),results_DSSE.Ireal_true(:,1),'--*','color',col1(1,:))
hold on
plot(time_test(1:end),results_DSSE.Ireal_status(:,1),'--o','color',col1(1,:) )
S = sprintf('true node %d*', 0);
C = regexp(S, '*', 'split');
C = C(1:end-1);
S = sprintf('estimated node %d*', 0);
C = [C,regexp(S, '*', 'split')];
C = C(1:end-1);
end
for  x = 1 + DM.PCC : GridData.Lines_num
plot(time_test(1:end),results_DSSE.Ireal_true(:,x),'--*','color',col1(x,:))
plot(time_test(1:end),results_DSSE.Ireal_status(:,x),'--o','color',col1(x,:))
S = sprintf('true node %d*', x);
C = [C,regexp(S, '*', 'split')];
C = C(1:end-1);
S = sprintf('estimated node %d*', x);
C = [C,regexp(S, '*', 'split')];
C = C(1:end-1);
end
legend(C)
grid on
title('current real')

subplot(2,1,2)
if DM.PCC == 1
plot(time_test(1:end),results_DSSE.Iimag_true(:,1),'--*','color',col1(1,:))
hold on
plot(time_test(1:end),results_DSSE.Iimag_status(:,1),'--o','color',col1(1,:))
S = sprintf('true node %d*', 0);
C = regexp(S, '*', 'split');
C = C(1:end-1);
S = sprintf('estimated node %d*', 0);
C = [C,regexp(S, '*', 'split')];
C = C(1:end-1);
end
for  x = 1 + DM.PCC : GridData.Lines_num
plot(time_test(1:end),results_DSSE.Iimag_true(:,x),'--*','color',col1(x,:))   
plot(time_test(1:end),results_DSSE.Iimag_status(:,x),'--o','color',col1(x,:))   
S = sprintf('true node %d*', x);
C = [C,regexp(S, '*', 'split')];
C = C(1:end-1);
S = sprintf('estimated node %d*', x);
C = [C,regexp(S, '*', 'split')];
C = C(1:end-1);
end
legend(C)
grid on
title('current imaginary')

end
