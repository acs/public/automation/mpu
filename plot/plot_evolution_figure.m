%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the evolution of the state over time
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_evolution_figure(vbD,vbQ, ioD,ioQ,i_lineA,i_lineP,i_lineA0,i_lineP0,time_test,Nnode,Nline,PCC,plac_inv_tot,plac_load_tot)
% function to plot the evolution of the states over time
n_inj = 0;
for b = 1 : Nnode
    if plac_inv_tot(b) ~= 0 ||  plac_load_tot(b) ~= 0
        n_inj = n_inj+1;
        p_inj(n_inj)=n_inj;
    end
    ioA(b,:) = sqrt(ioD(b,:).^2 + ioQ(b,:).^2);
    ioP(b,:) = phase(ioD(b,:) +1i* ioQ(b,:));
    Po(b,:) = 1.5*real((ioD(b,:) - 1i*ioQ(b,:)).*(vbD(b,:)+1i*vbQ(b,:)));
    Qo(b,:) = 1.5*imag((ioD(b,:) - 1i*ioQ(b,:)).*(vbD(b,:)+1i*vbQ(b,:)));
end

f=figure;
movegui(f,'northwest');
subplot(2,1,1)
plot(time_test(1:end),vbD(p_inj,:))
S = sprintf('node %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
grid on
title('time voltage D axis')
subplot(2,1,2)
plot(time_test(1:end),vbQ(p_inj,:))
S = sprintf('node %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
title('time voltage Q axis')
grid on

f=figure;
movegui(f,'northwest');
plot(time_test(1:end),sqrt(vbD(p_inj,:).^2+vbQ(p_inj,:).^2))
S = sprintf('node %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
grid on
title('time voltage amplitude')


f=figure;
movegui(f,'north');
subplot(2,1,1)
plot(time_test(1:end),ioA(p_inj,:))
S = sprintf('inj %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
grid on
title('time inverter current amplitude')
subplot(2,1,2)
plot(time_test(1:end),wrapToPi(ioP(p_inj,:)))
S = sprintf('inj %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
title('time inverter current phase')
grid on


f=figure;
movegui(f,'northeast');
subplot(2,1,1)
plot(time_test(1:end),Po(p_inj,:))
S = sprintf('inj %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
grid on
title('time inverter active power')
subplot(2,1,2)
plot(time_test(1:end),Qo(p_inj,:))
S = sprintf('inj %d*', 1:n_inj);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
title('time inverter reactive power')
grid on

f=figure;
movegui(f,'southwest');
subplot(2,1,1)
plot(time_test(1:end),i_lineA)
S = sprintf('line %d*', 1:Nline);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
grid on
title('time line current magnitude')
subplot(2,1,2)
plot(time_test(1:end),wrapToPi(i_lineP))
S = sprintf('line %d*', 1:Nline);
C = regexp(S, '*', 'split');
legend(C{1:end-1})
grid on
title('time line current phase')

if PCC == 1
    f=figure;
    movegui(f,'south');
    subplot(2,1,1)
    plot(time_test(1:end),i_lineA0)
    grid on
    title('time line current branch 0 magnitude')
    subplot(2,1,2)
    plot(time_test(1:end),i_lineP0)
    grid on
    title('time line current branch 0  phase')
end
end