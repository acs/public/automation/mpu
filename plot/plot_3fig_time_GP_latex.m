%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program to plot the uncertainties of the system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_3fig_time_GP_latex(x_status_t,x_status_est_t,time_delta,Nw,script_in)

width= 12.4;
height= 5;
alw = 0.5;
fnt=9;
fontlegend=7;


for x = 1 : 1:size(x_status_t,1)
    figure;
    plot(time_delta*[1:Nw],x_status_t(x,:),'LineWidth',2,'Color',[0.39,0.86,0.90])
    S = sprintf('actual status %d*',x);
    %C = regexp(S, '*', 'split');
    hold on
    plot(time_delta*[1:Nw], x_status_est_t(x,:),'--s','LineWidth',1,'MarkerSize',5,'Color',[0.86,0.25,0.18])
    S = [S,sprintf('estimated status %d*',x)];
    C = regexp(S, '*', 'split');
    legend(C{1:end-1},'Location', 'Best')
    grid on
    %title('deviation SE - MC uncertainty')
    xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
    ylabel('status [-]','FontSize',fnt,'Interpreter','latex')
    set(legend,'FontSize',fontlegend,'Interpreter','latex');
    set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
    set(gca,'LineWidth', 0.5);
    set(findall(gcf,'type','text'),'FontSize',fnt);
    set(gcf, 'Units','centimeters');
    figrsz=get(gcf, 'Position');
    set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
    set(gcf,'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A5')
    papersize = get(gcf, 'PaperSize');
    left = (papersize(1)- width)/2;
    bottom = (papersize(2)- height)/2;
    myfiguresize = [left, bottom, width, height];
    set(gcf,'PaperPosition', myfiguresize);
    save_case =  strcat(pwd,'\fig_actual_est_states_type_unc',script_in,'_stat',num2str(x));
    print(save_case,'-depsc');  %Print the file figure in eps
    savefig(save_case)
    
    
    figure
    %movegui(f,'southwest');
    %set(f, 'Position', [425, 400, 450, 280])
    plot(time_delta*[1:Nw],(x_status_t(x,:)-x_status_est_t(x,:))./abs(x_status_t(x,:)))
    S = sprintf('status %d*', x);
    C = regexp(S, '*', 'split');
    legend(C{1:end-1},'Location', 'northeast')
    grid on
    %title('unc test model with MC')
    xlabel('time [s]','FontSize',fnt,'Interpreter','latex')
    ylabel('relative error status [-]','FontSize',fnt,'Interpreter','latex')
    set(legend,'FontSize',fontlegend,'Interpreter','latex');
    set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
    set(gca,'LineWidth', 0.5);
    set(findall(gcf,'type','text'),'FontSize',fnt);
    set(gcf, 'Units','centimeters');
    figrsz=get(gcf, 'Position');
    set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
    set(gcf,'PaperUnits', 'centimeters');
    set(gcf, 'PaperType', 'A5')
    papersize = get(gcf, 'PaperSize');
    left = (papersize(1)- width)/2;
    bottom = (papersize(2)- height)/2;
    myfiguresize = [left, bottom, width, height];
    set(gcf,'PaperPosition', myfiguresize);
    save_case =  strcat(pwd,'\fig_error_states',script_in,'_stat',num2str(x));
    print(save_case,'-depsc');  %Print the file figure in eps
    savefig(save_case)
    
end


end

