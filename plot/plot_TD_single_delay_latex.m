%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time dependent uncertainties impact of delay
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_TD_single_delay_latex(width,height,alw,fnt,fontlegend,time_test,text_in)

load(strcat(pwd,'\Results\',text_in,'_TD_test_type_16\Result_test'))
in = 9;

%height= width/1.618;

for m = 1 : length(conf_test)
    unc(m).unc_I_P = unc(m).unc_I_P*100; %to convert it in crad
    unc(m).unc_V_P = unc(m).unc_V_P*100; %to convert it in crad
    for t = in : length(time_test)
        max_I_D(m,t) = max(unc(m).unc_I_D(:,t));
        max_I_Q(m,t) = max(unc(m).unc_I_Q(:,t));
        max_I_A(m,t) = max(unc(m).unc_I_A(:,t));
        max_I_P(m,t) = max(unc(m).unc_I_P(:,t));
        
        max_V_D(m,t) = max(unc(m).unc_V_D(:,t));
        max_V_Q(m,t) = max(unc(m).unc_V_Q(:,t));
        max_V_A(m,t) = max(unc(m).unc_V_A(:,t));
        max_V_P(m,t) = max(unc(m).unc_V_P(:,t));
    end
end

S = sprintf('delay = %.2f s*', ST.delay);
legend_x = regexp(S, '*', 'split');
%legend_x = legend_x(1:end-1);
%legend_x = legend_x([1,4,5,6,7]);
legend_x{1}='delay = 0 s';

% current
% amplitude and phase

figure
semilogx (time_test(in:end),max_I_A(1,in:end),'LineWidth',1.5,'Color','black')
hold on
%semilogx (time_test(in:end),max_I_A(2,in:end),'LineWidth',1.5,'Color','green')
%semilogx (time_test(in:end),max_I_A(3,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_I_A(2,in:end),'LineWidth',1.5,'Color','green')
semilogx (time_test(in:end),max_I_A(3,in:end),'LineWidth',1.5,'Color','blue')
semilogx (time_test(in:end),max_I_A(4,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_I_A(5,in:end),'LineWidth',1.5,'Color',[0.501, 0,0])
%semilogx (time_test(in:end),max_I_A(8,in:end),'LineWidth',1.5,'Color',[0.701, 0,0])

x_time_a = [Test_rep_IAP(1,5) Test_rep_IAP(1,5)];
y_time_a = [min(min(max_I_A)) max(max(max_I_A))*1.2];
txt = '\leftarrow start dyn. region'; 
%text(Test_rep_IAP(1,5),max(max(max_I_A))*1.15,txt,'Color','red','Interpreter','tex')
%line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [Test_rep_IAP(1,6) Test_rep_IAP(1,6)];
y_time_b = [min(min(max_I_A)) max(max(max_I_A))*1.2];
%line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
%text(Test_rep_IAP(1,6),max(max(max_I_A))*1.15,txt,'Color','red','Interpreter','tex')
grid on
xlim([time_test(in) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz = get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
ylim([0 max(max(max_I_A))*1.1])

set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthWest');
save_case =  strcat(pwd,'\Results\fig_uncertainty_delay_current_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)





figure
semilogx (time_test(in:end),max_I_P(1,in:end),'LineWidth',1.5,'Color','black')
hold on
%semilogx (time_test(in:end),max_I_P(2,in:end),'LineWidth',1.5,'Color','green')
%semilogx (time_test(in:end),max_I_P(3,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_I_P(2,in:end),'LineWidth',1.5,'Color','green')
semilogx (time_test(in:end),max_I_P(3,in:end),'LineWidth',1.5,'Color','blue')
semilogx (time_test(in:end),max_I_P(4,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_I_P(5,in:end),'LineWidth',1.5,'Color',[0.501, 0,0])
%semilogx (time_test(in:end),max_I_P(8,in:end),'LineWidth',1.5,'Color',[0.701, 0,0]) 

x_time_a = [Test_rep_IAP(1,5) Test_rep_IAP(1,5)];
y_time_a = [min(min(max_I_P)) max(max(max_I_P))*1.2];
txt = '\leftarrow start dyn. region'; 
%line(Test_rep_IAP(1,5),max(max(max_I_P))*1.15,txt,'Color','red','Interpreter','tex')
%line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [Test_rep_IAP(1,6) Test_rep_IAP(1,6)];
y_time_b = [min(min(max_I_P)) max(max(max_I_P))*1.2];
%line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
%line(Test_rep_IAP(1,6),max(max(max_I_P))*1.15,txt,'Color','red','Interpreter','tex')
grid on
xlim([time_test(in) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
ylim([0 max(max(max_I_P))*1.25])
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthWest');
save_case =  strcat(pwd,'\Results\fig_uncertainty_delay_current_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage
% amplitude and phase

figure
semilogx (time_test(in:end),max_V_A(1,in:end),'LineWidth',1.5,'Color','black')
hold on
%semilogx (time_test(in:end),max_V_A(2,in:end),'LineWidth',1.5,'Color','green')
%semilogx (time_test(in:end),max_V_A(3,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_V_A(2,in:end),'LineWidth',1.5,'Color','green')
semilogx (time_test(in:end),max_V_A(3,in:end),'LineWidth',1.5,'Color','blue')
semilogx (time_test(in:end),max_V_A(4,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_V_A(5,in:end),'LineWidth',1.5,'Color',[0.501, 0,0])
%semilogx (time_test(in:end),max_V_A(8,in:end),'LineWidth',1.5,'Color',[0.701, 0,0]) 

x_time_a = [Test_rep_VAP(1,5) Test_rep_VAP(1,5)];
y_time_a = [min(min(max_V_A)) max(max(max_V_A))*1.2];
txt = '\leftarrow start dyn. region'; 
%line(Test_rep_VAP(1,5),max(max(max_V_A))*1.15,txt,'Color','red','Interpreter','tex')
%line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [Test_rep_VAP(1,6) Test_rep_VAP(1,6)];
y_time_b = [min(min(max_V_A)) max(max(max_V_A))*1.2];
%line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
%line(Test_rep_VAP(1,6),max(max(max_V_A))*1.15,txt,'Color','red','Interpreter','tex')
grid on
xlim([time_test(in) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
ylim([0 max(max(max_V_A))*1.25])
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthWest');
save_case =  strcat(pwd,'\Results\fig_uncertainty_delay_voltage_A');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)






figure
semilogx (time_test(in:end),max_V_P(1,in:end),'LineWidth',1.5,'Color','black')
hold on
%semilogx (time_test(in:end),max_V_P(2,in:end),'LineWidth',1.5,'Color','green')
%semilogx (time_test(in:end),max_V_P(3,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_V_P(2,in:end),'LineWidth',1.5,'Color','green')
semilogx (time_test(in:end),max_V_P(3,in:end),'LineWidth',1.5,'Color','blue')
semilogx (time_test(in:end),max_V_P(4,in:end),'LineWidth',1.5,'Color','red')
semilogx (time_test(in:end),max_V_P(5,in:end),'LineWidth',1.5,'Color',[0.501, 0,0])
% semilogx (time_test(in:end),max_V_P(8,in:end),'LineWidth',1.5,'Color',[0.701, 0,0]) 

x_time_a = [Test_rep_VAP(1,5) Test_rep_VAP(1,5)];
y_time_a = [min(min(max_V_P)) max(max(max_V_P))*1.2];
txt = '\leftarrow start dyn. region'; 
%line(Test_rep_VAP(1,5),max(max(max_V_P))*1.15,txt,'Color','red','Interpreter','tex')
%line(x_time_a,y_time_a,'Color','red','LineStyle','--')
x_time_b = [Test_rep_VAP(1,6) Test_rep_VAP(1,6)];
y_time_b = [min(min(max_V_P)) max(max(max_V_P))*1.2];
%line(x_time_b,y_time_b,'Color','red','LineStyle','--')
txt = '\leftarrow end dyn. region'; 
%line(Test_rep_VAP(1,6),max(max(max_V_P))*1.15,txt,'Color','red','Interpreter','tex')
grid on
xlim([time_test(in) time_test(end)])
xlabel('time estimation window [s]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
legend(legend_x)
ylim([0 max(max(max_V_P))*1.25])
set(legend,'FontSize',fontlegend,'Interpreter','latex','Location','NorthWest');
save_case =  strcat(pwd,'\Results\fig_uncertainty_delay_voltage_P');
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


end