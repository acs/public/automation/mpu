%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program to plot the uncertainties of the system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_3fig_uncertainty_GP(unc_estimated,unc_system,unc_SE,time_test)

f = figure;
%movegui(f,'southwest');
set(f, 'Position', [5, 400, 450, 280])
semilogx (time_test(1:end),unc_estimated,'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc estimated model')


f = figure;
%movegui(f,'southeast');
set(f, 'Position', [5, 40, 450, 280])
%semilogx (time_test(1:end),(unc_SE - unc_system)./unc_SE ,'-s')
semilogx (time_test(1:end),(unc_SE - unc_system) ,'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation SE - MC uncertainty')

f = figure;
%movegui(f,'southwest');
set(f, 'Position', [425, 400, 450, 280])
semilogx (time_test(1:end),unc_system,'-s')
S = sprintf('status %d*', 1:size(unc_system,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc test model with MC')

f = figure;
%movegui(f,'southeast');
set(f, 'Position', [425, 40, 450, 280])
%semilogx (time_test(1:end),(unc_system - unc_estimated)./unc_system ,'-s')
semilogx (time_test(1:end),(unc_system - unc_estimated) ,'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation MC - estimated uncertainty')

f = figure;
%movegui(f,'southwest');
set(f, 'Position', [855, 400, 450, 280])
semilogx (time_test(1:end),unc_SE,'-s')
S = sprintf('status %d*', 1:size(unc_SE,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northwest')
grid on
title('unc test model with MC and SE')

f = figure;
%movegui(f,'southeast');
set(f, 'Position', [855, 40, 450, 280])
%semilogx (time_test(1:end),(unc_SE - unc_estimated)./unc_SE ,'-s')
semilogx (time_test(1:end),(unc_SE - unc_estimated),'-s')
S = sprintf('status %d*', 1:size(unc_estimated,1));
C = regexp(S, '*', 'split');
legend(C{1:end-1},'Location', 'northwest')
grid on
title('deviation SE - estimated uncertainty')

end

