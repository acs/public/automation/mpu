%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to print uncertainty tables
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function print_uncertainty_table(UncDM,type_unc)
%(unc_estimated_i_lineD,unc_estimated_i_lineQ,unc_system_i_lineD,unc_system_i_lineQ,...
%    unc_DSSE_emp_i_lineD,unc_DSSE_emp_i_lineQ,time_test,DM,save_dir)

display('Uncertainty D axis')
text_print = cell(size(UncDM.unc_estimated_i_lineD,1),1);
if type_unc == 0
    Ntot = 1;
else
    Ntot = size(UncDM.time_test,2);
end

for x = 1 :size(UncDM.unc_estimated_i_lineD,1)
    add_text = strcat(num2str(x),{' '},'&',{' '});
    text_print{x,1} = strcat(text_print{x,1},add_text);
    for t = 1 : Ntot
        add_text = strcat(num2str(UncDM.unc_estimated_i_lineD(x,t),3),{' '},'&',{' '});
        text_print{x,1} = strcat(text_print{x,1},add_text);
        
        if t <  Ntot
            add_text = strcat(num2str(UncDM.unc_DSSE_emp_i_lineD(x,t),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
        else
            add_text = strcat(num2str(UncDM.unc_DSSE_emp_i_lineD(x,t),3),{' '},'\\');
            text_print{x,1} = strcat(text_print{x,1},add_text);
        end
    end
    disp(text_print{x})
end


display('Uncertainty Q axis')
text_print = cell(size(UncDM.unc_estimated_i_lineD,1),1);

for x = 1 :size(UncDM.unc_estimated_i_lineQ,1)
    add_text = strcat(num2str(x),{' '},'&',{' '});
    text_print{x,1} = strcat(text_print{x,1},add_text);
    for t = 1 : Ntot
        add_text = strcat(num2str(UncDM.unc_estimated_i_lineQ(x,t),3),{' '},'&',{' '});
        text_print{x,1} = strcat(text_print{x,1},add_text);
        
        if t <  Ntot
            add_text = strcat(num2str(UncDM.unc_DSSE_emp_i_lineQ(x,t),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
        else
            add_text = strcat(num2str(UncDM.unc_DSSE_emp_i_lineQ(x,t),3),{' '},'\\');
            text_print{x,1} = strcat(text_print{x,1},add_text);
        end
    end
    disp(text_print{x})
end

display('Uncertainty D+Q axis estimated')

text_print = cell(size(UncDM.unc_estimated_i_lineD,1),1);


    for x = 1 :size(UncDM.unc_estimated_i_lineD,1)
        add_text = strcat(num2str(x),{' '},'&',{' '});
        text_print{x,1} = strcat(text_print{x,1},add_text);
        for t = 1 : Ntot
            add_text = strcat(num2str(UncDM.unc_estimated_i_lineD(x,t),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
        end
    end
    
    for x = 1 :size(UncDM.unc_estimated_i_lineD,1)
        for t = 1 : Ntot
            if t <  Ntot
                add_text = strcat(num2str(UncDM.unc_estimated_i_lineQ(x,t),3),{' '},'&',{' '});
                text_print{x,1} = strcat(text_print{x,1},add_text);
            else
                add_text = strcat(num2str(UncDM.unc_estimated_i_lineQ(x,t),3),{' '},'\\');
                text_print{x,1} = strcat(text_print{x,1},add_text);
            end
        end
        disp(text_print{x})
    end
    
    


end

