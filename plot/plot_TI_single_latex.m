%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to plot the time independent uncertainties impact on the base
% case
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = plot_TI_single_latex(width,height,alw,fnt,fontlegend,text_in)

load(strcat(pwd,'\Results\',text_in,'_TI_test_type_0\Result_test'))

m = 1;
x_axis_unique= [1:ST(m).Nnode];


max_Vmagn = max(max(unc(m).unc_V_A))
max_Vph = max(max(unc(m).unc_V_P))
max_Imagn = max(max(unc(m).unc_I_A))
max_Iph = max(max(unc(m).unc_I_P))

% amplitude and phase

figure
plot ([0:ST(m).Nnode-1],unc(m).unc_I_A)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [A]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_uncertainty_current_A_TI_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
plot ([0:ST(m).Nnode-1],100*unc(m).unc_I_P)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode-1])
grid on
xlabel('branch [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_uncertainty_current_P_TI_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)




% voltage
x_axis_unique= [1:16];

% amplitude and phase

figure
plot ([0:ST(m).Nnode],unc(m).unc_V_A)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty amplitude [V]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_uncertainty_voltage_A_TI_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


figure
plot ([0:ST(m).Nnode],100*unc(m).unc_V_P)
xticks(x_axis_unique); xticklabels(x_axis_unique);

xlim([0 ST(m).Nnode])
grid on
xlabel('node [-]','FontSize',fnt,'Interpreter','latex')
ylabel('Uncertainty phase [crad]','FontSize',fnt,'Interpreter','latex')
set(gca,'FontSize',fnt,'TickLabelInterpreter', 'latex');
set(gca,'LineWidth', 0.5);
set(findall(gcf,'type','text'),'FontSize',fnt);
set(gcf, 'Units','centimeters');
figrsz=get(gcf, 'Position');
set(gcf, 'Position', [figrsz(1) figrsz(2) width height]);
set(gcf,'PaperUnits', 'centimeters');
set(gcf, 'PaperType', 'A5')
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);
save_case =  strcat(pwd,'\Results\fig_uncertainty_voltage_P_TI_',text_in);
print(save_case,'-depsc');  %Print the file figure in eps
savefig(save_case)


end