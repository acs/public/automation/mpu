%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to plot the uncertainty and histograms 
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clc
% clear all
% close all

type_unc_x = type_unc(m);
% type_unc_x = [0,0.1,0.2,2,2.1,3,3.1,6,7,8,8.1,9,9.1];
% type_unc_x = [2.1];

for N_conf = 1 : length(type_unc_x)
    if type_unc_x(N_conf) == 0
        in_text = strcat('SES')
    elseif type_unc_x(N_conf) == 0.1
        in_text = strcat('SES_v')
    elseif type_unc_x(N_conf) == 0.1
        in_text = strcat('SES_VI')
    elseif type_unc_x(N_conf) == 2
        in_text = strcat('SED')
    elseif type_unc_x(N_conf) == 2.1
        in_text = strcat('colorSED')
    elseif type_unc_x(N_conf) == 3
        in_text = strcat('DED')
    elseif type_unc_x(N_conf) == 3.1
        in_text = strcat('colorDED')
    elseif type_unc_x(N_conf) == 6
        in_text = strcat('SEDrec')
    elseif type_unc_x(N_conf) == 7
        in_text = strcat('DEDrec')
    elseif type_unc_x(N_conf) == 8
        in_text = strcat('delaySED')
    elseif type_unc_x(N_conf) == 8.1
        in_text = strcat('unsSED')
    elseif type_unc_x(N_conf) == 9
        in_text = strcat('delayDED')
    elseif type_unc_x(N_conf) == 9.1
        in_text = strcat('unsDED')
    end
    
    if type_unc_x(N_conf) == 0.2 %comparison SES_i and SES_v
        uncI = load(pwd,'\Results\ComparisonDSSE_MV_SES\UncDM_SES.mat');
        uncV = load(pwd,'\Results\ComparisonDSSE_MV_SES_v\UncDM_SES_v.mat');
        
        text_print = cell(size(uncI.UncDM.unc_estimated_i_lineD,1),1);
        
        for x = 1 :size(uncI.UncDM.unc_estimated_i_lineD,1)
            add_text = strcat(num2str(x),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
            t = 1;
            add_text = strcat(num2str(uncI.UncDM.unc_estimated_i_lineD(x,1),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
            t = 2;
            add_text = strcat(num2str(uncI.UncDM.unc_DSSE_emp_i_lineD(x,1),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
            t = 3;
            add_text = strcat(num2str(uncV.UncDM.unc_DSSE_emp_i_lineD(x,1),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
            
            t = 4;
            add_text = strcat(num2str(uncI.UncDM.unc_estimated_i_lineQ(x,1),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
            t = 5;
            add_text = strcat(num2str(uncI.UncDM.unc_DSSE_emp_i_lineQ(x,1),3),{' '},'&',{' '});
            text_print{x,1} = strcat(text_print{x,1},add_text);
            t = 6;
            add_text = strcat(num2str(uncV.UncDM.unc_DSSE_emp_i_lineQ(x,1),3),{' '},'\\');
            text_print{x,1} = strcat(text_print{x,1},add_text);
            
            disp(text_print{x})
        end

        plot_histogram_latex(in_text,uncI.UncDM)
        plot_histogram_latex(in_text,uncV.UncDM)
    else
        load(strcat(pwd,'\Results\ComparisonDSSE_MV_',in_text,'\UncDM_',in_text,'.mat'))
        plot_histogram_latex(in_text,UncDM)
        print_uncertainty_table(UncDM,type_unc_x(N_conf))
    end
    
end



