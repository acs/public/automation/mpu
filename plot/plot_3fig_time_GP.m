%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program to plot the uncertainties of the system
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% Model_Propagation_Uncertainty
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_3fig_time_GP(x_status_t,x_status_est_t,time_delta,Nw,type_unc)

for x = 1 : 1:size(x_status_t,1)
    figure;
    plot(time_delta*[1:Nw],x_status_t(x,:),'LineWidth',2,'Color',[0.39,0.86,0.90])
    S = sprintf('actual status %d*',x);
    %C = regexp(S, '*', 'split');
    hold on
    plot(time_delta*[1:Nw], x_status_est_t(x,:),'--s','LineWidth',1,'MarkerSize',5,'Color',[0.86,0.25,0.18])
    S = [S,sprintf('estimated status %d*',x)];
    C = regexp(S, '*', 'split');
    legend(C{1:end-1},'Location', 'Best')
    grid on
    %title('deviation SE - MC uncertainty')
    xlabel('time [s]')
    ylabel('status [-]')


    
    
    figure
    %movegui(f,'southwest');
    %set(f, 'Position', [425, 400, 450, 280])
    plot(time_delta*[1:Nw],(x_status_t(x,:)-x_status_est_t(x,:))./abs(x_status_t(x,:)))
    S = sprintf('status %d*', x);
    C = regexp(S, '*', 'split');
    legend(C{1:end-1},'Location', 'northeast')
    grid on
    %title('unc test model with MC')
    xlabel('time [s]')
    ylabel('relative error status [-]')

    
end


end

